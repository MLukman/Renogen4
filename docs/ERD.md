# Renogen4 ERD

```mermaid
erDiagram
    USER {
        string id PK
        string fullname
        string email
        image avatar
    }
    LOGIN {
        string id PK
        string method
        string credential
    }
    MEMBERSHIP {
        string role
    }
    ECOSYSTEM {
        string id PK
        string name
        array layers
    }
    PROJECT {
        string id PK
        string name
    }
    ENVIRONMENT {
        string id PK
        string name
    }
    COMPONENT {
        string id PK
        string name
        string layer
    }
    ACTIVITY_TEMPLATE {
        guid id PK
        string type
        json configurations
        int priority
    }
    DEPLOYMENT {
        guid id PK
        string name
        datetime executeDate
        string dateString
    }
    ITEM {
        guid id PK
        string refnum
        string name
        string status
        string category
        text description
        string externalUrl
        string externalUrlLabel
    }
    ITEM_STATUS_LOG {
        guid id PK
        string status
    }
    ACTIVITY {
        guid id PK
        json parameters
        string status
    }
    RUN_ITEM {
        guid id PK
        json parameters
        string status
    }
    CHECKLIST {
        guid id PK
        string title
        datetime start
        datetime end
    }
    CHECKLIST_UPDATE {
        guid id PK
        string comment
        string status
    }
    ITEM_COMMENT {
        guid id PK
        text text
    }
    ACTIVITY_FILE {
        guid id PK
    	string identifier
    	file file
    }
    ECOSYSTEM_ADMIN {
        int id PK
    }
    USER ||--o{ LOGIN: "authenticate via"
    USER ||--o{ MEMBERSHIP: "member of"
    MEMBERSHIP }o--|| PROJECT: "for"
    USER ||--o{ ECOSYSTEM_ADMIN: "is"
    ECOSYSTEM ||--o{ ECOSYSTEM_ADMIN: "has"
    ECOSYSTEM ||--o{ PROJECT: "contains"
    ECOSYSTEM ||--o{ ENVIRONMENT: "contains"
    ECOSYSTEM ||--o{ COMPONENT: "contains"
    PROJECT ||--o{ ITEM: "manages"
    ENVIRONMENT ||--o{ DEPLOYMENT: "has"
    DEPLOYMENT ||--o{ CHECKLIST: "consists of"
    MEMBERSHIP }o--o{ CHECKLIST: "in charge of"
    DEPLOYMENT ||--o{ ITEM: "consists of"
    DEPLOYMENT ||--o{ ACTIVITY: "consists of"
    CHECKLIST ||--o{ CHECKLIST_UPDATE: "has"
    ITEM ||--o{ ITEM_COMMENT: "has"
    ITEM ||--o{ ITEM_STATUS_LOG: "has"
    ITEM }o--o{ ACTIVITY: "registers"
    COMPONENT ||--o{ ACTIVITY_TEMPLATE: "provides"
    ENVIRONMENT ||--o{ ACTIVITY_TEMPLATE: "manages"
    ACTIVITY_TEMPLATE ||--o{ ACTIVITY: "produces"
    ACTIVITY ||--o{ ACTIVITY_FILE: "contains"
    ACTIVITY ||--o{ RUN_ITEM: "produces"
    ACTIVITY_TEMPLATE ||--o| RUN_ITEM: "last"
    

```

