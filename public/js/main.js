function dateParser(text) {
    if (!text || text == null || text == 'undefined' || text == 'null') {
        return null;
    }
    var dateArray = null;
    switch (text.length) {
        case 19:
            var reggie = /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}) (\w{2})/;
            dateArray = reggie.exec(text);
            if (dateArray[4] != 12 && dateArray[6] == 'PM') {
                dateArray[4] = parseInt(dateArray[4]) + 12 + "";
            } else if (dateArray[4] == 12 && dateArray[6] == "AM") {
                dateArray[4] = "00";
            }
            break;

        case 10:
            var reggie = /^(\d{4})-(\d{2})-(\d{2})/;
            dateArray = reggie.exec(text);
            dateArray[4] = "00";
            dateArray[5] = "00";
            break;

        case 7:
        case 8:
            var reggie = /^(\d{1,2}):(\d{2}) (\w{2})/;
            var timeArray = reggie.exec(text);
            var today = new Date();
            dateArray = [
                '', today.getFullYear(), today.getMonth() + 1, today.getDate(), timeArray[1], timeArray[2]
            ];
            if (timeArray[3] == 'PM') {
                dateArray[4] = parseInt(dateArray[4]) + 12 + "";
            } else if (timeArray[1] == 12) {
                dateArray[4] = "00";
            }
            break;

        default:
            return new Date();
    }
    var dateObject = new Date(
            (+dateArray[1]),
            (+dateArray[2]) - 1, // Careful, month starts at 0!
            (+dateArray[3]),
            (+dateArray[4]),
            (+dateArray[5])
            );
    return dateObject;
}

function dateFieldConfig(minDate, mode) {
    var cfg = {
        type: mode ? mode : 'datetime',
        //today: true,
        className: {
            todayCell: 'ui header today'
        },
        formatter: {
            date: function (date, settings) {
                if (!date)
                    return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return  year + '-' + ("00" + month).slice(-2) + '-' + ("00" + day).slice(-2);
            },
            time: function (date, settings) {
                if (!date)
                    return '';
                var hour = date.getHours();
                var ampm = '';
                if (hour == 12) {
                    ampm = 'PM';
                } else if (hour > 12) {
                    hour -= 12;
                    ampm = 'PM';
                } else {
                    ampm = 'AM';
                    if (hour == 0) {
                        hour = 12;
                    }
                }
                var minute = date.getMinutes();
                return ("00" + hour).slice(-2) + ':' + ("00" + minute).slice(-2) + ' ' + ampm;
            },
            datetime: function (date, setting) {
                return date ? cfg.formatter.date(date, setting) + ' ' + cfg.formatter.time(date, setting) : '';
            }
        }
        ,
        parser: {
            date: function (text, settings) {
                return dateParser(text);
            }
        }
    };
    if (minDate !== null) {
        cfg.minDate = minDate;
    }
    return cfg;
}

function adjustTopFiller() {
    $('#topfiller').height($('#topbar').height());
}

function onResize() {
    var gap = $('#topbar').outerHeight(true);
    $('#topfiller').css('height', gap);
    $('#topmargin').css('marginTop', gap).css('height', $('body').height() - gap);
    $('#body').css('padding-bottom', $('#footer').outerHeight());
}

function showToast(message, title, iserror) {
    $('body').toast({
        title: title,
        message: message,
        class: (iserror ? 'red' : 'inverted') + ' center aligned',
        position: 'bottom center'
    });
}

function confirmOperation(message, value) {
    if (prompt(message) === value) {
        return true;
    }
    showToast('Operation cancelled');
    return false;
}

function inputCopier(input, title) {
    var copytoast = (error) => showToast(
                error ? 'Unable to copy to clipboard due to error: ' + error : 'Copied to clipboard',
                title, error);
    input.select();
    if (!navigator.clipboard) {
        document.execCommand("copy");
        copytoast();
    } else {
        navigator.clipboard.writeText(input.val())
                .then(() => copytoast())
                .catch((err) => copytoast(err));
    }

}

$(function () {
    onResize();
    $(window).resize(onResize);
    $(window).on("scroll", function () {
        var scroll = $(window).scrollTop();
        if (scroll > 0) {
            $('#scrollToTop').show();
        } else {
            $('#scrollToTop').hide();
        }
    });
    $('#scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, "slow");
    });

    // dropdown
    $('select.ui.dropdown:not(.action-nothing)').dropdown({
        fullTextSearch: true,
        clearable: true
    });
    $('.ui.dropdown.action-nothing').dropdown({
        action: 'nothing'
    });
    $('.ui.dropdown:not(select):not(.action-nothing)').dropdown();
    $('.ui.dropdown.button').dropdown();

    // tabs
    $('.item[data-tab').tab({
        history: true,
        historyType: 'hash'
    });

    // sortable table
    $('.accordion:not(.nested)').accordion();

    // sortable table
    $('table.sortable').tablesort();

    // checkbox
    $('.ui.checkbox').checkbox();

    // closable message
    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });

    // autofocus
    $('input.autofocus').on('focus', function () {
        var end = $(this).val().length;
        $(this)[0].setSelectionRange(end, end);
    });
    $('input.autofocus').focus();

    // activate the confirmation on leave if the form is dirty
    $('form.confirm-on-leave').each(() => {
        var el = $(this);
        // since jquery.dirty relies on the id attribute, set one of not set
        if (el.attr('id') === undefined) {
            el.attr('id', 'form' + Math.floor(10000 + Math.random() * 100000));
        }
        // activate jquery.dirty
        el.dirty({
            preventLeaving: true
        });
        // if there are multiple forms on a page, set all clean
        el.on("submit", () => {
            $('form.confirm-on-leave').dirty("setAsClean");
        });
    });

    // top filler's height
    $(window).resize(adjustTopFiller);
    adjustTopFiller();

});