import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
mermaid.initialize({startOnLoad: false});
// Watch the element's attributes for changes
let mermaidObserverOpts = {
    attributes: true
};

window.markdownParseMermaid = (container) => {
    // Find all Mermaid elements and act on each
    let selector = (container ?
            $('pre > code[class=language-mermaid]', $(container)) :
            $('pre > code[class=language-mermaid]'));
    selector.each(function () {
        var el = $(this);
        let observer = new IntersectionObserver((entries) => {
            let target = $(entries[0].target);
            // Act only when the element becomes visible
            if (target.is(':visible')) {
                let html = target.text();
                target.text('');
                // Generate a unique-ish ID so we don't clobber existing graphs
                // This is definitely quick and dirty and could be improved to 
                // avoid collisions when many charts are used
                let id = 'graph-' + Math.floor(Math.random() * Math.floor(1000));
                // Actually render the chart
                mermaid.render(id, html).then(({ svg }) => {
                    target.parent().html(svg);
                }).catch((err) => {
                    let svg = $('#' + id);
                    svg.detach();
                    target.parent().replaceWith(
                            $('<div />')
                            .append(svg)
                            .append($('<pre style="color:red; font-weight:bold;" />').text(err))
                            );
                });
                // Disconnect the observer, since the chart is now on the page. 
                // There's no point in continuing to watch it
                observer.disconnect();
            }
        });
        observer.observe(el.get(0), mermaidObserverOpts);
    });
};

$(function () {
    markdownParseMermaid();
});
