<?php

namespace App\Service;

use ParsedownExtraPlugin;

class Markdown
{
    private ParsedownExtraPlugin $parser;

    public function __construct(array $props = [])
    {
        $this->parser = new class($props) extends \ParsedownExtraPlugin {
            public $array_lines = [];
            protected $props;

            function __construct(array $props)
            {
                $this->props = $props;
                parent::__construct();
            }

            protected function blockHeader($Line)
            {
                $Block = parent::blockHeader($Line);

                // Set headings
                if (isset($Block['element']['name'])) {
                    $Level = (integer) trim($Block['element']['name'], 'h');
                    $adjustLevel = intval($this->props['adjustHeaderLevel'] ?? 0);
                    $Block['element']['name'] = 'h'.($Level + $adjustLevel);
                    $this->array_lines[] = $Block;
                }
                return $Block;
            }
        };
        $this->parser->setSafeMode($props['safemode'] ?? true);
        $this->parser->table_class = 'ui table table-striped table-bordered';
    }

    public function parse(string $raw): string
    {
        return $this->parser->text(trim($raw));
    }
}