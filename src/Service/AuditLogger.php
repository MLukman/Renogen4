<?php

namespace App\Service;

use App\Entity\Login;
use Psr\Log\LoggerInterface;

class AuditLogger
{

    public function __construct(protected LoggerInterface $auditLogLogger)
    {

    }

    public function log(Login $login, string $event, array $details = []): void
    {
        $data = [
            'event' => $event,
            'user' => [
                'id' => $login->getUserIdentifier(),
                'email' => $login->getEmail(),
                'name' => $login->getUser() ? $login->getUser()->getFullname() : $login->getFullname(),
            ],
        ];
        if (!empty($details)) {
            $data['details'] = $details;
        }
        $this->auditLogLogger->info(json_encode($data));
    }
}