<?php

namespace App\Service;

use App\Service\DataStore;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UnregisteredUserRedirector implements EventSubscriberInterface
{
    private array $allowedControllers = ['App\\Controller\\ErrorController::show'];
    private array $allowedRoutes = [];
    private string $redirectToRoute = 'app_profile_update';

    public function __construct(protected DataStore $ds,
                                protected UrlGeneratorInterface $urlgen)
    {
        $this->allowedRoutes[] = $this->redirectToRoute;
    }

    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::REQUEST => 'onRequest'];
    }

    public function onRequest(RequestEvent $event): void
    {
        /** @var Request $request */
        $request = $event->getRequest();
        $attr = $request->attributes;
        $access_ctrl = $attr->get('_access_control_attributes') ?: [];

        if (in_array('PUBLIC_ACCESS', $access_ctrl) // public access
            || empty($this->ds->currentSecurityUser()) // not logged in
            || in_array($attr->get('_controller'), $this->allowedControllers) // allowed controllers
            || in_array($attr->get('_route'), $this->allowedRoutes) // allowed routes
        ) {
            // allow request handling
            return;
        }
        /** @var Session $session */
        $session = $request->getSession();

        if (!$this->isRegistered()) {
            if (empty($session->get('_redirect_after_save_profile'))) {
                $session->set('_redirect_after_save_profile', $request->getRequestUri());
            }
            $session->getFlashBag()->add('warning', 'Please update your profile before you can proceed to use Renogen');
            $event->setResponse(new RedirectResponse($this->urlgen->generate($this->redirectToRoute)));
        } elseif (($redirectTo = $session->get('_redirect_after_save_profile'))) {
            $session->remove('_redirect_after_save_profile');
            $event->setResponse(new RedirectResponse($redirectTo));
        }
    }

    protected function isRegistered(): bool
    {
        return !empty($this->ds->getUser());
    }
}