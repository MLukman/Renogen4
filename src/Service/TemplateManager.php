<?php

namespace App\Service;

use App\Base\ActivityTemplateInterface;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class TemplateManager
{
    protected array $templateClasses = [];

    public function __construct(#[TaggedIterator(ActivityTemplateInterface::class)] iterable $templateClasses)
    {
        foreach ($templateClasses as $tplClass) {
            $this->templateClasses[$tplClass::class] = $tplClass;
        }
    }

    public function allTemplateClasses(): array
    {
        return $this->templateClasses;
    }

    public function getTemplateClass(string $class): ?ActivityTemplateInterface
    {
        return $this->templateClasses[$class] ?? null;
    }
}