<?php

namespace App\Service;

use App\Entity\ActivityFile;
use App\Entity\ActivityTemplate;
use App\Entity\Component;
use App\Entity\Deployment;
use App\Entity\Ecosystem;
use App\Entity\Environment;
use App\Entity\Item;
use App\Entity\ItemActivity;
use App\Entity\ItemComment;
use App\Entity\ItemStatusLog;
use App\Entity\Membership;
use App\Entity\RunItem;
use App\Entity\Squad;
use App\Entity\User;
use DateTime;
use Doctrine\ORM\QueryBuilder;
use MLukman\DoctrineHelperBundle\Service\DataStore as DataStoreBase;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Service\Attribute\Required;

class DataStore extends DataStoreBase
{
    protected Security $security;
    protected CacheInterface $cache;

    #[Required]
    public function requiredByDataStore(Security $security,
                                        CacheInterface $cache)
    {
        $this->security = $security;
        $this->cache = $cache;
    }

    public function currentSecurityUser(): ?UserInterface
    {
        return $this->security->getUser();
    }

    public function getUser(): ?User
    {
        return $this->currentSecurityUser()->getUser();
    }

    public function getSquadMembership(Squad $squad, User $user): ?Membership
    {
        return $this->queryOne(Membership::class, ['squad' => $squad, 'member' => $user]);
    }

    public function getEcosystemMembers(Ecosystem $ecosystem): array
    {
        return $this->createQuery('SELECT DISTINCT u FROM App\\Entity\\User u JOIN App\\Entity\\Membership m WITH m.member = u JOIN App\\Entity\\Squad s WITH m.squad = s AND s.ecosystem = :ecosystem ORDER BY u.fullname ASC')
                ->setParameter('ecosystem', $ecosystem)
                ->getResult();
    }

    public function getActivityFile(?string $id): ?ActivityFile
    {
        return $this->queryOne(ActivityFile::class, $id);
    }

    public function getItemAllowedTransitions(Item $item)
    {
        $user = $this->getUser();
        $transitions = [];
        foreach (Item::STATUSES as $status => $config) {
            $progress = $item->compareCurrentStatusTo($status);
            $transition = [];
            if ($progress < 0) {
                // iterated status is behind current status
                if (!$user || $this->security->isGranted($config['role'], $item)) {
                    $transition[$status] = [
                        'label' => 'Revert',
                        'status' => $status,
                        'remark' => true,
                        'type' => '',
                    ];
                }
            } else if (in_array($item->getStatus(true), $config['requirecurrent'])) {
                // current status is compatible with this transition
                if (!$user || $this->security->isGranted($config['role'], $item)) {
                    $transition[$config['proceedstatus']] = [
                        'label' => $config['proceedaction'],
                        'status' => $config['proceedstatus'],
                        'remark' => false,
                        'type' => 'primary',
                    ];
                    if ($config['rejectaction']) {
                        $transition[$config['rejectaction']] = [
                            'label' => $config['rejectaction'],
                            'status' => $config['rejectaction'],
                            'remark' => true,
                            'type' => '',
                        ];
                    }
                    // Special condition: Documentation/Failed cannot be submitted for review if there are no activities
                    if (in_array($item->getStatus(true), [Item::STATUS_INIT, Item::STATUS_FAILED])
                        && $item->getItemActivities()->count() == 0) {
                        $transition = [];
                    }
                    // Special condition: Ready For Release cannot be completed here if there are activities
                    if ($item->getStatus() == Item::STATUS_READY &&
                        $item->getItemActivities()->count() > 0) {
                        $transition = [];
                    }
                }
            }
            if (!empty($transition)) {
                $transitions[$status] = $transition;
            }
        }
        return $transitions;
    }

    public function changeItemStatus(Item $item, string $status, $remark = null)
    {
        $old_status = $item->getStatus(false);
        if (Item::compareStatuses($old_status, $status) == 0) {
            return;
        }
        $item->setStatus($status);

        // Item changed status forwards to RFR
        if ($status == Item::STATUS_READY &&
            Item::compareStatuses($old_status, $status) > 0) {
            // process all activities into runitems
            foreach ($item->getItemActivities() as $itemActivity) {
                /** @var ItemActivity $itemActivity */
                if (!$itemActivity->getRunItem()) {
                    $newRunItem = null;
                    foreach ($itemActivity->getActivity()->getItemActivities() as $activityItemActivity) {
                        $checkRunItem = $activityItemActivity->getRunItem();
                        if ($checkRunItem && $checkRunItem->getStatus() == 'New') {
                            $newRunItem = $checkRunItem;
                            break;
                        }
                    }
                    if (!$newRunItem) {
                        $newRunItem = new RunItem($itemActivity->getActivity());
                    }
                    $itemActivity->setRunItem($newRunItem);
                }
            }
        }

        // Item changed status backward from RFR
        if (Item::compareStatuses(Item::STATUS_READY, $old_status) >= 0 &&
            Item::compareStatuses($status, Item::STATUS_READY) > 0) {
            // remove runitems from all activities
            foreach ($item->getItemActivities() as $itemActivity) {
                /** @var ItemActivity $itemActivity */
                $runItem = $itemActivity->getRunItem();
                if ($runItem && $runItem->getStatus() == 'New') {
                    if ($runItem->getItemActivities()->count() == 1) {
                        $runItem->getActivity()->removeRunItem($runItem);
                    }
                    $itemActivity->setRunItem(null);
                }
            }
        }
        $status_log = new ItemStatusLog($item, $status, $old_status);
        if (!empty($remark)) {
            $status_log->setRemark($remark);
            $comment = new ItemComment($item);
            $comment->setEvent("$old_status > $status");
            $comment->setText($remark);
            $item->addComment($comment);
        }
        $item->addStatusLog($status_log);

        // special case: if status is 'Ready for Release'
        if ($status == Item::STATUS_READY) {
            $all_completed = true;
            foreach ($item->getItemActivities() as $itemActivity) {
                $runItem = $itemActivity->getRunItem();
                if ($runItem->getStatus() != 'Completed') {
                    $all_completed = false;
                }
            }
            if ($all_completed) {
                $this->changeItemStatus($item, Item::STATUS_COMPLETED);
            }
        }
    }

    public function getActivityTemplates(Environment $environment,
                                         Component $component,
                                         bool $includeDisabled = false): array
    {
        $criteria = [
            'environment' => $environment,
            'component' => $component,
        ];
        if (!$includeDisabled) {
            $criteria['disabled'] = 0;
        }
        return $this->queryMany(ActivityTemplate::class, $criteria, ['priority' => 'DESC']);
    }

    public function getUpcomingDeploymentsForSquad(Squad $squad): array
    {
        return $this->queryBuilderForSquadDeployments($squad)
                ->andWhere('d.expectedCompletionDate > :now')->setParameter('now', new DateTime())
                ->orderBy('d.executeDate', 'ASC')
                ->getQuery()->getResult();
    }

    public function getDeploymentItemsForSquad(Deployment $deployment,
                                               Squad $squad): array
    {
        return $this->createQuery('SELECT i FROM App\\Entity\\Item i WHERE i.deployment = :deployment AND i.squad = :squad')
                ->setParameter('squad', $squad)
                ->setParameter('deployment', $deployment)
                ->getResult();
    }

    public function queryBuilderForSquadDeployments(Squad $squad): QueryBuilder
    {
        return $this->queryBuilder(Deployment::class, 'd')
                ->join('d.items', 'i', 'WITH', 'i.squad = :squad')
                ->setParameter('squad', $squad);
    }
}