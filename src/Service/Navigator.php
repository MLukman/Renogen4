<?php

namespace App\Service;

use App\Base\Entity;
use App\Entity\Activity;
use App\Entity\ActivityTemplate;
use App\Entity\Checklist;
use App\Entity\Component;
use App\Entity\Deployment;
use App\Entity\DeploymentRequest;
use App\Entity\Ecosystem;
use App\Entity\Environment;
use App\Entity\Item;
use App\Entity\ItemActivity;
use App\Entity\Squad;
use Doctrine\DBAL\Types\ConversionException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class Navigator
{
    public const ROUTE_PERMISSIONS = [
        'app_ecosystem_view' => ['ecosystem' => 'any'],
        'app_ecosystem_update' => ['ecosystem' => 'admin'],
        'app_ecosystem_add' => [],
        'app_environment_view' => ['ecosystem' => 'any'],
        'app_environment_update' => ['ecosystem' => 'admin'],
        'app_environment_add' => ['ecosystem' => 'admin'],
        'app_file_download' => ['ecosystem' => 'any'],
        'app_squad_view' => ['ecosystem' => 'any'],
        'app_squad_update' => ['squad' => 'approval', 'ecosystem' => 'admin'],
        'app_squad_add' => ['ecosystem' => 'admin'],
        'app_squad_users' => ['squad' => 'approval', 'ecosystem' => 'admin'],
        'app_squad_favourite_ajax' => ['ecosystem' => 'any'],
        'app_squad_deployment' => ['ecosystem' => 'any'],
        'app_component_view' => ['ecosystem' => 'any'],
        'app_component_update' => ['ecosystem' => 'admin'],
        'app_component_add' => ['ecosystem' => 'admin'],
        'app_activitytemplate_view' => ['ecosystem' => 'any'],
        'app_activitytemplate_update' => ['ecosystem' => 'admin'],
        'app_activitytemplate_add' => ['ecosystem' => 'admin'],
        'app_deployment_view' => ['ecosystem' => 'any'],
        'app_deployment_update' => ['ecosystem' => ['admin', 'approval']],
        'app_deployment_add' => ['ecosystem' => ['admin', 'approval']],
        'app_deployment_runbook' => ['ecosystem' => ['admin', 'approval', 'execute']],
        'app_deployment_releasenote' => ['ecosystem' => 'any'],
        'app_deployment_request_update' => ['ecosystem' => 'any'],
        'app_deployment_request_add' => ['ecosystem' => 'entry'],
        'app_item_view' => ['ecosystem' => 'any'],
        'app_item_update' => ['squad' => ['approval', 'entry']],
        'app_item_add' => ['squad' => ['approval', 'entry']],
        'app_activity_update' => ['squad' => ['approval', 'entry']],
        'app_activity_add' => ['squad' => ['approval', 'entry']],
        'app_activity_remove' => ['squad' => ['approval', 'entry']],
        'app_checklist_update' => ['ecosystem' => 'any'],
        'app_checklist_add' => ['ecosystem' => 'any'],
    ];

    protected ?array $_entities = null;

    public function __construct(protected DataStore $ds,
                                protected ViewContext $context,
                                protected RequestStack $requestStack,
                                protected RouterInterface $router,
                                protected Security $security)
    {

    }

    public function addHeader(string $id, string $text, string $url,
                              ?string $icon = null, ?string $tooltip = null,
                              array $extraCssClasses = [])
    {
        $this->context->addHeader($id, $text, $url, $icon, $tooltip, $extraCssClasses);
    }

    public function entityRoutePath(string $route, Entity ...$entities): string
    {
        $params = array_reduce($entities, function (array $carry, Entity $entity) {
            return $carry + $this->entityToParameters($entity);
        }, []);
        return $this->router->generate(
                $route,
                $params,
                UrlGeneratorInterface::ABSOLUTE_PATH
        );
    }

    public function entityToParameters(Entity $entity)
    {
        switch (true) {
            case $entity instanceof Ecosystem:
                return ['ecosystemId' => $entity->getId()];
            case $entity instanceof Component:
                return ['componentId' => $entity->getIdentifier()] + $this->entityToParameters($entity->getEcosystem());
            case $entity instanceof Environment:
                return ['environmentId' => $entity->getIdentifier()] + $this->entityToParameters($entity->getEcosystem());
            case $entity instanceof Squad:
                return ['squadId' => $entity->getIdentifier()] + $this->entityToParameters($entity->getEcosystem());
            case $entity instanceof Deployment:
                return ['deploymentId' => $entity->getIdentifier()] + $this->entityToParameters($entity->getEnvironment());
            case $entity instanceof DeploymentRequest:
                return ['deploymentRequestId' => $entity->getIdentifier()] + $this->entityToParameters($entity->getEnvironment());
            case $entity instanceof Item:
                return ['itemId' => $entity->getId()] + $this->entityToParameters($entity->getDeployment())
                    + $this->entityToParameters($entity->getSquad());
            case $entity instanceof Checklist:
                return ['checklistId' => $entity->getId()] + $this->entityToParameters($entity->getDeployment());
            case $entity instanceof Activity:
                return ['activityId' => $entity->getId()] + $this->entityToParameters($entity->getDeployment());
            case $entity instanceof ActivityTemplate:
                return ['templateId' => $entity->getId()] + $this->entityToParameters($entity->getComponent())
                    + $this->entityToParameters($entity->getEnvironment());
        }
        return [];
    }

    public function fetchEcosystem(string $ecosystemId, ?string $url = null): ?Ecosystem
    {
        $ecosystem = $this->ds->queryOne(Ecosystem::class, $ecosystemId);
        if (!$ecosystem) {
            return null;
        }
        $this->addHeader('eco-'.$ecosystemId,
            $ecosystem->getName(),
            $url ?: $this->entityRoutePath('app_ecosystem_view', $ecosystem),
            'sitemap',
            'Ecosystem');
        return $ecosystem;
    }

    public function fetchComponent(Ecosystem|string $ecosystem,
                                   string $componentId, ?string $url = null): ?Component
    {
        if (\is_string($ecosystem)) {
            $ecosystem = $this->fetchEcosystem($ecosystem);
        }
        $component = $this->ds->queryOne(Component::class, [
            'ecosystem' => $ecosystem,
            'identifier' => $componentId,
        ]);
        if (!$component) {
            return null;
        }
        $this->addHeader('cmp-'.$componentId,
            $component->getName(),
            $url ?: $this->entityRoutePath('app_component_view', $component),
            'cubes',
            'Component');
        return $component;
    }

    public function fetchEnvironment(Ecosystem|string $ecosystem,
                                     string $environmentId, ?string $url = null): ?Environment
    {
        if (\is_string($ecosystem)) {
            $ecosystem = $this->fetchEcosystem($ecosystem);
        }
        $environment = $this->ds->queryOne(Environment::class, [
            'ecosystem' => $ecosystem,
            'identifier' => $environmentId,
        ]);
        if (!$environment) {
            return null;
        }
        $this->addHeader('env-'.$environmentId,
            $environment->displayTitle(),
            $url ?: $this->entityRoutePath('app_environment_view', $environment),
            'globe',
            'Environment');
        return $environment;
    }

    public function fetchSquad(Ecosystem|string $ecosystem, string $squadId,
                               ?string $url = null): ?Squad
    {
        if (\is_string($ecosystem)) {
            $ecosystem = $this->fetchEcosystem($ecosystem);
        }
        $squad = $this->ds->queryOne(Squad::class, [
            'ecosystem' => $ecosystem,
            'identifier' => $squadId,
        ]);
        if (!$squad) {
            return null;
        }
        $this->addHeader('squad-'.$squadId,
            sprintf(empty($squad->getDecommissioned()) ? '%s' : '%s [decommissioned]', $squad->getName()),
            $url ?: $this->entityRoutePath('app_squad_view', $squad),
            ($squad->getIcon()) ? $squad->getIcon() : 'users',
            'Squad');
        return $squad;
    }

    public function fetchTemplate(Ecosystem|string $ecosystem,
                                  Environment|string $environment,
                                  string $templateId, ?string $url = null): ?ActivityTemplate
    {
        if (\is_string($ecosystem)) {
            $ecosystem = $this->fetchEcosystem($ecosystem);
        }
        if (\is_string($environment) && $ecosystem) {
            $environment = $this->fetchEnvironment($ecosystem, $environment);
        }
        $template = $this->ds->queryOne(ActivityTemplate::class, [
            'environment' => $environment,
            'id' => $templateId
        ]);
        if (!$template) {
            return null;
        }
        $this->addHeader('templ-'.$templateId,
            $template->getName(),
            $url ?: $this->entityRoutePath('app_activitytemplate_view', $template),
            'clipboard',
            'Activity Template');
        return $template;
    }

    public function fetchDeployment(Ecosystem|string $ecosystem,
                                    Environment|string $environment,
                                    string $deploymentId, ?string $url = null): ?Deployment
    {
        if (\is_string($ecosystem)) {
            $ecosystem = $this->fetchEcosystem($ecosystem);
        }
        if (\is_string($environment) && $ecosystem) {
            $environment = $this->fetchEnvironment($ecosystem, $environment);
        }
        $deployment = $this->ds->queryBuilder(Deployment::class, 'd')
            ->where('d.environment = :environment')
            ->andWhere('d.identifier >= :deploymentId')
            ->orderBy('d.identifier', 'ASC')
            ->setParameter('deploymentId', $deploymentId)
            ->setParameter('environment', $environment)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        if (!$deployment) {
            return null;
        }
        $this->addHeader('depl-'.$deploymentId,
            $deployment->displayTitle(),
            $url ?: $this->entityRoutePath('app_deployment_view', $deployment),
            'calendar check o',
            'Deployment',
            $deployment->isRunning() ? ['blink'] : []
        );
        return $deployment;
    }

    public function fetchDeploymentRequest(Ecosystem|string $ecosystem,
                                           Environment|string $environment,
                                           string $deploymentRequestId,
                                           ?string $url = null): ?DeploymentRequest
    {
        if (\is_string($ecosystem)) {
            $ecosystem = $this->fetchEcosystem($ecosystem);
        }
        if (\is_string($environment) && $ecosystem) {
            $environment = $this->fetchEnvironment($ecosystem, $environment);
        }
        $deploymentRequest = $this->ds->queryOne(DeploymentRequest::class, [
            'environment' => $environment,
            'identifier' => $deploymentRequestId
        ]);
        if (!$deploymentRequest) {
            return null;
        }
        return $deploymentRequest;
    }

    public function fetchItem(Deployment $deployment, Squad $squad,
                              string $itemId, ?string $url = null): ?Item
    {
        $item = $this->ds->queryOne(Item::class, [
            'deployment' => $deployment,
            'squad' => $squad,
            'id' => $itemId,
        ]);
        if (!$item) {
            return null;
        }
        $this->addHeader('item-'.$itemId,
            $item->displayTitle(),
            $url ?: $this->entityRoutePath('app_item_view', $item),
            'flag',
            'Deployment Item');
        return $item;
    }

    public function fetchActivity(Deployment $deployment, string $activityId,
                                  ?string $url = null): ?Activity
    {
        $activity = $this->ds->queryOne(Activity::class, [
            'deployment' => $deployment,
            'id' => $activityId,
        ]);
        if (!$activity) {
            return null;
        }
        $this->addHeader('act-'.$activityId,
            $activity->displayTitle(),
            $url ?: '#',
            'cart',
            'Activity');
        return $activity;
    }

    public function fetchItemActivity(Item $item, Activity $activity): ?ItemActivity
    {
        return $this->ds->queryOne(ItemActivity::class, [
                'item' => $item,
                'activity' => $activity,
        ]);
    }

    public function fetchChecklist(Deployment $deployment, string $checklistId,
                                   ?string $url = null): ?Checklist
    {
        $checklist = $this->ds->queryOne(Checklist::class, [
            'deployment' => $deployment,
            'id' => $checklistId,
        ]);
        if (!$checklist) {
            return null;
        }
        $this->addHeader('checklist-'.$checklistId,
            $checklist->displayTitle(),
            $url ?: $this->entityRoutePath('app_checklist_update', $checklist),
            'tasks',
            'Checklist');
        return $checklist;
    }

    public function resolveEntitiesFromRouteParameters(?array $params = null): array
    {
        static $notFoundMessage = 'Invalid URL path';
        if (is_array($this->_entities)) {
            return $this->_entities;
        }

        $this->_entities = [];
        if (!$params) {
            $params = $this->requestStack->getCurrentRequest()->attributes->get('_route_params');
        }

        try {
            // resolve ecosystem
            if (($ecosystemId = $params['ecosystemId'] ?? null)) {
                $this->_entities['ecosystem'] = $this->fetchEcosystem($ecosystemId)
                        ?:
                    throw new NotFoundHttpException(sprintf("Ecosystem '$ecosystemId' not found"));
            }

            // resolve component
            if (($componentId = $params['componentId'] ?? null) && isset($this->_entities['ecosystem'])) {
                $this->_entities['component'] = $this->fetchComponent($this->_entities['ecosystem'], $componentId)
                        ?: throw new NotFoundHttpException("Component '$componentId' not found");
            }

            // resolve environment
            if (($environmentId = $params['environmentId'] ?? null) && isset($this->_entities['ecosystem'])) {
                $this->_entities['environment'] = $this->fetchEnvironment($this->_entities['ecosystem'], $environmentId)
                        ?: throw new NotFoundHttpException("Environment '$environmentId' not found");
            }

            // resolve template
            if (($templateId = $params['templateId'] ?? null) && isset($this->_entities['environment'])) {
                $this->_entities['template'] = $this->fetchTemplate($this->_entities['environment']->getEcosystem(), $this->_entities['environment'], $templateId)
                        ?: throw new NotFoundHttpException("Template '$templateId' not found");
            }

            // resolve deployment
            if (($deploymentId = $params['deploymentId'] ?? null) && isset($this->_entities['environment'])) {
                $this->_entities['deployment'] = $this->fetchDeployment($this->_entities['environment']->getEcosystem(), $this->_entities['environment'], $deploymentId)
                        ?: throw new NotFoundHttpException("Deployment '$deploymentId' not found");
            }

            // resolve deployment request
            if (($deploymentRequestId = $params['deploymentRequestId'] ?? null) && isset($this->_entities['environment'])) {
                $this->_entities['deploymentRequest'] = $this->fetchDeploymentRequest($this->_entities['environment']->getEcosystem(), $this->_entities['environment'], $deploymentRequestId)
                        ?: throw new NotFoundHttpException("Deployment request '$deploymentRequestId' not found");
            }

            // resolve squad
            $squadId = $params['squadId'] ?? null;
            if ($squadId && isset($this->_entities['deployment'])) {
                $this->_entities['squad'] = $this->fetchSquad($this->_entities['deployment']->getEnvironment()->getEcosystem(), $squadId,
                        $this->router->generate('app_squad_deployment',
                            ['squadId' => $squadId] + $this->entityToParameters($this->_entities['deployment'])))
                        ?: throw new NotFoundHttpException("Squad '$squadId' not found");
            } elseif ($squadId && isset($this->_entities['ecosystem'])) {
                $this->_entities['squad'] = $this->fetchSquad($this->_entities['ecosystem'], $squadId)
                        ?: throw new NotFoundHttpException("Squad '$squadId' not found");
            }

            // resolve item
            if (($itemId = $params['itemId'] ?? null) && isset($this->_entities['deployment'])
                && isset($this->_entities['squad'])) {
                $this->_entities['item'] = $this->fetchItem($this->_entities['deployment'], $this->_entities['squad'], $itemId)
                        ?: throw new NotFoundHttpException("Item '$itemId' not found");
            }

            // resolve checklist
            if (($checklistId = $params['checklistId'] ?? null) && isset($this->_entities['deployment'])) {
                $this->_entities['checklist'] = $this->fetchChecklist($this->_entities['deployment'], $checklistId)
                        ?: throw new NotFoundHttpException("Checklist '$checklistId' not found");
            }

            // resolve activity
            if (($activityId = $params['activityId'] ?? null) && isset($this->_entities['deployment'])) {
                $this->_entities['activity'] = $this->fetchActivity($this->_entities['deployment'], $activityId)
                        ?: throw new NotFoundHttpException("Activity '$activityId' not found");
            }
        } catch (ConversionException $ex) {
            throw new NotFoundHttpException($notFoundMessage);
        }
        return $this->_entities;
    }

    public function generateUrl(string $route, array $parameters = [],
                                int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string
    {
        return $this->router->generate($route, $parameters, $referenceType);
    }

    public function isRouteGranted(string $route, array $extraEntities = []): bool
    {
        if (!isset(self::ROUTE_PERMISSIONS[$route]) || $this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        $entities = $this->resolveEntitiesFromRouteParameters() + $extraEntities;
        foreach (self::ROUTE_PERMISSIONS[$route] as $entity => $permissions) {
            if ($this->security->isGranted($permissions, $entities[$entity])) {
                return true;
            }
        }
        return false;
    }
}