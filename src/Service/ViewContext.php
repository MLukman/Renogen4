<?php

namespace App\Service;

class ViewContext implements \ArrayAccess
{
    protected array $context = [
        'headers' => [],
    ];

    public function addHeader(string $id, string $text, string $url,
                              string $icon = null, $tooltip = null,
                              array $extraCssClasses = [])
    {
        $tooltip = $tooltip ?: $text;
        $this->context['headers'][$id] = compact(['text', 'url', 'icon', 'tooltip',
            'extraCssClasses']);
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->context[$offset]);
    }

    public function &offsetGet(mixed $offset): mixed
    {
        return $this->context[$offset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->context[$offset] = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->context[$offset]);
    }

    public function merge(array $merge): self
    {
        $this->context += $merge;
        return $this;
    }

    public function toArray(): array
    {
        return $this->context;
    }
}