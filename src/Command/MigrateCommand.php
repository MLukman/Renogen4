<?php

namespace App\Command;

use App\Service\DataStore;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Service\Attribute\Required;

#[AsCommand(
        name: 'app:migrate',
        description: 'Migrate data if necessary (to be called automatically by start script)',
    )]
class MigrateCommand extends Command
{
    protected DataStore $ds;

    #[Required]
    public function required(DataStore $ds)
    {
        $this->ds = $ds;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if (method_exists(\App\Entity\Login::class, 'getTheUser')) {
            $patched = 0;
            $logins = $this->ds->createQuery('SELECT login FROM \\App\\Entity\\Login login WHERE login.user IS NULL AND login.theUser IS NOT NULL')->getResult();
            foreach ($logins as $login) {
                $login->setUser($login->getTheUser());
                $patched++;
            }
            $this->ds->commit();
            $io->success(sprintf("Logins patched: %d", $patched));
        }
        return Command::SUCCESS;
    }
}