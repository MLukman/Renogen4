<?php

namespace App\Base;

use App\Base\Entity;
use App\Entity\Login;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Interface\AuditedEntityInterface;
use MLukman\DoctrineHelperBundle\Trait\AuditedEntityTrait;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\MappedSuperclass, ORM\HasLifecycleCallbacks]
abstract class AuditedEntity extends Entity implements AuditedEntityInterface
{

    use AuditedEntityTrait;
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    protected ?User $createdBy = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    protected ?User $updatedBy = null;

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setCreatedBy(?UserInterface $createdBy)
    {
        if ($createdBy instanceof Login) {
            $this->createdBy = $createdBy->getUser();
        }
    }

    public function setUpdatedBy(?UserInterface $updatedBy)
    {
        if ($updatedBy instanceof Login) {
            $this->updatedBy = $updatedBy->getUser();
        }
    }
}