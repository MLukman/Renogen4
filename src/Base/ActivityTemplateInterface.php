<?php

namespace App\Base;

use App\Entity\Actionable;
use App\Entity\Activity;
use App\Entity\ActivityTemplate;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag(ActivityTemplateInterface::class)]
interface ActivityTemplateInterface
{

    static public function templateFormTwigFilepath(): string;

    static public function activityFormTwigFilepath(): string;

    static public function allowMultipleTemplates(): bool;

    public function prepareTemplateName(ActivityTemplate $template):string;

    public function validateTemplate(ActivityTemplate $template): array;

    public function validateActivity(Activity $activity): array;

    public function describeActionableAsArray(Actionable $actionable, bool $isForRunBook = false): array;
}