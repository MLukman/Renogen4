<?php

namespace App\Base;

use App\Service\AuditLogger;
use App\Service\DataStore;
use App\Service\Navigator;
use App\Service\ViewContext;
use MLukman\DoctrineHelperBundle\Service\ObjectValidator;
use MLukman\DoctrineHelperBundle\Service\RequestBodyConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Service\Attribute\Required;

class Controller extends AbstractController
{
    protected DataStore $ds;
    protected ViewContext $context;
    protected Navigator $nav;
    protected ObjectValidator $validator;
    protected AuditLogger $auditLogger;
    protected RequestBodyConverter $bodyConverter;
    protected Security $security;
    protected RequestStack $requestStack;

    #[Required]
    public function requiredByController(DataStore $ds, ViewContext $context,
                                         Navigator $nav,
                                         ObjectValidator $validator,
                                         AuditLogger $auditLogger,
                                         RequestBodyConverter $bodyConverter,
                                         Security $security,
                                         RequestStack $requestStack)
    {
        $this->ds = $ds;
        $this->context = $context;
        $this->nav = $nav;
        $this->validator = $validator;
        $this->auditLogger = $auditLogger;
        $this->bodyConverter = $bodyConverter;
        $this->security = $security;
        $this->requestStack = $requestStack;
    }

    public function reload(Request $request): Response
    {
        $redirectTo = $request->getSession()->get('_redirect_later') ?: $request->getRequestUri();
        $request->getSession()->remove('_redirect_later');
        return new RedirectResponse($redirectTo, RedirectResponse::HTTP_SEE_OTHER);
    }

    public function redirect($url, int $status = 303): RedirectResponse
    {
        $request = $this->requestStack->getCurrentRequest();
        $redirectTo = $request->getSession()->get('_redirect_later') ?: $url;
        $request->getSession()->remove('_redirect_later');
        return parent::redirect($redirectTo, $status);
    }

    protected function redirectToRoute($route, $parameters = [],
                                       int $status = 303, ?string $anchor = null): RedirectResponse
    {
        $redirect = parent::redirectToRoute($route, $parameters, $status);
        if ($anchor) {
            if (substr($anchor, 0, 1) != '#') {
                $anchor = '#'.$anchor;
            }
            $redirect->setTargetUrl($redirect->getTargetUrl().$anchor);
        }
        return $redirect;
    }

    public function addHeader(string $id, string $text, ?string $url = null,
                              ?string $icon = null, ?string $tooltip = null,
                              array $extraCssClasses = [])
    {
        $this->context->addHeader($id, $text, $url ?: $this->requestStack->getCurrentRequest()->getRequestUri(), $icon, $tooltip, $extraCssClasses);
    }

    protected function render(string $view, array $parameters = [],
                              Response $response = null): Response
    {
        return parent::render($view, $this->context->merge($this->nav->resolveEntitiesFromRouteParameters())->merge($parameters)->toArray(), $response);
    }

    protected function saveCurrentUrlToRedirectLater(Request $request,
                                                     ?bool $useRefererInstead = false)
    {
        $request->getSession()->set('_redirect_later',
            ($useRefererInstead && ($referer = $request->headers->get('referer')))
                    ? $referer : $request->getRequestUri());
    }

    protected function getCurrentUrlToRedirectLater(Request $request): ?string
    {
        $request->getSession()->get('_redirect_later');
    }

    protected function resolveEntitiesAndCheckPermission(): array
    {
        $route = $this->requestStack->getCurrentRequest()->attributes->get('_route');
        if (!$this->nav->isRouteGranted($route)) {
            throw $this->createAccessDeniedException();
        }
        return $this->nav->resolveEntitiesFromRouteParameters();
    }
}