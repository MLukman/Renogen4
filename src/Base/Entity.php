<?php

namespace App\Base;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\DTO\RequestBodyTargetInterface;
use Stringable;
use Symfony\Component\PropertyAccess\PropertyAccessor;

#[ORM\MappedSuperclass]
#[ORM\HasLifecycleCallbacks]
abstract class Entity implements RequestBodyTargetInterface, Stringable
{
    private array $_caches;

    protected function cached($cacheid, callable $create, $force = false)
    {
        if (!isset($this->_caches)) {
            $this->_caches = [];
        }
        if (!isset($this->_caches[$cacheid]) || $force) {
            $this->_caches[$cacheid] = $create();
        }
        return $this->_caches[$cacheid];
    }

    public function toArray(): array
    {
        return \json_decode(\json_encode($this->jsonSerialize()), true);
    }

    /**
     * Filters the provided collection for date_field that has upcoming dates only..
     * @param Selectable $collection The collection to filter from
     * @param string $date_field The name of the field that contains the start date time
     * @param string $end_date_field The name of the field that contains the end date time
     * @param int $limit Limit to this number of results. Default to 0, which means no limit
     * @return Selectable the filtered collection sorted by the ascending order of the date field
     */
    static public function filterUpcomingDateOnly(Selectable $collection,
                                                  $date_field,
                                                  $end_date_field = null,
                                                  $limit = 0): Selectable
    {
        $compare_dates = [];
        $now = date_create();
        if (!empty($end_date_field)) {
            // find out if there is ongoing deployment (the latest one between now and previous $lookback hours)
            $ongoing = $collection->matching(Criteria::create()
                    ->where(new Comparison("$end_date_field", '>=', $now))
                    ->andWhere(new Comparison($date_field, '<=', $now))
                    ->orderBy([$date_field => 'DESC'])
                    ->setMaxResults(1));
            if ($ongoing->count() > 0) {
                $compare_dates[] = (new PropertyAccessor())->getValue($ongoing->get(0), $date_field);
            }
        }
        $compare_dates[] = $now;
        $upcoming = [];
        foreach ($compare_dates as $compare) {
            $criteria = Criteria::create()
                ->where(new Comparison($date_field, '>=', $compare))
                ->orderBy([$date_field => 'ASC']);
            if ($limit > 0) {
                $criteria = $criteria->setMaxResults(abs($limit));
            }
            $upcoming = $collection->matching($criteria);
            if ($upcoming->count() > 0) {
                break;
            }
        }
        return $upcoming;
    }

    static public function filterPastDateOnly(Selectable $collection,
                                              $date_field,
                                              $end_date_field = null, $limit = 0): Selectable
    {
        $now = date_create();
        $compare_date = $now;
        if (!empty($end_date_field)) {
            // find out if there is ongoing deployment (the latest one between now and previous $lookback hours)
            $ongoing = $collection->matching(Criteria::create()
                    ->where(new Comparison("$end_date_field", '>=', $now))
                    ->andWhere(new Comparison($date_field, '<=', $now))
                    ->orderBy([$date_field => 'DESC'])
                    ->setMaxResults(1));
            if ($ongoing->count() > 0) {
                $compare_date = (new PropertyAccessor())->getValue($ongoing->get(0), $date_field);
            }
        }

        $criteria = Criteria::create()
            ->where(new Comparison($date_field, '<', $compare_date))
            ->orderBy(array($date_field => 'DESC'))
            ->setMaxResults(abs($limit));
        return $collection->matching($criteria);
    }

    abstract public function displayTitle(): ?string;

    public function __toString(): string
    {
        return $this->displayTitle() ?: '';
    }
}