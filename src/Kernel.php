<?php

namespace App;

use App\Security\Authentication\OAuth2Authenticator;
use App\Security\Authentication\OAuth2Authenticator\DigitalmeProvider;
use KnpU\OAuth2ClientBundle\Client\OAuth2PKCEClient;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{

    use MicroKernelTrait {
        configureContainer as traitConfigureContainer;
    }

    public function boot(): void
    {
        parent::boot();
        date_default_timezone_set($this->getContainer()->getParameter('timezone'));
    }

    private function configureContainer(ContainerConfigurator $container,
                                        LoaderInterface $loader,
                                        ContainerBuilder $builder): void
    {
        $this->traitConfigureContainer($container, $loader, $builder);
        $container->extension('knpu_oauth2_client', [
            'clients' => $this->generateKnpuOAuth2ClientConfigurations(),
        ]);
    }

    private function generateKnpuOAuth2ClientConfigurations(): array
    {
        $knpu_clients = [];

        if (!empty($_ENV['DIGITALME_CLIENT_ID'])) {
            $knpu_clients['digitalme'] = [
                'type' => 'generic',
                'client_id' => $_ENV['DIGITALME_CLIENT_ID'],
                'client_secret' => '',
                'provider_class' => DigitalmeProvider::class,
                'client_class' => OAuth2PKCEClient::class,
                'provider_options' => [
                    'use_staging' => $_ENV['DIGITALME_USE_STAGING'] ?? false,
                ],
                'redirect_route' => OAuth2Authenticator::OAUTH_REDIRECT_ROUTE,
                'redirect_params' => ['client' => 'digitalme'],
            ];
        }

        return $knpu_clients;
    }
}