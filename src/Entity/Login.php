<?php

namespace App\Entity;

use App\Repository\LoginRepository;
use App\Security\Authentication\UserEntity;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LoginRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Login extends UserEntity
{
    #[ORM\ManyToOne(inversedBy: 'logins', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    private ?User $user = null;

    public function getRoles(): array
    {
        return $this->user ? $this->user->getRoles() : ['ROLE_USER'];
    }

    public function setRoles(array $roles): UserEntity
    {
        if ($this->user) {
            $this->user->setRoles($roles);
        }

        return $this;
    }

    public function isBlocked(): ?bool
    {
        return parent::isBlocked() ||
            ($this->user ? $this->user->isBlocked() : false);
    }

    public function setLastLogin(DateTimeInterface $lastLogin): UserEntity
    {
        parent::setLastLogin($lastLogin);
        if ($this->user) {
            $this->user->setLastLogin($this->getLastLogin());
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}