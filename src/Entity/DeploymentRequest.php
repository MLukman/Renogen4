<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\DeploymentRequestRepository;
use App\Security\Authorization\SecuredAccessInterface;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: DeploymentRequestRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(
        fields: ['environment', 'executeDate'],
        errorPath: 'executeDate',
    )]
class DeploymentRequest extends AuditedEntity implements SecuredAccessInterface
{

    use UuidEntityTrait;
    #[ORM\Column(length: 12)]
    private ?string $identifier = null;

    #[ORM\ManyToOne(inversedBy: 'deploymentRequests')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Environment $environment = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\NotBlank]
    private ?DateTimeInterface $executeDate = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\Column]
    #[Assert\Positive]
    public int $duration = 1;

    #[ORM\OneToOne(inversedBy: 'deploymentRequest', cascade: ['persist'])]
    private ?Deployment $deployment = null;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    public function displayTitle(): ?string
    {
        if (!$this->executeDate) {
            return null;
        }
        return ($this->executeDate->format('hi') == '0000' ?
            $this->executeDate->format('Y-m-d') : $this->executeDate->format('Y-m-d h:i A'))
            .' - '.$this->name;
    }

    public function isRoleAllowed(string $role, string $attribute): bool
    {
        return $this->getEnvironment()->getEcosystem()->isRoleAllowed($role, $attribute);
    }

    public function isUsernameAllowed(string $username, string $attribute): bool
    {
        switch ($attribute) {
            case 'update':
                return $this->isUsernameAllowed($username, 'entry') || $this->isUsernameAllowed($username, 'approval');
            case 'accept':
                return $this->isUsernameAllowed($username, 'approval') || $this->environment->getEcosystem()->isUsernameAllowed($username, 'admin');
            case 'delete':
                return $this->getCreatedBy()->getId() == $username;
        }
        return $this->getEnvironment()->getEcosystem()->isUsernameAllowed($username, $attribute);
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getEnvironment(): ?Environment
    {
        return $this->environment;
    }

    public function getExecuteDate(): ?DateTimeInterface
    {
        return $this->executeDate;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function setEnvironment(?Environment $environment): void
    {
        $this->environment = $environment;
    }

    public function setExecuteDate(?DateTimeInterface $executeDate): void
    {
        $this->executeDate = $executeDate;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function generateIdentifier(): void
    {
        if ($this->executeDate) {
            $this->identifier = $this->executeDate->format('YmdHi');
        }
    }

    public function canDelete(): bool
    {
        return true;
    }

    public function getDeployment(): ?Deployment
    {
        return $this->deployment;
    }

    public function setDeployment(?Deployment $deployment): self
    {
        $this->deployment = $deployment;

        return $this;
    }

    public function createDeployment(): Deployment
    {
        $deployment = new Deployment($this->getEnvironment());
        $deployment->setName($this->getName());
        $deployment->setDescription($this->getDescription());
        $deployment->setExecuteDate($this->getExecuteDate());
        $deployment->setDuration($this->getDuration());
        $deployment->setDeploymentRequest($this);
        return $deployment;
    }
}