<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\ItemCommentRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;

#[ORM\Entity(repositoryClass: ItemCommentRepository::class)]
#[ORM\HasLifecycleCallbacks]
class ItemComment extends AuditedEntity
{
    use UuidEntityTrait;
    #[ORM\ManyToOne(inversedBy: 'comments')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Item $item = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $text = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $deletedDate = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $event = null;

    public function __construct(?Item $item)
    {
        $this->item = $item;
    }


    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getDeletedDate(): ?DateTimeInterface
    {
        return $this->deletedDate;
    }

    public function setDeletedDate(?DateTimeInterface $deletedDate): self
    {
        $this->deletedDate = $deletedDate;

        return $this;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function setEvent(?string $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->text;
    }
}