<?php

namespace App\Entity;

use App\Base\Entity;
use App\Repository\UserRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Interface\AuditedEntityInterface;
use MLukman\DoctrineHelperBundle\Trait\AuditedEntityTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity('fullname')]
class User extends Entity implements AuditedEntityInterface
{

    use AuditedEntityTrait;
    #[ORM\Id]
    #[ORM\Column(length: 15)]
    protected ?string $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(
            min: 5,
            max: 100,
            minMessage: 'Full name must be at least {{ limit }} characters long',
            maxMessage: 'Full name cannot be longer than {{ limit }} characters',
        )]
    protected ?string $fullname = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\Email]
    protected ?string $email = null;

    #[ORM\Column(type: 'image', nullable: true)]
    protected $avatar = null;

    #[ORM\Column(nullable: true)]
    protected array $roles = ['ROLE_USER'];

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Login::class, orphanRemoval: true,
            cascade: ['persist', 'remove'])]
    protected Collection $logins;

    #[ORM\OneToMany(mappedBy: 'member', targetEntity: Membership::class, orphanRemoval: true,
            cascade: ['persist', 'remove'])]
    protected Collection $memberships;

    #[ORM\Column(nullable: true)]
    protected ?bool $blocked = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?DateTimeInterface $lastLogin = null;

    #[ORM\ManyToMany(targetEntity: Checklist::class, mappedBy: 'pics')]
    protected Collection $checklists;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: EcosystemAdmin::class, orphanRemoval: true, indexBy: 'ecosystem_id', cascade: [
            'persist'])]
    protected Collection $ecosystemAdmins;

    public function __construct(string $id)
    {
        $this->id = $id;
        $this->logins = new ArrayCollection();
        $this->memberships = new ArrayCollection();
        $this->checklists = new ArrayCollection();
        $this->ecosystemAdmins = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setAvatar($avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return Collection<int, Login>
     */
    public function getLogins(): Collection
    {
        return $this->logins;
    }

    public function addLogin(Login $login): self
    {
        if (!$this->logins->contains($login)) {
            $this->logins->add($login);
            $login->setUser($this);
        }

        return $this;
    }

    public function removeLogin(Login $login): self
    {
        if ($this->logins->removeElement($login)) {
            // set the owning side to null (unless already changed)
            if ($login->getUser() === $this) {
                $login->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Membership>
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    public function addMembership(Membership $membership): self
    {
        if (!$this->memberships->contains($membership)) {
            $this->memberships->add($membership);
            $membership->setMember($this);
        }

        return $this;
    }

    public function removeMembership(Membership $membership): self
    {
        if ($this->memberships->removeElement($membership)) {
            // set the owning side to null (unless already changed)
            if ($membership->getMember() === $this) {
                $membership->setMember(null);
            }
        }

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->fullname;
    }

    public function getRoles(): array
    {
        return $this->roles ?: ['ROLE_USER'];
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles ?: [];

        return $this;
    }

    public function isAdmin(): bool
    {
        return in_array('ROLE_ADMIN', $this->roles);
    }

    public function setAdmin(bool $admin): self
    {
        if ($admin) {
            $this->roles = array_unique(array_merge(['ROLE_ADMIN'], $this->roles));
        } else {
            $this->roles = array_values(array_diff($this->roles, ['ROLE_ADMIN']));
        }
        return $this;
    }
    private ?bool $admin;

    public function isBlocked(): ?bool
    {
        return $this->blocked;
    }

    public function setBlocked(?bool $blocked): self
    {
        $this->blocked = $blocked;

        return $this;
    }

    public function getLastLogin(): ?DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * @return Collection<int, Checklist>
     */
    public function getChecklists(): Collection
    {
        return $this->checklists;
    }

    public function addChecklist(Checklist $checklist): self
    {
        if (!$this->checklists->contains($checklist)) {
            $this->checklists->add($checklist);
            $checklist->addPic($this);
        }

        return $this;
    }

    public function removeChecklist(Checklist $checklist): self
    {
        if ($this->checklists->removeElement($checklist)) {
            $checklist->removePic($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, EcosystemAdmin>
     */
    public function getEcosystemAdmins(): Collection
    {
        return $this->ecosystemAdmins;
    }

    public function addEcosystemAdmin(EcosystemAdmin $ecosystemAdmin): static
    {
        if (!$this->ecosystemAdmins->contains($ecosystemAdmin)) {
            $this->ecosystemAdmins->add($ecosystemAdmin);
            $ecosystemAdmin->setUser($this);
        }

        return $this;
    }

    public function removeEcosystemAdmin(EcosystemAdmin $ecosystemAdmin): static
    {
        if ($this->ecosystemAdmins->removeElement($ecosystemAdmin)) {
            // set the owning side to null (unless already changed)
            if ($ecosystemAdmin->getUser() === $this) {
                $ecosystemAdmin->setUser(null);
            }
        }

        return $this;
    }
}