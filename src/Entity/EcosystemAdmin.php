<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\EcosystemAdminRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EcosystemAdminRepository::class)]
#[ORM\HasLifecycleCallbacks]
class EcosystemAdmin extends AuditedEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'ecosystemAdmins')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'ecosystemAdmins')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Ecosystem $ecosystem = null;

    public function __construct(?Ecosystem $ecosystem, ?User $user)
    {
        $this->user = $user;
        $this->ecosystem = $ecosystem;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getEcosystem(): ?Ecosystem
    {
        return $this->ecosystem;
    }

    public function setEcosystem(?Ecosystem $ecosystem): static
    {
        $this->ecosystem = $ecosystem;

        return $this;
    }

    public function displayTitle(): ?string
    {
        return sprintf("%s @ %s", $this->user->displayTitle(), $this->ecosystem->displayTitle());
    }
}