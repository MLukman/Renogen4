<?php

namespace App\Entity;

use App\Base\ActivityTemplateInterface;
use App\Base\AuditedEntity;
use App\DTO\ActivityTemplateResponse;
use App\Repository\ActivityTemplateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use MLukman\DoctrineHelperBundle\DTO\ResponseBody;
use MLukman\DoctrineHelperBundle\DTO\ResponseBodySourceInterface;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ActivityTemplateRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Index(name: 'class_idx', columns: ['class'])]
#[UniqueEntity(
        fields: ['environment', 'name'],
        errorPath: 'name',
    )]
class ActivityTemplate extends AuditedEntity implements JsonSerializable, ResponseBodySourceInterface
{
    public const CLASS_NAME = '';

    use UuidEntityTrait;
    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    protected ?string $name = null;

    #[ORM\Column(nullable: true)]
    protected array $configurations = [];

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Positive]
    protected ?int $priority = null;

    #[ORM\ManyToOne(inversedBy: 'activityTemplates', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    protected ?Component $component = null;

    #[ORM\ManyToOne(inversedBy: 'activityTemplates', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    protected ?Environment $environment = null;

    #[ORM\OneToMany(mappedBy: 'template', targetEntity: Activity::class, orphanRemoval: true)]
    #[ORM\OrderBy(['created' => 'ASC'])]
    protected Collection $activities;

    #[ORM\OneToMany(mappedBy: 'template', targetEntity: RunItem::class, orphanRemoval: true)]
    #[ORM\OrderBy(['created' => 'ASC'])]
    protected Collection $runItems;

    #[ORM\Column(length: 255)]
    protected ?string $class = null;

    #[ORM\Column(nullable: true)]
    protected ?bool $singleActivityPerDeployment = null;

    #[ORM\Column(options: ['default' => false])]
    private ?bool $disabled = false;

    public function __construct(Environment $environment, Component $component,
                                ActivityTemplateInterface $class)
    {
        $this->environment = $environment;
        $this->component = $component;
        $this->class = \get_class($class);
        $this->activities = new ArrayCollection();
        $this->runItems = new ArrayCollection();
        $this->singleActivityPerDeployment = !$class::allowMultipleTemplates();
        $this->name = $class->prepareTemplateName($this);
    }

    public function getComponent(): ?Component
    {
        return $this->component;
    }

    public function getEnvironment(): ?Environment
    {
        return $this->environment;
    }

    public function getName(): ?string
    {
        return $this->name ?: sprintf('%s %s', static::CLASS_NAME, $this->component->displayTitle());
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function &getConfigurations(): array
    {
        return $this->configurations;
    }

    public function setConfigurations(?array $configurations): self
    {
        $this->configurations = $configurations;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Collection<int, Activity>
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            //$activity->setTemplate($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activities->removeElement($activity)) {
            // set the owning side to null (unless already changed)
            if ($activity->getTemplate() === $this) {
                //$activity->setTemplate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RunItem>
     */
    public function getRunItems(): Collection
    {
        return $this->runItems;
    }

    public function addRunItem(RunItem $runItem): self
    {
        if (!$this->runItems->contains($runItem)) {
            $this->runItems->add($runItem);
            //$activity->setTemplate($this);
        }

        return $this;
    }

    public function removeRunItem(RunItem $runItem): self
    {
        if ($this->runItems->removeElement($runItem)) {
            // set the owning side to null (unless already changed)
            if ($runItem->getTemplate() === $this) {
                //$activity->setTemplate(null);
            }
        }

        return $this;
    }

    public function creatableForDeployment(Deployment $deployment): bool
    {
        return !$this->singleActivityPerDeployment ||
            0 == $this->activities->matching(
                Criteria::create()
                    ->where(new Comparison('deployment', '=', $deployment))
                    ->andWhere(new Comparison('status', 'IN', ['New', 'Failed']))
            )->count();
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->name;
    }

    public function isSingleActivityPerDeployment(): ?bool
    {
        return $this->singleActivityPerDeployment;
    }

    public function setSingleActivityPerDeployment(?bool $singleActivityPerDeployment): self
    {
        $this->singleActivityPerDeployment = $singleActivityPerDeployment;

        return $this;
    }

    public function canDelete(): bool
    {
        return $this->activities->count() == 0;
    }

    public function isDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(bool $disabled): static
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function createResponseBody(): ?ResponseBody
    {
        return new ActivityTemplateResponse();
    }

    public function jsonSerialize(): mixed
    {
        return ResponseBody::createResponseFromSource($this);
    }
}