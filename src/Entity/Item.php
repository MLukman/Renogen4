<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\ItemRepository;
use App\Security\Authorization\SecuredAccessInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ExpressionBuilder;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ItemRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(
        fields: ['deployment', 'name'],
        errorPath: 'name',
    )]
class Item extends AuditedEntity implements SecuredAccessInterface
{

    use UuidEntityTrait;
    public const STATUS_INIT = 'Documentation';
    public const STATUS_REVIEW = 'Review';
    public const STATUS_APPROVAL = 'Go No Go';
    public const STATUS_READY = 'Ready For Release';
    public const STATUS_COMPLETED = 'Completed';
    public const STATUS_REJECTED = 'Rejected';
    public const STATUS_FAILED = 'Failed';
    public const STATUSES = [
        self::STATUS_INIT => [
            'icon' => 'edit',
            'stepicon' => 'edit',
            'proceedaction' => 'Submit For Review',
            'proceedstatus' => self::STATUS_REVIEW,
            'rejectaction' => false,
            'role' => ['entry', 'approval'],
            'requirecurrent' => [self::STATUS_INIT],
            'color' => 'teal',
            'sequence' => 1,
        ],
        self::STATUS_REVIEW => [
            'icon' => 'clipboard check',
            'stepicon' => 'clipboard check',
            'proceedaction' => 'Verified',
            'proceedstatus' => self::STATUS_APPROVAL,
            'rejectaction' => 'Rejected',
            'role' => ['review', 'approval'],
            'requirecurrent' => [self::STATUS_REVIEW],
            'color' => 'grey',
            'sequence' => 2,
        ],
        self::STATUS_APPROVAL => [
            'icon' => 'thumbs up',
            'stepicon' => 'thumbs up',
            'proceedaction' => 'Approved',
            'proceedstatus' => self::STATUS_READY,
            'rejectaction' => 'Rejected',
            'role' => 'approval',
            'requirecurrent' => [self::STATUS_REVIEW, self::STATUS_APPROVAL],
            'color' => 'yellow',
            'sequence' => 3,
        ],
        self::STATUS_READY => [
            'icon' => 'cloud upload',
            'stepicon' => 'cloud upload',
            'proceedaction' => 'Completed',
            'proceedstatus' => self::STATUS_COMPLETED,
            'rejectaction' => 'Failed',
            'role' => ['execute', 'approval'],
            'requirecurrent' => [self::STATUS_READY],
            'color' => 'orange',
            'sequence' => 4,
        ],
        self::STATUS_COMPLETED => [
            'icon' => 'flag checkered',
            'stepicon' => 'flag checkered',
            'proceedaction' => false,
            'proceedstatus' => null,
            'rejectaction' => false,
            'requirecurrent' => [self::STATUS_COMPLETED],
            'role' => null,
            'color' => 'green',
            'sequence' => 5,
        ]
    ];
    public const ALL_STATUSES = [
        self::STATUS_FAILED => [
            'icon' => 'x',
            'color' => 'red',
        ],
        self::STATUS_REJECTED => [
            'icon' => 'x',
            'color' => 'red',
        ],
        ] + self::STATUSES;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $name = null;

    #[ORM\Column(length: 30)]
    private ?string $status = self::STATUS_INIT;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $refnum = null;

    #[ORM\Column(length: 100)]
    #[Assert\NotBlank]
    private ?string $category = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $externalUrl = null;

    #[ORM\Column(length: 15, nullable: true)]
    private ?string $externalUrlLabel = null;

    #[ORM\ManyToOne(inversedBy: 'items', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Deployment $deployment = null;

    #[ORM\ManyToOne(inversedBy: 'items', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Squad $squad = null;

    #[ORM\OneToMany(mappedBy: 'item', targetEntity: ItemComment::class, orphanRemoval: true,
            cascade: ['persist', 'remove'])]
    #[ORM\OrderBy(['created' => 'ASC'])]
    private Collection $comments;

    #[ORM\OneToMany(mappedBy: 'item', targetEntity: ItemStatusLog::class, orphanRemoval: true,
            fetch: 'EXTRA_LAZY', cascade: ['persist', 'remove'])]
    #[ORM\OrderBy(['created' => 'ASC'])]
    private Collection $statusLogs;

    #[ORM\OneToMany(mappedBy: 'item', targetEntity: ItemActivity::class, orphanRemoval: true,
            cascade: ['persist', 'remove'])]
    #[ORM\OrderBy(['created' => 'ASC'])]
    private Collection $itemActivities;

    public function __construct(Deployment $deployment, Squad $squad)
    {
        $this->deployment = $deployment;
        $this->squad = $squad;
        $this->comments = new ArrayCollection();
        $this->statusLogs = new ArrayCollection();
        $this->itemActivities = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(bool $mapped = false): ?string
    {
        return $mapped ? static::realStatus($this->status) : $this->status;
    }

    static public function realStatus(string $status): string
    {
        if ($status == self::STATUS_FAILED || $status == self::STATUS_REJECTED) {
            return self::STATUS_INIT;
        }
        return $status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRefnum(): ?string
    {
        return $this->refnum;
    }

    public function setRefnum(?string $refnum): self
    {
        $this->refnum = $refnum;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getExternalUrl(): ?string
    {
        return $this->externalUrl;
    }

    public function setExternalUrl(?string $externalUrl): self
    {
        $this->externalUrl = $externalUrl;

        return $this;
    }

    public function getExternalUrlLabel(): ?string
    {
        return $this->externalUrlLabel;
    }

    public function setExternalUrlLabel(?string $externalUrlLabel): self
    {
        $this->externalUrlLabel = $externalUrlLabel;

        return $this;
    }

    public function getDeployment(): ?Deployment
    {
        return $this->deployment;
    }

    public function setDeployment(?Deployment $deployment): self
    {
        $this->deployment = $deployment;

        return $this;
    }

    public function getSquad(): ?Squad
    {
        return $this->squad;
    }

    public function displayTitle(): ?string
    {
        return !empty($this->refnum) ? sprintf("[%s] %s", $this->refnum, $this->name)
                : $this->name;
    }

    /**
     * @return Collection<int, ItemComment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(ItemComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setItem($this);
        }

        return $this;
    }

    public function removeComment(ItemComment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getItem() === $this) {
                $comment->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ItemStatusLog>
     */
    public function getStatusLogs(): Collection
    {
        return $this->statusLogs;
    }

    public function addStatusLog(ItemStatusLog $statusLog): self
    {
        if (!$this->statusLogs->contains($statusLog)) {
            $this->statusLogs->add($statusLog);
            $statusLog->setItem($this);
        }

        return $this;
    }

    public function removeStatusLog(ItemStatusLog $statusLog): self
    {
        if ($this->statusLogs->removeElement($statusLog)) {
            // set the owning side to null (unless already changed)
            if ($statusLog->getItem() === $this) {
                $statusLog->setItem(null);
            }
        }

        return $this;
    }

    public function getStatusLog($status): ?ItemStatusLog
    {
        static $crit = null;
        if (!$crit) {
            $eb = new ExpressionBuilder();
            $crit = new Criteria($eb->eq('status', $status));
        }
        return $this->statusLogs->filter(function (ItemStatusLog $log) use ($status) {
                return $log->getStatus() == $status;
            })->last() ?: null;
    }

    public function getStatusLogBefore(ItemStatusLog $status)
    {
        $found = false;
        foreach (array_reverse($this->statusLogs->toArray()) as $log) {
            if ($found && $log->getCreated() < $status->getCreated()) {
                return $log;
            } else if ($log === $status) {
                $found = true;
            }
        }
    }

    public function compareCurrentStatusTo(string $status_to_compare): int
    {
        return static::compareStatuses($this->status, $status_to_compare);
    }

    /**
     *
     * @param string $compare_status
     * @param string $against_status
     * @return int if > 0 then compare_status is before against_status & vice versa.
     */
    static public function compareStatuses(string $compare_status,
                                           string $against_status): int|bool
    {
        $_statuses = array_keys(self::STATUSES);
        $compare_status_idx = array_search(static::realStatus($compare_status), $_statuses);
        $against_status_idx = array_search(static::realStatus($against_status), $_statuses);
        if ($against_status_idx === FALSE) {
            return FALSE;
        }
        if ($compare_status_idx === FALSE) {
            return -1 - $against_status_idx;
        }
        return $against_status_idx - $compare_status_idx;
    }

    public function getNextStatus(): ?string
    {
        $_statuses = array_keys(self::STATUSES);
        $compare_status = array_search($this->status, $_statuses);
        if ($compare_status === FALSE) {
            return $_statuses[0];
        } elseif ($compare_status < count($_statuses) - 1) {
            return $_statuses[$compare_status + 1];
        } else {
            return null;
        }
    }

    public function statusConfig(): array
    {
        return self::ALL_STATUSES[$this->status];
    }

    public function statusIcon(): string
    {
        return $this->statusConfig()['icon'];
    }

    public function statusSortValue(): int
    {
        return array_search($this->status, array_keys(static::ALL_STATUSES));
    }

    /**
     * @return Collection<int, ItemActivity>
     */
    public function getItemActivities(): Collection
    {
        return $this->itemActivities;
    }

    public function addItemActivity(ItemActivity $itemActivity): self
    {
        if (!$this->itemActivities->contains($itemActivity)) {
            $this->itemActivities->add($itemActivity);
            $itemActivity->setItem($this);
        }

        return $this;
    }

    public function removeItemActivity(ItemActivity $itemActivity): self
    {
        if ($this->itemActivities->removeElement($itemActivity)) {
            // set the owning side to null (unless already changed)
            if ($itemActivity->getItem() === $this) {
                $itemActivity->setItem(null);
            }
        }

        return $this;
    }

    public function isRoleAllowed(string $role, string $attribute): bool
    {
        return $this->squad->isRoleAllowed($role, $attribute);
    }

    public function isUsernameAllowed(string $username, string $attribute): bool
    {
        switch ($attribute) {
            case 'delete':
                return ($this->createdBy && $this->createdBy->getId() == $username)
                    || $this->isUsernameAllowed($username, 'approval');
        }

        return $this->squad->isUsernameAllowed($username, $attribute);
    }

    public function canDelete(): bool
    {
        return $this->itemActivities->count() == 0;
    }

    #[ORM\PreRemove]
    public function cleanupBeforeRemove()
    {
        foreach ($this->itemActivities as $itemActivity) {
            /** @var ItemActivity $itemActivity */
            $this->removeItemActivity($itemActivity);
        }
    }
}