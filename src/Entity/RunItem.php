<?php

namespace App\Entity;

use App\Repository\RunItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RunItemRepository::class)]
#[ORM\HasLifecycleCallbacks]
class RunItem extends Actionable
{
    #[ORM\ManyToOne(inversedBy: 'runItems')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Activity $activity = null;

    #[ORM\ManyToOne(inversedBy: 'runItems')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    protected ?ActivityTemplate $template = null;

    #[ORM\ManyToOne(inversedBy: 'runItems')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Deployment $deployment = null;

    #[ORM\OneToMany(mappedBy: 'runItem', targetEntity: ItemActivity::class)]
    private Collection $itemActivities;

    public function __construct(?Activity $activity)
    {
        $this->activity = $activity;
        $this->template = $activity->getTemplate();
        $this->deployment = $activity->getDeployment();
        $this->setParameters($activity->getParameters());
        $activity->addRunItem($this);
        $activity->setStatus('New');
        $this->itemActivities = new ArrayCollection();
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function getDeployment(): ?Deployment
    {
        return $this->deployment;
    }

    /**
     * @return Collection<int, ItemActivity>
     */
    public function getItemActivities(): Collection
    {
        return $this->itemActivities;
    }

    public function addItemActivity(ItemActivity $itemActivity): self
    {
        if (!$this->itemActivities->contains($itemActivity)) {
            $this->itemActivities->add($itemActivity);
            $itemActivity->setRunItem($this);
        }

        return $this;
    }

    public function removeItemActivity(ItemActivity $itemActivity): self
    {
        if ($this->itemActivities->removeElement($itemActivity)) {
            // set the owning side to null (unless already changed)
            if ($itemActivity->getRunItem() === $this) {
                $itemActivity->setRunItem(null);
            }
        }

        return $this;
    }

    public function getTemplate(): ?ActivityTemplate
    {
        return $this->template;
    }
}