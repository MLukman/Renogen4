<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\EcosystemRepository;
use App\Security\Authorization\SecuredAccessInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: EcosystemRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity('id')]
#[UniqueEntity('name')]
class Ecosystem extends AuditedEntity implements SecuredAccessInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 30)]
    #[Assert\NotBlank]
    #[Assert\Length(
            min: 2,
            max: 30,
            minMessage: 'ID must be at least {{ limit }} characters long',
            maxMessage: 'ID cannot be longer than {{ limit }} characters',
        )]
    #[Assert\Regex(
            pattern: '/^[a-z][a-z0-9_-]*$/i',
            message: 'Must be alphanumeric, dashes and underscores only (the first character must be alphabet)'
        )]
    private ?string $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $name = null;

    #[ORM\Column(type: Types::ARRAY)]
    #[Assert\Count(
            min: 1,
            minMessage: 'You must specify at least one layer',
        )]
    private array $layers = [];

    #[ORM\OneToMany(mappedBy: 'ecosystem', targetEntity: Environment::class, orphanRemoval: true)]
    #[ORM\OrderBy(['level' => 'DESC'])]
    private Collection $environments;

    #[ORM\OneToMany(mappedBy: 'ecosystem', targetEntity: Component::class, orphanRemoval: true)]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $components;

    #[ORM\OneToMany(mappedBy: 'ecosystem', targetEntity: Squad::class, orphanRemoval: true)]
    private Collection $squads;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'ecosystem', targetEntity: EcosystemAdmin::class, orphanRemoval: true, indexBy: 'user_id')]
    private Collection $ecosystemAdmins;

    #[ORM\Column(nullable: true)]
    private ?bool $private = null;

    public function __construct(?string $id)
    {
        $this->id = $id;
        $this->environments = new ArrayCollection();
        $this->squads = new ArrayCollection();
        $this->components = new ArrayCollection();
        $this->ecosystemAdmins = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLayers(): array
    {
        return $this->layers;
    }

    public function setLayers(?array $layers): self
    {
        $this->layers = array_values(array_filter($layers));

        return $this;
    }

    /**
     * @return Collection<int, Environment>
     */
    public function getEnvironments(): Collection
    {
        return $this->environments;
    }

    public function addEnvironment(Environment $environment): self
    {
        if (!$this->environments->contains($environment)) {
            $this->environments->add($environment);
            $environment->setEcosystem($this);
        }

        return $this;
    }

    public function removeEnvironment(Environment $environment): self
    {
        if ($this->environments->removeElement($environment)) {
            // set the owning side to null (unless already changed)
            if ($environment->getEcosystem() === $this) {
                $environment->setEcosystem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Squad>
     */
    public function getSquads(): Collection
    {
        return $this->squads;
    }

    public function addSquad(Squad $squad): self
    {
        if (!$this->squads->contains($squad)) {
            $this->squads->add($squad);
            $squad->setEcosystem($this);
        }

        return $this;
    }

    public function removeSquad(Squad $squad): self
    {
        if ($this->squads->removeElement($squad)) {
            // set the owning side to null (unless already changed)
            if ($squad->getEcosystem() === $this) {
                $squad->setEcosystem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Component>
     */
    public function getComponents(): Collection
    {
        return $this->components;
    }

    public function addComponent(Component $component): self
    {
        if (!$this->components->contains($component)) {
            $this->components->add($component);
            $component->setEcosystem($this);
        }

        return $this;
    }

    public function removeComponent(Component $component): self
    {
        if ($this->components->removeElement($component)) {
            // set the owning side to null (unless already changed)
            if ($component->getEcosystem() === $this) {
                $component->setEcosystem(null);
            }
        }

        return $this;
    }

    public function getComponentsByLayer(): array
    {
        $cbl = array_fill_keys($this->layers, []);
        foreach ($this->components as $component) {
            $cbl[$component->getLayer()][] = $component;
        }
        return $cbl;
    }

    public function displayTitle(): ?string
    {
        return $this->name;
    }

    public function isRoleAllowed(string $role, string $attribute): bool
    {
        foreach ($this->squads as $squad) {
            if ($squad->isRoleAllowed($role, $attribute)) {
                return true;
            }
        }
        return false;
    }

    public function isUsernameAllowed(string $username, string $attribute): bool
    {
        if ($this->ecosystemAdmins->containsKey($username) &&
            in_array($attribute, ['any', 'admin'])) {
            return true;
        }
        foreach ($this->squads as $squad) {
            if ($squad->isUsernameAllowed($username, $attribute)) {
                return true;
            }
        }
        return false;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function canDelete(): bool
    {
        return $this->environments->count() + $this->squads->count() == 0;
    }

    /**
     * @return Collection<int, EcosystemAdmin>
     */
    public function getEcosystemAdmins(): Collection
    {
        return $this->ecosystemAdmins;
    }

    public function addEcosystemAdmin(EcosystemAdmin $ecosystemAdmin): static
    {
        if (!$this->ecosystemAdmins->contains($ecosystemAdmin)) {
            $this->ecosystemAdmins->add($ecosystemAdmin);
            $ecosystemAdmin->setEcosystem($this);
        }

        return $this;
    }

    public function removeEcosystemAdmin(EcosystemAdmin $ecosystemAdmin): static
    {
        if ($this->ecosystemAdmins->removeElement($ecosystemAdmin)) {
            // set the owning side to null (unless already changed)
            if ($ecosystemAdmin->getEcosystem() === $this) {
                $ecosystemAdmin->setEcosystem(null);
            }
        }

        return $this;
    }

    public function isPrivate(): ?bool
    {
        return $this->private;
    }

    public function setPrivate(?bool $private): static
    {
        $this->private = $private;

        return $this;
    }
}