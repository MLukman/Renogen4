<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\DeploymentRepository;
use App\Security\Authorization\SecuredAccessInterface;
use DateInterval;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: DeploymentRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(
        fields: ['environment', 'executeDate'],
        errorPath: 'executeDate',
    )]
class Deployment extends AuditedEntity implements SecuredAccessInterface
{

    use UuidEntityTrait;
    #[ORM\ManyToOne(inversedBy: 'deployments', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Environment $environment = null;

    #[ORM\Column(length: 12)]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $name = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\NotBlank]
    private ?DateTimeInterface $executeDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $expectedCompletionDate = null;

    #[ORM\Column]
    #[Assert\Positive]
    public int $duration = 1;

    #[ORM\OneToMany(mappedBy: 'deployment', targetEntity: Item::class, orphanRemoval: true)]
    private Collection $items;

    #[ORM\OneToMany(mappedBy: 'deployment', targetEntity: Activity::class, orphanRemoval: true)]
    private Collection $activities;

    #[ORM\OneToMany(mappedBy: 'deployment', targetEntity: RunItem::class, orphanRemoval: true)]
    private Collection $runItems;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\OneToOne(mappedBy: 'deployment', cascade: ['persist', 'remove'])]
    private ?DeploymentRequest $deploymentRequest = null;

    #[ORM\OneToMany(mappedBy: 'deployment', targetEntity: Checklist::class, orphanRemoval: true)]
    private Collection $checklists;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
        $this->items = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->runItems = new ArrayCollection();
        $this->checklists = new ArrayCollection();
    }

    public function getEnvironment(): ?Environment
    {
        return $this->environment;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getExecuteDate(): ?DateTimeInterface
    {
        return $this->executeDate;
    }

    public function setExecuteDate(DateTimeInterface $executeDate): self
    {
        $this->executeDate = $executeDate;
        if ($this->executeDate) {
            $hour = $this->duration ?: 1;
            $this->expectedCompletionDate = (clone $this->executeDate)->add(new DateInterval("PT{$hour}H"));
            $this->identifier = $this->executeDate->format('YmdHi');
        }

        return $this;
    }

    public function getExpectedCompletionDateDate(): ?DateTimeInterface
    {
        return $this->expectedCompletionDate;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return Collection<int, Item>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
            $item->setDeployment($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getDeployment() === $this) {
                $item->setDeployment(null);
            }
        }

        return $this;
    }

    public function getItemsForSquad(Squad $squad): Collection
    {
        return $this->items->matching(Criteria::create()->where(Criteria::expr()->eq('squad', $squad)));
    }

    /**
     * @return Collection<int, Activity>
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activities->contains($activity)) {
            $this->activities->add($activity);
            $activity->setDeployment($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activities->removeElement($activity)) {
            // set the owning side to null (unless already changed)
            if ($activity->getDeployment() === $this) {
                $activity->setDeployment(null);
            }
        }

        return $this;
    }

    #[ORM\PostLoad]
    public function calculateExpectedCompletionDate(): void
    {
        if ($this->executeDate) {
            $hour = $this->duration ?: 1;
            $expectedCompletionDate = (clone $this->executeDate)->add(new DateInterval("PT{$hour}H"));
            // we only assign if different in order to avoid unnecessarily dirtying the object
            if ($this->expectedCompletionDate != $expectedCompletionDate) {
                $this->expectedCompletionDate = $expectedCompletionDate;
            }
        }
    }

    public function getItemsWithStatus($status): array
    {
        $status_items = $this->cached("items",
            function () {
                $status_items = [];
                foreach ($this->items as $item) {
                    if (!isset($status_items[$item->getStatus()])) {
                        $status_items[$item->getStatus()] = [];
                    }
                    $status_items[$item->getStatus()][] = $item;
                }
                return $status_items;
            });
        return $status_items[$status] ?? [];
    }

    public function isActive(): bool
    {
        return ($this->executeDate >= date_create(sprintf("-%d hours", $this->duration)));
    }

    public function isRunning(): bool
    {
        return $this->isActive() && $this->executeDate <= date_create();
    }

    public function displayTitle(): ?string
    {
        if (!$this->executeDate) {
            return null;
        }
        return (substr($this->identifier, -4) == '0000' ?
            $this->executeDate->format('Y-m-d') : $this->executeDate->format('Y-m-d h:i A'))
            .' - '.$this->name;
    }

    /**
     * @return Collection<int, RunItem>
     */
    public function getRunItems(): Collection
    {
        return $this->runItems;
    }

    public function addRunItem(RunItem $runItem): self
    {
        if (!$this->runItems->contains($runItem)) {
            $this->runItems->add($runItem);
            $runItem->setDeployment($this);
        }

        return $this;
    }

    public function removeRunItem(RunItem $runItem): self
    {
        if ($this->runItems->removeElement($runItem)) {
            // set the owning side to null (unless already changed)
            if ($runItem->getDeployment() === $this) {
                $runItem->setDeployment(null);
            }
        }

        return $this;
    }

    public function isRoleAllowed(string $role, string $attribute): bool
    {
        return $this->getEnvironment()->getEcosystem()->isRoleAllowed($role, $attribute);
    }

    public function isUsernameAllowed(string $username, string $attribute): bool
    {
        return $this->getEnvironment()->getEcosystem()->isUsernameAllowed($username, $attribute);
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function canDelete(): bool
    {
        return $this->items->count() == 0;
    }

    public function getDeploymentRequest(): ?DeploymentRequest
    {
        return $this->deploymentRequest;
    }

    public function setDeploymentRequest(?DeploymentRequest $deploymentRequest): self
    {
        // unset the owning side of the relation if necessary
        if ($deploymentRequest === null && $this->deploymentRequest !== null) {
            $this->deploymentRequest->setDeployment(null);
        }

        // set the owning side of the relation if necessary
        if ($deploymentRequest !== null && $deploymentRequest->getDeployment() !== $this) {
            $deploymentRequest->setDeployment($this);
        }

        $this->deploymentRequest = $deploymentRequest;

        return $this;
    }

    /**
     * @return Collection<int, Checklist>
     */
    public function getChecklists(): Collection
    {
        return $this->checklists;
    }

    public function addChecklist(Checklist $checklist): self
    {
        if (!$this->checklists->contains($checklist)) {
            $this->checklists->add($checklist);
            $checklist->setDeployment($this);
        }

        return $this;
    }

    public function removeChecklist(Checklist $checklist): self
    {
        if ($this->checklists->removeElement($checklist)) {
            // set the owning side to null (unless already changed)
            if ($checklist->getDeployment() === $this) {
                $checklist->setDeployment(null);
            }
        }

        return $this;
    }
}