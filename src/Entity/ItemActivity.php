<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\ItemActivityRepository;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;

#[ORM\Entity(repositoryClass: ItemActivityRepository::class)]
#[ORM\HasLifecycleCallbacks]
class ItemActivity extends AuditedEntity
{

    use UuidEntityTrait;
    #[ORM\ManyToOne(inversedBy: 'itemActivities', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Item $item = null;

    #[ORM\ManyToOne(inversedBy: 'itemActivities', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Activity $activity = null;

    #[ORM\ManyToOne(inversedBy: 'itemActivities', cascade: ['persist', 'remove'])]
    private ?RunItem $runItem = null;

    public function __construct(Item $item, Activity $activity)
    {
        $this->item = $item;
        $this->activity = $activity;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(?Activity $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    public function getRunItem(): ?RunItem
    {
        return $this->runItem;
    }

    public function setRunItem(?RunItem $runItem): self
    {
        $this->runItem = $runItem;

        return $this;
    }

    public function getActionable(): Actionable
    {
        return $this->runItem ?: $this->activity;
    }

    public function displayTitle(): ?string
    {
        return '';
    }

    #[ORM\PreRemove]
    public function cleanupBeforeRemove()
    {
        if ($this->runItem && $this->runItem->getStatus() == 'New' &&
            $this->runItem->getItemActivities()->count() == 1) {
            $this->activity->removeRunItem($this->runItem);
            $this->runItem = null;
        }
    }
}