<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\ComponentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ComponentRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(
        fields: ['ecosystem', 'name'],
        errorPath: 'name',
    )]
#[UniqueEntity(
        fields: ['ecosystem', 'identifier'],
        errorPath: 'identifier',
    )]
class Component extends AuditedEntity
{

    use UuidEntityTrait;
    #[ORM\ManyToOne(inversedBy: 'components', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Ecosystem $ecosystem = null;

    #[ORM\Column(length: 30)]
    #[Assert\NotBlank]
    #[Assert\Length(
            min: 2,
            max: 30,
            minMessage: 'Identifier must be at least {{ limit }} characters long',
            maxMessage: 'Identifier cannot be longer than {{ limit }} characters',
        )]
    #[Assert\Regex(
            pattern: '/^[a-z][a-z0-9_-]*$/i',
            message: 'Must be alphanumeric, dashes and underscores only (the first character must be alphabet)'
        )]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\When(
            expression: 'this.getEcosystem().getLayers() != []',
            constraints: [
            new Assert\NotBlank,
            ])]
    private ?string $layer = null;

    #[ORM\OneToMany(mappedBy: 'component', targetEntity: ActivityTemplate::class, orphanRemoval: true)]
    #[ORM\OrderBy(['priority' => 'DESC'])]
    private Collection $activityTemplates;

    public function __construct(Ecosystem $ecosystem, ?string $identifier)
    {
        $this->ecosystem = $ecosystem;
        $this->identifier = $identifier;
        $this->activityTemplates = new ArrayCollection();
    }

    public function getEcosystem(): ?Ecosystem
    {
        return $this->ecosystem;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLayer(): ?string
    {
        return $this->layer;
    }

    public function setLayer(?string $layer): self
    {
        $this->layer = $layer;

        return $this;
    }

    /**
     * @return Collection<int, ActivityTemplate>
     */
    public function getActivityTemplates(): Collection
    {
        return $this->activityTemplates;
    }

    public function addActivityTemplate(ActivityTemplate $activityTemplate): self
    {
        if (!$this->activityTemplates->contains($activityTemplate)) {
            $this->activityTemplates->add($activityTemplate);
            $activityTemplate->setComponent($this);
        }

        return $this;
    }

    public function removeActivityTemplate(ActivityTemplate $activityTemplate): self
    {
        if ($this->activityTemplates->removeElement($activityTemplate)) {
            // set the owning side to null (unless already changed)
            if ($activityTemplate->getComponent() === $this) {
                $activityTemplate->setComponent(null);
            }
        }

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->name;
    }

    public function canDelete(): bool
    {
        return $this->activityTemplates->count() == 0;
    }
}