<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\ItemStatusLogRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;

#[ORM\Entity(repositoryClass: ItemStatusLogRepository::class)]
#[ORM\HasLifecycleCallbacks]
class ItemStatusLog extends AuditedEntity
{

    use UuidEntityTrait;
    #[ORM\ManyToOne(inversedBy: 'statusLogs')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Item $item = null;

    #[ORM\Column(length: 100)]
    private ?string $status = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $oldStatus = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $remark = null;

    public function __construct(?Item $item, ?string $status, ?string $oldStatus)
    {
        $this->item = $item;
        $this->status = $status;
        $this->oldStatus = $oldStatus;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getOldStatus(): ?string
    {
        return $this->oldStatus;
    }

    public function getRemark(): ?string
    {
        return $this->remark;
    }

    public function setRemark(string $remark): self
    {
        $this->remark = $remark;

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->status;
    }
}