<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\ActivityFileRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Type\FileWrapper;
use Ramsey\Uuid\Uuid;

#[ORM\Entity(repositoryClass: ActivityFileRepository::class)]
#[ORM\HasLifecycleCallbacks]
class ActivityFile extends AuditedEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private ?string $id = null;

    #[ORM\ManyToOne(inversedBy: 'files')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Activity $activity = null;

    #[ORM\Column(length: 255)]
    private ?string $identifier = null;

    #[ORM\Column(type: 'file')]
    private ?FileWrapper $file = null;

    public function __construct(?string $id = null)
    {
        $this->id = $id;
        if (!$this->id) {
            $this->id = Uuid::uuid7(new DateTime)->toString();
        }
    }

    public function clone(): self
    {
        $newActivityFile = new self();
        $newActivityFile->setIdentifier($this->getIdentifier());
        $newActivityFile->setActivity($this->getActivity());
        $newActivityFile->setFile($this->getFile());
        return $newActivityFile;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(?Activity $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getFile(): ?FileWrapper
    {
        return $this->file;
    }

    public function setFile(?FileWrapper $file): self
    {
        $this->file = $file;
        $this->identifier = $file->getName();
        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->file->getName();
    }
}