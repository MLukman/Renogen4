<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\SquadRepository;
use App\Security\Authorization\SecuredAccessInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
use MLukman\DoctrineHelperBundle\Type\ImageWrapper;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SquadRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(
        fields: ['ecosystem', 'name'],
        errorPath: 'name',
    )]
#[UniqueEntity(
        fields: ['ecosystem', 'identifier'],
        errorPath: 'identifier',
    )]
class Squad extends AuditedEntity implements SecuredAccessInterface
{

    use UuidEntityTrait;
    #[ORM\ManyToOne(inversedBy: 'squads', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Ecosystem $ecosystem = null;

    #[ORM\Column(length: 30)]
    #[Assert\NotBlank]
    #[Assert\Length(
            min: 2,
            max: 30,
            minMessage: 'Identifier must be at least {{ limit }} characters long',
            maxMessage: 'Identifier cannot be longer than {{ limit }} characters',
        )]
    #[Assert\Regex(
            pattern: '/^[a-z][a-z0-9_-]*$/i',
            message: 'Must be alphanumeric, dashes and underscores only (the first character must be alphabet)'
        )]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'squad', targetEntity: Item::class, orphanRemoval: true)]
    private Collection $items;

    #[ORM\OneToMany(mappedBy: 'squad', targetEntity: Membership::class, orphanRemoval: true, indexBy: 'userId',
            cascade: ['persist', 'remove'])]
    #[ORM\OrderBy(['rolesort' => 'DESC', 'created' => 'ASC'])]
    private Collection $memberships;

    #[ORM\Column(type: Types::ARRAY)]
    private array $categories = [
        'Bug Fix',
        'Enhancement',
        'New Feature',
    ];

    #[ORM\Column(type: 'image', nullable: true)]
    private ?ImageWrapper $icon = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $decommissioned = null;

    public function __construct(Ecosystem $ecosystem, ?string $identifier)
    {
        $this->ecosystem = $ecosystem;
        $this->identifier = $identifier;
        $this->items = new ArrayCollection();
        $this->memberships = new ArrayCollection();
    }

    public function getEcosystem(): ?Ecosystem
    {
        return $this->ecosystem;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Item>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
            $item->setSquad($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getSquad() === $this) {
                $item->setSquad(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Membership>
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    public function addMembership(Membership $membership): self
    {
        if (!$this->memberships->contains($membership)) {
            $this->memberships->add($membership);
            //$membership->setSquad($this);
        }

        return $this;
    }

    public function removeMembership(Membership $membership): self
    {
        if ($this->memberships->removeElement($membership)) {
            // set the owning side to null (unless already changed)
            if ($membership->getSquad() === $this) {
                //$membership->setSquad(null);
            }
        }

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->name;
    }

    public function getCategories(): array
    {
        return $this->categories;
    }

    public function setCategories(array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function isRoleAllowed(string $role, string $attribute): bool
    {
        return false;
    }

    public function isUsernameAllowed(string $username, string $attribute): bool
    {
        return (($membership = $this->memberships->get($username)) &&
            ($membership->getRole() == $attribute || $attribute == 'any'));
    }

    public function getIcon(): ?ImageWrapper
    {
        return $this->icon;
    }

    public function setIcon(?ImageWrapper $icon): self
    {
        $this->icon = $icon;
        if ($this->icon) {
            $this->icon->resize(72);
        }
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function canDelete(): bool
    {
        return $this->items->count() == 0;
    }

    public function getStarCount(): int
    {
        return $this->memberships->matching(Criteria::create()->where(Criteria::expr()->eq('favourite', true)))->count();
    }

    public function getDecommissioned(): ?\DateTimeInterface
    {
        return $this->decommissioned;
    }

    public function setDecommissioned(?\DateTimeInterface $decommissioned): static
    {
        $this->decommissioned = $decommissioned;

        return $this;
    }

}