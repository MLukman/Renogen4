<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\ChecklistUpdateRepository;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ChecklistUpdateRepository::class)]
#[ORM\HasLifecycleCallbacks]
class ChecklistUpdate  extends AuditedEntity
{
    use UuidEntityTrait;

    #[ORM\ManyToOne(inversedBy: 'updates')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Checklist $checklist = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $comment = null;

    #[ORM\Column(length: 30)]
    #[Assert\NotBlank]
    private ?string $status = null;

    public function __construct(?Checklist $checklist)
    {
        $this->checklist = $checklist;
    }

    public function getChecklist(): ?Checklist
    {
        return $this->checklist;
    }

    public function setChecklist(?Checklist $checklist): self
    {
        $this->checklist = $checklist;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->comment;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
