<?php

namespace App\Entity;

use App\Repository\ActivityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ActivityRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Activity extends Actionable
{
    #[ORM\ManyToOne(inversedBy: 'activities')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Deployment $deployment = null;

    #[ORM\ManyToOne(inversedBy: 'activities')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    protected ?ActivityTemplate $template = null;

    #[ORM\OneToMany(mappedBy: 'activity', targetEntity: RunItem::class, orphanRemoval: true)]
    #[ORM\OrderBy(['created' => 'ASC'])]
    private Collection $runItems;

    /**
     * @var Collection<int,ItemActivity>
     */
    #[ORM\OneToMany(mappedBy: 'activity', targetEntity: ItemActivity::class, orphanRemoval: true)]
    private Collection $itemActivities;

    #[ORM\OneToMany(mappedBy: 'activity', targetEntity: ActivityFile::class, orphanRemoval: true, indexBy: 'identifier',
            cascade: ['persist', 'remove'])]
    private Collection $files;

    public function __construct(ActivityTemplate $template,
                                Deployment $deployment)
    {
        $this->deployment = $deployment;
        $this->template = $template;
        $this->runItems = new ArrayCollection();
        $this->itemActivities = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    public function getDeployment(): ?Deployment
    {
        return $this->deployment;
    }

    public function setDeployment(?Deployment $deployment): self
    {
        $this->deployment = $deployment;

        return $this;
    }

    /**
     * @return Collection<int, RunItem>
     */
    public function getRunItems(): Collection
    {
        return $this->runItems;
    }

    public function addRunItem(RunItem $runItem): self
    {
        if (!$this->runItems->contains($runItem)) {
            $this->runItems->add($runItem);
            //$runItem->setActivity($this);
        }

        return $this;
    }

    public function removeRunItem(RunItem $runItem): self
    {
        if ($this->runItems->removeElement($runItem)) {
            // set the owning side to null (unless already changed)
            if ($runItem->getActivity() === $this) {
                //$runItem->setActivity(null);
            }
        }

        return $this;
    }

    public function isEditable(): bool
    {
        return $this->status != Item::STATUS_COMPLETED;
    }

    public function setParameters(?array $parameters): Actionable
    {
        foreach ($this->itemActivities as $itemActivity) {
            $runItem = $itemActivity->getRunItem();
            if (!$runItem ||
                $runItem->getStatus() == 'Completed' ||
                $runItem->getParameters() == $parameters) {
                continue;
            }
            if ($runItem->getStatus() == 'Failed') {
                // since run item has failed & parameters have been updated, set null so that it will create new run item upon approval
                foreach ($runItem->getItemActivities() as $itemActivity) {
                    $itemActivity->setRunItem(null);
                }
            } else {
                $runItem->setParameters($parameters);
            }
        }
        return parent::setParameters($parameters);
    }

    /**
     * @return Collection<int, ItemActivity>
     */
    public function getItemActivities(): Collection
    {
        return $this->itemActivities;
    }

    public function addItemActivity(ItemActivity $itemActivity): self
    {
        if (!$this->itemActivities->contains($itemActivity)) {
            $this->itemActivities->add($itemActivity);
            $itemActivity->setActivity($this);
        }

        return $this;
    }

    public function removeItemActivity(ItemActivity $itemActivity): self
    {
        if ($this->itemActivities->removeElement($itemActivity)) {
            // set the owning side to null (unless already changed)
            if ($itemActivity->getActivity() === $this) {
                $itemActivity->setActivity(null);
            }
        }

        return $this;
    }

    public function getTemplate(): ?ActivityTemplate
    {
        return $this->template;
    }

    /**
     * @return Collection<int, ActivityFile>
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(ActivityFile $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files->add($file);
            $file->setActivity($this);
        }

        return $this;
    }

    public function removeFile(ActivityFile $file): self
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getActivity() === $this) {
                $file->setActivity(null);
            }
        }

        return $this;
    }
}