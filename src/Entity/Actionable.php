<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\MappedSuperclass]
abstract class Actionable extends AuditedEntity
{

    use \MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
    #[ORM\Column(nullable: true)]
    protected array $parameters = [];

    #[ORM\Column(length: 255)]
    protected ?string $status = 'New';

    abstract public function getTemplate(): ?ActivityTemplate;

    public function &getParameters(): array
    {
        return $this->parameters;
    }

    public function setParameters(?array $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->getTemplate()->displayTitle();
    }
}