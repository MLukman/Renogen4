<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\EnvironmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: EnvironmentRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(
        fields: ['ecosystem', 'name'],
        errorPath: 'name',
    )]
#[UniqueEntity(
        fields: ['ecosystem', 'identifier'],
        errorPath: 'identifier',
    )]
class Environment extends AuditedEntity
{

    use UuidEntityTrait;
    #[ORM\ManyToOne(inversedBy: 'environments', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Ecosystem $ecosystem = null;

    #[ORM\Column(length: 30)]
    #[Assert\NotBlank]
    #[Assert\Length(
            min: 2,
            max: 30,
            minMessage: 'Identifier must be at least {{ limit }} characters long',
            maxMessage: 'Identifier cannot be longer than {{ limit }} characters',
        )]
    #[Assert\Regex(
            pattern: '/^[a-z][a-z0-9_-]*$/i',
            message: 'Must be alphanumeric, dashes and underscores only (the first character must be alphabet)'
        )]
    private ?string $identifier = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'environment', targetEntity: ActivityTemplate::class, orphanRemoval: true)]
    private Collection $activityTemplates;

    #[ORM\OneToMany(mappedBy: 'environment', targetEntity: Deployment::class, orphanRemoval: true, indexBy: 'identifier')]
    #[ORM\OrderBy(['executeDate' => 'ASC'])]
    private Collection $deployments;

    #[ORM\OneToMany(mappedBy: 'environment', targetEntity: DeploymentRequest::class, orphanRemoval: true, indexBy: 'identifier')]
    #[ORM\OrderBy(['executeDate' => 'DESC'])]
    private Collection $deploymentRequests;

    #[ORM\Column]
    #[Assert\Positive]
    private ?int $level = 0;

    public function __construct(Ecosystem $ecosystem, ?string $identifier)
    {
        $this->ecosystem = $ecosystem;
        $this->identifier = $identifier;
        $this->activityTemplates = new ArrayCollection();
        $this->deployments = new ArrayCollection();
        $this->deploymentRequests = new ArrayCollection();
    }

    public function getEcosystem(): ?Ecosystem
    {
        return $this->ecosystem;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, ActivityTemplate>
     */
    public function getActivityTemplates(): Collection
    {
        return $this->activityTemplates;
    }

    public function addActivityTemplate(ActivityTemplate $activityTemplate): self
    {
        if (!$this->activityTemplates->contains($activityTemplate)) {
            $this->activityTemplates->add($activityTemplate);
            $activityTemplate->setEnvironment($this);
        }

        return $this;
    }

    public function removeActivityTemplate(ActivityTemplate $activityTemplate): self
    {
        if ($this->activityTemplates->removeElement($activityTemplate)) {
            // set the owning side to null (unless already changed)
            if ($activityTemplate->getEnvironment() === $this) {
                $activityTemplate->setEnvironment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Deployment>
     */
    public function getDeployments(): Collection
    {
        return $this->deployments;
    }

    public function addDeployment(Deployment $deployment): self
    {
        if (!$this->deployments->contains($deployment)) {
            $this->deployments->add($deployment);
            $deployment->setEnvironment($this);
        }

        return $this;
    }

    public function removeDeployment(Deployment $deployment): self
    {
        if ($this->deployments->removeElement($deployment)) {
            // set the owning side to null (unless already changed)
            if ($deployment->getEnvironment() === $this) {
                $deployment->setEnvironment(null);
            }
        }

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->name;
    }

    public function canDelete(): bool
    {
        return $this->deployments->count() == 0;
    }

    /**
     * @return Collection<int, DeploymentRequest>
     */
    public function getDeploymentRequests(): Collection
    {
        return $this->deploymentRequests;
    }

    public function addDeploymentRequest(DeploymentRequest $deploymentRequest): self
    {
        if (!$this->deploymentRequests->contains($deploymentRequest)) {
            $this->deploymentRequests->add($deploymentRequest);
            $deploymentRequest->setEnvironment($this);
        }

        return $this;
    }

    public function removeDeploymentRequest(DeploymentRequest $deploymentRequest): self
    {
        if ($this->deploymentRequests->removeElement($deploymentRequest)) {
            // set the owning side to null (unless already changed)
            if ($deploymentRequest->getEnvironment() === $this) {
                $deploymentRequest->setEnvironment(null);
            }
        }

        return $this;
    }

    /**
     * @param $count Max count of deployments
     * @return Collection<int, Deployment>
     */
    public function upcoming(int $count = 10): Collection
    {
        return self::filterUpcomingDateOnly($this->deployments, 'executeDate', 'expectedCompletionDate', $count);
    }

    /**
     * @param $count Max count of deployments
     * @return Collection<int, Deployment>
     */
    public function past(int $count = 10): Collection
    {
        return self::filterPastDateOnly($this->deployments, 'executeDate', 'expectedCompletionDate', $count);
    }
}