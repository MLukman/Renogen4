<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\ChecklistRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ChecklistRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Checklist extends AuditedEntity implements \App\Security\Authorization\SecuredAccessInterface
{

    use UuidEntityTrait;
    #[ORM\ManyToOne(inversedBy: 'checklists')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Deployment $deployment = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $title = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\NotBlank]
    private ?DateTimeInterface $start = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $end = null;

    #[ORM\Column(length: 30)]
    #[Assert\NotBlank]
    private ?string $status = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'checklists')]
    #[ORM\OrderBy(['fullname' => 'ASC'])]
    private Collection $pics;

    #[ORM\OneToMany(mappedBy: 'checklist', targetEntity: ChecklistUpdate::class, orphanRemoval: true,
            cascade: ['persist', 'remove'])]
    #[ORM\OrderBy(['created' => 'ASC'])]
    private Collection $updates;

    public function __construct(Deployment $deployment)
    {
        $this->deployment = $deployment;
        $this->pics = new ArrayCollection();
        $this->updates = new ArrayCollection();
    }

    public function getDeployment(): ?Deployment
    {
        return $this->deployment;
    }

    public function setDeployment(?Deployment $deployment): self
    {
        $this->deployment = $deployment;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStart(): ?DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(?DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getPics(): Collection
    {
        return $this->pics;
    }

    public function addPic(User $pic): self
    {
        if (!$this->pics->contains($pic)) {
            $this->pics->add($pic);
        }

        return $this;
    }

    public function removePic(User $pic): self
    {
        $this->pics->removeElement($pic);

        return $this;
    }

    /**
     * @return Collection<int, ChecklistUpdate>
     */
    public function getUpdates(): Collection
    {
        return $this->updates;
    }

    public function addUpdate(ChecklistUpdate $update): self
    {
        if (!$this->updates->contains($update)) {
            $this->updates->add($update);
            $update->setChecklist($this);
        }

        return $this;
    }

    public function removeUpdate(ChecklistUpdate $update): self
    {
        if ($this->updates->removeElement($update)) {
            // set the owning side to null (unless already changed)
            if ($update->getChecklist() === $this) {
                $update->setChecklist(null);
            }
        }

        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->title;
    }

    public function isRoleAllowed(string $role, string $attribute): bool
    {
        return $this->deployment->isRoleAllowed($role, $attribute);
    }

    public function isUsernameAllowed(string $username, string $attribute): bool
    {
        switch ($attribute) {
            case 'update':
                if ($this->createdBy->getId() == $username) {
                    return true;
                }
                foreach ($this->pics as $user) {
                    if ($user->getId() == $username) {
                        return true;
                    }
                }
                break;

            case 'delete':
            case 'edit_title':
                if ($this->createdBy->getId() == $username) {
                    return true;
                }
                break;
        }
        return $this->deployment->isUsernameAllowed($username, 'approval');
    }

    public function canDelete(): bool
    {
        return true;
    }
}