<?php

namespace App\Entity;

use App\Base\AuditedEntity;
use App\Repository\MembershipRepository;
use Doctrine\ORM\Mapping as ORM;
use MLukman\DoctrineHelperBundle\Trait\UuidEntityTrait;

#[ORM\Entity(repositoryClass: MembershipRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Membership extends AuditedEntity
{

    use UuidEntityTrait;
    public const ROLESEQUENCE = [
        'none',
        'view',
        'execute',
        'entry',
        'review',
        'approval',
    ];

    #[ORM\Column(length: 15)]
    private ?string $userId = null;

    #[ORM\ManyToOne(inversedBy: 'memberships', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?User $member = null;

    #[ORM\ManyToOne(inversedBy: 'memberships', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private ?Squad $squad = null;

    #[ORM\Column(length: 255)]
    private ?string $role = null;

    #[ORM\Column]
    private ?int $rolesort = null;

    #[ORM\Column(options: ['default' => 0])]
    private ?bool $favourite = false;

    public function __construct(?User $member, ?Squad $squad, ?string $role)
    {
        $this->setMember($member);
        $this->setSquad($squad);
        $this->setRole($role);
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function getMember(): ?User
    {
        return $this->member;
    }

    protected function setMember(?User $member): self
    {
        $this->member = $member;
        $this->userId = $this->member ? $this->member->getId() : null;
        return $this;
    }

    public function getSquad(): ?Squad
    {
        return $this->squad;
    }

    protected function setSquad(?Squad $squad): self
    {
        $this->squad = $squad;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;
        $this->rolesort = array_search($role, self::ROLESEQUENCE, true) ?: 0;
        return $this;
    }

    public function displayTitle(): ?string
    {
        return $this->member->displayTitle().' @ '.$this->squad->displayTitle();
    }

    public function getRolesort(): ?int
    {
        return $this->rolesort;
    }

    public function isFavourite(): ?bool
    {
        return $this->favourite;
    }

    public function setFavourite(?bool $favourite): self
    {
        $this->favourite = $favourite;

        return $this;
    }
}