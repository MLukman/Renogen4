<?php

namespace App\Repository;

use App\Entity\EcosystemAdmin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EcosystemAdmin>
 *
 * @method EcosystemAdmin|null find($id, $lockMode = null, $lockVersion = null)
 * @method EcosystemAdmin|null findOneBy(array $criteria, array $orderBy = null)
 * @method EcosystemAdmin[]    findAll()
 * @method EcosystemAdmin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcosystemAdminRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EcosystemAdmin::class);
    }

//    /**
//     * @return EcosystemAdmin[] Returns an array of EcosystemAdmin objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EcosystemAdmin
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
