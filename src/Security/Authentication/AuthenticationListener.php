<?php

namespace App\Security\Authentication;

use App\Entity\Login;
use App\Security\ReCaptchaUtil;
use App\Service\AuditLogger;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use ReflectionClass;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\FlashBagAwareSessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class AuthenticationListener implements AuthenticationEntryPointInterface, EventSubscriberInterface
{
    public const USER_ENTITY_CLASS = Login::class;
    public const MESSAGE_WELCOME = 'Welcome, %s.';
    public const MESSAGE_WELCOME_BACK = 'Welcome back, %s. Your last login was on %s.';

    protected array $verifiedCaptchas = [];

    public function __construct(protected EntityManagerInterface $entityManager,
                                protected Security $security,
                                protected AuditLogger $auditLogger,
                                protected CsrfTokenManagerInterface $csrfTokenManager,
                                protected ReCaptchaUtil $recaptcha,
                                protected RequestStack $requestStack,
                                protected UrlGeneratorInterface $urlGenerator)
    {

    }

    public function start(Request $request,
                          AuthenticationException $authException = null): Response
    {
        $request->getSession()->set('_route_before_login', $request->attributes->get('_route'));
        $request->getSession()->set('_request_uri_before_login', $request->getRequestUri());
        return new RedirectResponse($this->urlGenerator->generate('app_login'));
    }

    public static function getSubscribedEvents(): array
    {
        return [
            LoginSuccessEvent::class => 'onLogin',
            LogoutEvent::class => 'onLogout',
        ];
    }

    public function loginFormPreAuthenticationChecks(Request $request): void
    {
        $csrf = $request->request->get('csrf_token');

        // check CSRF
        if (!$this->csrfTokenManager->isTokenValid(new CsrfToken('authenticate', $csrf))) {
            throw new InvalidCsrfTokenException('Invalid CSRF');
        }

        // check recaptcha
        if ($this->recaptcha->isEnabled()) {
            $resp = $this->recaptcha->verify($request->request->get('g-recaptcha-response'));
            if (!$resp->isSuccess()) {
                throw new CustomUserMessageAuthenticationException('reCAPTCHA error: '.join(", ", $resp->getErrorCodes()));
            }
        }
    }

    public function onLogin(LoginSuccessEvent $event): void
    {
        $user = ($securityUser = $this->security->getUser()) ?
            $this->entityManager->getRepository(self::USER_ENTITY_CLASS)->find($securityUser->getUsername())
                : null;

        $session = $event->getRequest()->getSession();

        if ($user) {
            $name = $user->getUser() ? $user->getUser()->getFullname() :
                ($user->getFullname() ?: $user->getEmail());
            // update last login
            if (($lastlogin = $user->getLastLogin())) {
                $welcome = sprintf(self::MESSAGE_WELCOME_BACK, $name, $lastlogin->format('Y-m-d h:i A'));
            } else {
                $welcome = sprintf(self::MESSAGE_WELCOME, $name);
            }
            if ($session instanceof FlashBagAwareSessionInterface) {
                $session->getFlashBag()->add('info', $welcome);
                $this->auditLogger->log($user, 'LOGIN');
            }
            $user->setLastLogin(new DateTime());

            // read user's language
            if ($user && ($userLanguage = $user->getLanguage())) {
                $session->set('_locale', $userLanguage);
            }

            $this->entityManager->flush();
        }

        // redirect
        if (($redirect = $session->get('_request_uri_before_login'))) {
            $session->remove('_request_uri_before_login');
            $event->setResponse(new RedirectResponse($redirect, RedirectResponse::HTTP_SEE_OTHER));
        }
    }

    public function onLogout(LogoutEvent $event): void
    {
        // redirect if there is redirect_uri query parameter
        $this->auditLogger->log($event->getToken()->getUser(), 'LOGOUT');
        $request = $event->getRequest();
        if (($redirect_uri = $request->query->get('redirect_uri'))) {
            $response = new RedirectResponse(
                $redirect_uri, RedirectResponse::HTTP_SEE_OTHER
            );
            $event->setResponse($response);
        }
    }

    public function newUserEntity(string $method, string $credential,
                                  string $username = null): UserEntity
    {
        $refl = new ReflectionClass(self::USER_ENTITY_CLASS);
        return $refl->newInstance($method, $credential, $username);
    }

    public function queryUserEntity(string $method, string $criteriaField,
                                    string $criteriaValue): ?UserEntity
    {
        $authRepo = $this->entityManager->getRepository(self::USER_ENTITY_CLASS);
        if (!in_array($criteriaField, ['username', 'credential', 'resetCode', 'email'])) {
            throw new InvalidArgumentException('Cannot query '.self::USER_ENTITY_CLASS.' using '.$criteriaValue);
        }
        return $authRepo->findOneBy([
                ($criteriaField) => $criteriaValue,
                'method' => $method
        ]);
    }
}