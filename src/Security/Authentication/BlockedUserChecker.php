<?php

namespace App\Security\Authentication;

use App\Entity\Login;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class BlockedUserChecker implements UserCheckerInterface
{

    public function checkPreAuth(UserInterface $login): void
    {
        if ($login instanceof Login && ($user = $login->getUser()) && $user->isBlocked()) {
            throw new CustomUserMessageAuthenticationException('Sorry but you have been blocked from logging in. Please contact an administrator if you think it is a mistake.');
        }
    }

    public function checkPostAuth(UserInterface $user): void
    {

    }
}