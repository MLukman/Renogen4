<?php

namespace App\Security\Authentication\OAuth2Authenticator;

use App\Security\Authentication\UserEntity;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * Description of DigitalmeVisitor
 *
 * @author Lukman
 */
class DigitalmeVisitor implements VisitorInterface
{

    public function prepareNewUserFromResourceOwner(UserEntity $user,
                                                    ResourceOwnerInterface $resourceOwner)
    {
        if ($resourceOwner instanceof DigitalmeUser) {
            $user->setFullname($resourceOwner->getName());
            $user->setEmail($resourceOwner->getEmail());
        }
    }

    public function prepareRedirectOptions(array &$options,
                                           OAuth2ClientInterface $client)
    {

    }
}