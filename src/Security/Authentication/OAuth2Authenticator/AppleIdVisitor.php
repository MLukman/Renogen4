<?php

namespace App\Security\Authentication\OAuth2Authenticator;

use App\Security\Authentication\UserEntity;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use League\OAuth2\Client\Provider\AppleResourceOwner;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * Description of AppleIdVisitor
 *
 * @author Lukman
 */
class AppleIdVisitor implements VisitorInterface
{

    public function prepareNewUserFromResourceOwner(UserEntity $user,
                                                    ResourceOwnerInterface $resourceOwner)
    {
        if ($resourceOwner instanceof AppleResourceOwner) {
            $user->setFullname($resourceOwner->getFirstName().' '.$resourceOwner->getLastName());
            $user->setEmail($resourceOwner->getEmail());
        }
    }

    public function prepareRedirectOptions(array &$options,
                                           OAuth2ClientInterface $client)
    {

    }
}