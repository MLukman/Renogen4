<?php

namespace App\Security\Authentication\OAuth2Authenticator;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class DigitalmeProvider extends AbstractProvider
{
    const ACCESS_TOKEN_RESOURCE_OWNER_ID = 'sub';

    private bool $useStaging = false;

    public function __construct($options = [], array $collaborators = [])
    {
        parent::__construct($options, $collaborators);
        if (isset($options['use_staging']) && $options['use_staging']) {
            $this->useStaging = true;
        }
    }

    protected function checkResponse(ResponseInterface $response, $data): void
    {
        if (isset($data['status']) && $data['status']['type'] == 'NOK') {
            throw new IdentityProviderException('DigitalMe is having problems handling your login', 503, $data);
        }
        if (isset($data['error_hint'])) {
            throw new IdentityProviderException($data['error_hint'], 503, $data);
        }
    }

    protected function createResourceOwner(array $response, AccessToken $token): ResourceOwnerInterface
    {
        return new DigitalmeUser($response['sub'], $response['nickname'], $response['email']);
    }

    protected function getDefaultScopes(): array
    {
        return ['openid profile email'];
    }

    public function getBaseAccessTokenUrl(array $params): string
    {
        return $this->getBaseUrl().'/oauth2/token';
    }

    public function getBaseAuthorizationUrl(): string
    {
        return $this->getBaseUrl().'/oauth2/authorize';
    }

    public function getResourceOwnerDetailsUrl(AccessToken $token): string
    {
        return $this->getBaseUrl().'/userinfo';
    }

    protected function getBaseUrl(): string
    {
        return $this->useStaging ?
            'https://staging-auth.digitalme.my' :
            'https://auth.digitalme.my';
    }

    protected function getAccessTokenRequest(array $params): RequestInterface
    {
        unset($params['client_secret']);
        return parent::getAccessTokenRequest($params);
    }

    protected function getAuthorizationHeaders($token = null): array
    {
        return [
            'Authorization' => "Bearer $token",
            'Accept' => 'application/json',
        ];
    }
}