<?php

namespace App\Security\Authentication\OAuth2Authenticator;

use App\Security\Authentication\UserEntity;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * Description of GoogleVisitor
 *
 * @author Lukman
 */
class GoogleVisitor implements VisitorInterface
{

    public function prepareNewUserFromResourceOwner(UserEntity $user,
                                                    ResourceOwnerInterface $resourceOwner)
    {
        if ($resourceOwner instanceof GoogleUser) {
            $user->setFullname($resourceOwner->getName());
            $user->setEmail($resourceOwner->getEmail());
        }
    }

    public function prepareRedirectOptions(array &$options,
                                           OAuth2ClientInterface $client)
    {
        if ($client instanceof \League\OAuth2\Client\Provider\Google) {
            $options += ['prompt' => 'select_account'];
        }
    }
}