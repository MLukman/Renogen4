<?php

namespace App\Security\Authentication\OAuth2Authenticator;

use App\Security\Authentication\UserEntity;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

/**
 *
 * @author Lukman
 */
#[AutoconfigureTag('oauth2.authenticator.visitor')]
interface VisitorInterface
{

    public function prepareRedirectOptions(array &$options,
                                           OAuth2ClientInterface $client);

    public function prepareNewUserFromResourceOwner(UserEntity $user,
                                                    ResourceOwnerInterface $resourceOwner);
}