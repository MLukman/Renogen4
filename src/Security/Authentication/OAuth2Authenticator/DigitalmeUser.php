<?php

namespace App\Security\Authentication\OAuth2Authenticator;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class DigitalmeUser implements ResourceOwnerInterface
{

    public function __construct(private string $id, private string $name,
                                private string $email)
    {

    }

    public function getId(): mixed
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
        ];
    }
}