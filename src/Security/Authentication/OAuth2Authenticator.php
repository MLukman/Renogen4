<?php

namespace App\Security\Authentication;

use App\Entity\Login;
use App\Service\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Security\Authenticator\OAuth2Authenticator as KnpUOAuth2Authenticator;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use MLukman\DoctrineHelperBundle\Service\ObjectValidator;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class OAuth2Authenticator extends KnpUOAuth2Authenticator
{
    const DEFAULT_REDIRECT_ROUTE = 'app_home';
    const OAUTH_REDIRECT_ROUTE = 'oauth2_connect_check';

    use TargetPathTrait;

    public function __construct(private AuthenticationListener $authListener,
                                private ClientRegistry $clientRegistry,
                                private EntityManagerInterface $entityManager,
                                private ObjectValidator $validator,
                                private RouterInterface $router,
                                private Security $security,
                                protected AuditLogger $auditLogger,
                                #[TaggedIterator('oauth2.authenticator.visitor')] private iterable $visitors)
    {

    }

    public function supports(Request $request): ?bool
    {
        // continue ONLY if the current ROUTE matches the check ROUTE
        return $request->attributes->get('_route') === self::OAUTH_REDIRECT_ROUTE;
    }

    public function getRedirectionToProvider(Request $request, string $client,
                                             ?string $redirect_after_login = null): ?RedirectResponse
    {
        if ($request->query->get('_remember_me')) {
            $request->getSession()->set('_remember_me', true);
        }
        $redirect = $redirect_after_login ?: $this->router->generate(self::DEFAULT_REDIRECT_ROUTE, referenceType: Router::ABSOLUTE_URL);
        $options = [
            'state' => rtrim(strtr(base64_encode($redirect), '+/', '-_'), '='),
        ];
        $clientObj = $this->clientRegistry->getClient($client);
        foreach ($this->visitors as $visitor) {
            $visitor->prepareRedirectOptions($options, $clientObj);
        }
        return $clientObj->redirect([], $options);
    }

    public function authenticate(Request $request): Passport
    {
        $clientId = $request->attributes->get('_route_params')['client'];
        $client = $this->clientRegistry->getClient($clientId);
        $accessToken = $this->fetchAccessToken($client);
        $oauth2User = $client->fetchUserFromToken($accessToken);
        $authenticatedUser = $this->createOrGetUserInterface($clientId, $oauth2User);

        if (($sessionUser = $this->security->getUser())) {
            $this->handleExistingUserSession($sessionUser, $authenticatedUser);
        }

        $rememberMe = new RememberMeBadge();
        if ($request->getSession()->get('_remember_me')) {
            $rememberMe->enable();
            $request->getSession()->remove('_remember_me');
        }
        return new SelfValidatingPassport(
            new UserBadge($accessToken->getToken(), fn() => $authenticatedUser),
            [$rememberMe]
        );
    }

    protected function createOrGetUserInterface(string $clientId,
                                                ResourceOwnerInterface $oauth2User): UserInterface
    {
        $uid = $oauth2User->getId();

        if (!($user = $this->authListener->queryUserEntity($clientId, 'credential', $uid))) {
            // create new user
            $user = $this->authListener->newUserEntity($clientId, $uid);

            // let visitors populate it
            foreach ($this->visitors as $visitor) {
                $visitor->prepareNewUserFromResourceOwner($user, $oauth2User);
            }

            // fallback if email is missing
            if (!$user->getEmail()) {
                $user->setEmail($user->getUserIdentifier());
            }

            $this->auditLogger->log($user, 'REGISTER');
        }

        $user->setAuthSession(bin2hex(random_bytes(16)));
        if (($errors = $this->validator->validate($user))) {
            $errorMessages = [];
            foreach ($errors as $errorArray) {
                foreach ($errorArray as $error) {
                    $errorMessages[] = $error;
                }
            }
            throw new \Exception(join(' ', $errorMessages));
        }
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    protected function handleExistingUserSession(UserInterface $sessionUser,
                                                 UserInterface $authenticatedUser)
    {
        /** @var Login $sessionUser */
        /** @var Login $authenticatedUser */
        if ($sessionUser != $authenticatedUser &&
            !$authenticatedUser->getUser() &&
            ($user = $sessionUser->getUser())) {
            $authenticatedUser->setUser($user);
            $this->entityManager->flush();
        }
    }

    public function onAuthenticationSuccess(Request $request,
                                            TokenInterface $token,
                                            string $firewallName): ?Response
    {
        return new RedirectResponse(
            $this->getTargetPath($request->getSession(), $firewallName) ?:
            base64_decode(strtr($request->query->get('state'), '-_', '+/')) ?:
            $this->router->generate(self::DEFAULT_REDIRECT_ROUTE));
    }

    public function onAuthenticationFailure(Request $request,
                                            AuthenticationException $exception): ?Response
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());
        return new Response($message, Response::HTTP_FORBIDDEN);
    }
}