<?php

namespace App\Security\Authentication;

use App\Entity\Login;
use App\Service\AuditLogger;
use Doctrine\ORM\EntityManagerInterface;
use MLukman\DoctrineHelperBundle\Service\ObjectValidator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\SecurityRequestAttributes;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * Description of PasswordAuthenticator
 *
 * @author Lukman
 */
class PasswordAuthenticator extends AbstractAuthenticator
{
    const DEFAULT_REDIRECT_ROUTE = 'app_home';
    const LOGIN_ROUTE = 'app_login';

    use TargetPathTrait;

    public function __construct(protected AuthenticationListener $authListener,
                                protected EntityManagerInterface $entityManager,
                                protected AuditLogger $auditLogger,
                                protected ObjectValidator $validator,
                                protected RouterInterface $router,
                                protected UserPasswordHasherInterface $passwordEncoder,
                                protected MailerInterface $mailer
    )
    {
        
    }

    public function authenticate(Request $request): Passport
    {
        $this->authListener->loginFormPreAuthenticationChecks($request);

        $username = $request->request->get('username');
        $password = $request->request->get('password');

        if (empty($username) || empty($password)) { // empty input
            throw new CustomUserMessageAuthenticationException('Both username and password are required.');
        }

        // fetch user authentication entity
        /** @var Login $user_auth */
        try {
            $user_auth = $this->authListener->queryUserEntity('password', 'username', $username);
        } catch (\Exception $ex) {
            throw new CustomUserMessageAuthenticationException($ex->getMessage());
        }

        if (!$user_auth) { // User not found
            throw new CustomUserMessageAuthenticationException('Invalid credentials');
        }

        if (($reset_code = $request->query->get('reset_code'))) {
            if ($reset_code != $user_auth->getResetCode()) {
                $this->auditLogger->log($user_auth, 'RESET_PASSWORD_FAILURE');
                throw new CustomUserMessageAuthenticationException('Invalid reset code.');
            }
            // reset password
            $user_auth->setPassword($this->passwordEncoder->hashPassword($user_auth, $password));
            $user_auth->setResetCode(null);
            $this->entityManager->flush();
            $this->entityManager->refresh($user_auth);
            $this->auditLogger->log($user_auth, 'RESET_PASSWORD_SUCCESS');
        } elseif (!$this->passwordEncoder->isPasswordValid($user_auth, $password)) {
            $this->auditLogger->log($user_auth, 'LOGIN_FAILURE');
            throw new CustomUserMessageAuthenticationException('Invalid credentials');
        }

        $user_auth->setAuthSession(bin2hex(random_bytes(16)));
        $this->entityManager->flush();

        return new SelfValidatingPassport(
            new UserBadge($user_auth->getUserIdentifier(), fn() => $user_auth),
            [new RememberMeBadge()]
        );
    }

    public function queryUserByUsername(string $username): ?UserEntity
    {
        return $this->authListener->queryUserEntity('password', 'username', $username);
    }

    public function registerNewUser(string $username, string $email): UserEntity|array
    {
        $login = $this->authListener->newUserEntity('password', $username, $username);
        $login->setEmail($email);
        if (($errors = $this->validator->validate($login))) {
            return $errors;
        }
        $this->entityManager->persist($login);
        $this->sendResetPasswordEmail($login, 'auth/email.registration.html.twig', 'Your Renogen Registration');
        return $login;
    }

    public function sendResetPasswordEmail(Login $login, string $twigTemplate,
                                           string $subject)
    {
        $login->setResetCode(md5($login->getEmail().':'.time()));
        $reset_url = $this->router->generate('app_login', ['reset_code' => $login->getResetCode()], RouterInterface::ABSOLUTE_URL);
        $email = (new TemplatedEmail())
            ->to($login->getEmail())
            ->htmlTemplate($twigTemplate)
            ->subject($subject)
            ->text("To login Renogen, go to $reset_url.")
            ->context(['username' => $login->getUsername(), 'reset_url' => $reset_url,]);
        $mailer_from = $_ENV['MAILER_FROM'] ?? 'renogen@dummy.net';
        if (!preg_match('/^([^<]*) <([^>]+)>$/', $mailer_from)) {
            $mailer_from = "Renogen <$mailer_from>";
        }
        $email->from($mailer_from);
        $this->mailer->send($email);
        $this->entityManager->flush();
        $this->auditLogger->log($login, 'RESET_PASSWORD_EMAIL');
    }

    public function onAuthenticationFailure(Request $request,
                                            AuthenticationException $exception): ?Response
    {
        $request->getSession()->set(SecurityRequestAttributes::AUTHENTICATION_ERROR, $exception);
        $request->getSession()->set(SecurityRequestAttributes::LAST_USERNAME, $request->get('username'));
        return null;
    }

    public function onAuthenticationSuccess(Request $request,
                                            TokenInterface $token,
                                            string $firewallName): ?Response
    {
        return new RedirectResponse(
            $this->getTargetPath($request->getSession(), $firewallName) ?:
            $this->router->generate(self::DEFAULT_REDIRECT_ROUTE));
    }

    public function supports(Request $request): ?bool
    {
        return $request->attributes->get('_route') === self::LOGIN_ROUTE &&
            $request->getMethod() == 'POST';
    }
}