<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\ProfileRequest;
use App\Entity\User;
use App\Security\Authentication\AuthenticationListener;
use App\Security\Authentication\OAuth2Authenticator;
use App\Security\Authentication\PasswordAuthenticator;
use App\Security\Authentication\UserEntity;
use App\Security\ReCaptchaUtil;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Service\Attribute\Required;

class AuthController extends Controller
{
    public const MAKE_1ST_USER_ADMIN = true;

    protected AuthenticationListener $auth;
    protected MailerInterface $mailer;

    #[Required]
    public function requiredByAuthController(AuthenticationListener $auth,
                                             MailerInterface $mailer)
    {
        $this->auth = $auth;
        $this->mailer = $mailer;
    }

    #[Route('/@auth/login/', name: 'app_login')]
    public function login(ClientRegistry $clientRegistry,
                          AuthenticationUtils $authenticationUtils,
                          SessionInterface $session, Request $request,
                          TokenStorageInterface $tokenStorage,
                          AuthenticationListener $auth): Response
    {
        $context = [
            'message' => [],
            'last_username' => $authenticationUtils->getLastUsername(),
            'reset_code' => null,
            'providers' => $clientRegistry->getEnabledClientKeys(),
        ];

        if (($error = $authenticationUtils->getLastAuthenticationError())) {
            $context['message'] = ['negative' => true, 'text' => $error->getMessage()];
        } elseif (($reset_code = $request->query->get('reset_code')) &&
            '$' != substr($reset_code, 0, 1) &&
            ($reset_user = $auth->queryUserEntity('password', 'resetCode', $reset_code))) {
            $context['last_username'] = $reset_user->getUsername();
            $context['message'] = ['negative' => false, 'text' => 'Please login now to set your password.'];
            $request->getSession()->invalidate();
        }

        return $this->render('auth/login.html.twig', $context);
    }

    #[Route('/@auth/register', name: 'app_register')]
    public function register(Request $request, ReCaptchaUtil $recaptcha,
                             PasswordAuthenticator $passwordAuth): Response
    {
        $recaptcha_keys = [
            'sitekey' => $_ENV['GOOGLE_RECAPTCHA_SITE_KEY'] ?? null,
            'secretkey' => $_ENV['GOOGLE_RECAPTCHA_SECRET'] ?? null,
        ];
        $post = $request->request;
        $errors = [];
        if ($post->count() > 0) {
            $required = ['username' => null, 'email' => null];
            $parameters = array_merge($required, $post->all());
            if ($recaptcha->isEnabled()) {
                $resp = $recaptcha->verify($post->get('g-recaptcha-response'));
                if (!$resp->isSuccess()) {
                    $errors['recaptcha'] = 'Invalid response: '.join(", ", $resp->getErrorCodes());
                }
            }
            if (empty($errors)) {
                $registerResult = $passwordAuth->registerNewUser($parameters['username'], $parameters['email']);
                if ($registerResult instanceof UserEntity) {
                    $this->auditLogger->log($registerResult, 'REGISTER');
                    $this->addFlash("info", "Renogen has sent you an email with the link to reset your password.\nPlease check you email inbox.");
                    return $this->redirectToRoute('app_home');
                } elseif (is_array($registerResult)) {
                    $errors = $registerResult;
                }
            }
        }
        return $this->render('auth/register.html.twig', ($parameters ?? []) + [
                'recaptcha' => $recaptcha_keys,
                'errors' => $errors,
        ]);
    }

    #[Route('/@auth/forgotpassword', name: 'app_forgotpassword')]
    public function forgotpassword(Request $request, ReCaptchaUtil $recaptcha,
                                   PasswordAuthenticator $passwordAuth): Response
    {
        $recaptcha_keys = [
            'sitekey' => $_ENV['GOOGLE_RECAPTCHA_SITE_KEY'] ?? null,
            'secretkey' => $_ENV['GOOGLE_RECAPTCHA_SECRET'] ?? null,
        ];
        $post = $request->request;
        $errors = [];
        if ($post->count() > 0) {
            $required = ['username' => null];
            $parameters = array_merge($required, $post->all());
            if ($recaptcha->isEnabled()) {
                $resp = $recaptcha->verify($post->get('g-recaptcha-response'));
                if (!$resp->isSuccess()) {
                    $errors['recaptcha'] = 'Invalid response: '.join(", ", $resp->getErrorCodes());
                }
            }
            if (empty($errors)) {
                $resetUser = $passwordAuth->queryUserByUsername($parameters['username']);
                if (!$resetUser) {
                    $this->addFlash("error", "No user with such username.");
                    return $this->redirectToRoute('app_home');
                }
                $passwordAuth->sendResetPasswordEmail($resetUser, 'auth/email.reset_password.html.twig', 'Did you forget your Renogen password?');
                $this->addFlash("info", "A reset password email has been sent to your email address.");
                return $this->redirectToRoute('app_home');
            }
        }
        return $this->render('auth/reset_password.html.twig', ($parameters ?? [])
                + [
                'recaptcha' => $recaptcha_keys,
                'errors' => $errors,
        ]);
    }

    #[Route('/@auth/connect/{client}', name: 'oauth2_connect_start')]
    public function connect(Request $request, OAuth2Authenticator $oauth2,
                            $client): Response
    {
        return $oauth2->getRedirectionToProvider($request, $client);
    }

    #[Route('/@auth/profile/', name: 'app_profile_update')]
    public function profile_update(?ProfileRequest $profileRequest): Response
    {
        $login = $this->ds->currentSecurityUser();
        $user = $login->getUser();
        if ($profileRequest) {
            $needLogout = false;
            if (!$user) {
                $user = new User(substr(sha1(random_bytes(100)), 0, 15));
                $login->setUser($user);
                // make 1st user admin
                if (static::MAKE_1ST_USER_ADMIN && !in_array('ROLE_ADMIN', $user->getRoles())
                    && 0 == $this->ds->count(User::class)) {
                    $user->setRoles(['ROLE_ADMIN', 'ROLE_USER']);
                    $needLogout = true;
                }
            }
            $profileRequest->populate($user);
            if (!($errors = $this->validator->validate($user))) {
                $this->ds->commit($user);
                if ($needLogout) {
                    $this->security->logout(false);
                    return $this->redirectToRoute('app_home');
                }
                $this->addFlash('info', 'Profile saved');
                return $this->redirectToRoute('app_profile_update');
            }
        }
        $this->addHeader('profile', 'Update Profile', $this->generateUrl('app_profile_update'), 'user');
        return $this->render('auth/profile.html.twig', [
                'user' => $user,
                'errors' => $errors ?? [],
        ]);
    }
}