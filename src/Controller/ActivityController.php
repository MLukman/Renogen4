<?php

namespace App\Controller;

use App\Base\Controller;
use App\Entity\Activity;
use App\Entity\ActivityTemplate;
use App\Entity\Item;
use App\Entity\ItemActivity;
use App\Service\TemplateManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Service\Attribute\Required;

class ActivityController extends Controller
{
    protected TemplateManager $templateMgr;

    #[Required]
    public function requireTemplateManager(TemplateManager $templateMgr)
    {
        $this->templateMgr = $templateMgr;
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/@{squadId}/{itemId}/+', name: 'app_activity_add', priority: 20)]
    public function add(Request $request): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('addact',
            'Add Activity',
            $this->nav->entityRoutePath('app_activity_add', $entities['item']),
            'plus');
        if (($templateId = $request->query->get('template'))) {
            $template_or_activity = $this->nav->fetchTemplate($entities['ecosystem'], $entities['environment'], $templateId);
        } elseif (($activityId = $request->query->get('activity'))) {
            $template_or_activity = $this->nav->fetchActivity($entities['deployment'], $activityId);
        }
        return $this->add_or_update($request, $entities['item'],
                $template_or_activity ?? null);
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/@{squadId}/{itemId}/{activityId}', name: 'app_activity_update', priority: 10)]
    public function update(Request $request): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        return $this->add_or_update($request, $entities['item'], $entities['activity']);
    }

    public function add_or_update(Request $request, Item $item,
                                  ActivityTemplate|Activity|null $template_or_activity): Response
    {
        $config = [
            'item' => $item,
            'templateMgr' => $this->templateMgr,
        ];

        if ($template_or_activity instanceof Activity) {
            $config['activity'] = $template_or_activity;
        } elseif ($template_or_activity instanceof ActivityTemplate) {
            $config['template'] = $template_or_activity;
            $config['activity'] = new Activity($config['template'], $item->getDeployment());
        } else {
            $config['templates'] = $this->ds->queryMany(ActivityTemplate::class, [
                'environment' => $item->getDeployment()->getEnvironment(),
            ]);
            $config['activities'] = $this->ds
                ->createQuery('SELECT a FROM App\\Entity\\Activity a WHERE a.deployment = :deployment')
                ->setParameter('deployment', $item->getDeployment())
                ->getResult();
        }

        if (isset($config['activity']) && $request->request->count() > 0) {
            $parameters = $request->request->all('parameters');
            $errors = [];
            if ($parameters != $config['activity']->getParameters()) {
                $config['activity']->setParameters($parameters);
                $templateClass = $this->templateMgr->getTemplateClass($config['activity']->getTemplate()->getClass());
                $errors += $templateClass->validateActivity($config['activity']);
                if (!($errors += $this->validator->validate($config['activity']))) {
                    $this->ds->commit($config['activity']);
                }
            }
            if (empty($errors)) {
                $itemActivity = $this->nav->fetchItemActivity($item, $config['activity']);
                if (!$itemActivity) {
                    $itemActivity = new ItemActivity($item, $config['activity']);
                    $this->ds->commit($itemActivity);
                } elseif (($runItem = $itemActivity->getRunItem()) &&
                    $runItem->getParameters() != $parameters) {
                    if ($runItem->getStatus() == 'New') {
                        $runItem->setParameters($parameters);
                    } else {
                        $itemActivity->setRunItem(null);
                    }
                    $this->ds->commit();
                }
                return $this->redirectToRoute('app_item_view', $this->nav->entityToParameters($item));
            }
            $config['errors'] = $errors;
        }

        return $this->render('activity/form.html.twig', $config);
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/@{squadId}/{itemId}/{activityId}/-', name: 'app_activity_remove', priority: 20)]
    public function remove(Request $request): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        if (($itemActivity = $this->nav->fetchItemActivity($entities['item'], $entities['activity']))) {
            $entities['item']->removeItemActivity($itemActivity);
            $entities['activity']->removeItemActivity($itemActivity);
            $this->ds->delete($itemActivity);
            $this->ds->reloadEntity($entities['activity']);
        }
        if ($entities['activity']->getItemActivities()->count() == 0) {
            $this->ds->delete($entities['activity']);
        }
        return $this->redirectToRoute('app_item_view', $this->nav->entityToParameters($entities['item']));
    }
}