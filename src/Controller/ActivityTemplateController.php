<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\ActivityTemplateRequest;
use App\Entity\Activity;
use App\Entity\ActivityTemplate;
use App\Entity\Component;
use App\Entity\Deployment;
use App\Entity\Environment;
use App\Service\TemplateManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Service\Attribute\Required;

class ActivityTemplateController extends Controller
{
    protected TemplateManager $templateMgr;

    #[Required]
    public function requireTemplateManager(TemplateManager $templateMgr)
    {
        $this->templateMgr = $templateMgr;
    }

    #[Route('/{ecosystemId}/:{componentId}/@{environmentId}/{templateId}/', name: 'app_activitytemplate_view', priority: 10)]
    public function view(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        return $this->render('activity_template/view.html.twig', $entities + [
                'templateClass' => $this->templateMgr->getTemplateClass($entities['template']->getClass()),
                'activity' => new Activity($entities['template'], new Deployment($entities['environment'])), // dummy activity for preview
        ]);
    }

    #[Route('/{ecosystemId}/:{componentId}/@{environmentId}/+', name: 'app_activitytemplate_add', priority: 20)]
    public function add(Request $request, ?ActivityTemplateRequest $atr): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('act-'.$entities['ecosystem']->getId().'-'.$entities['environment']->getId(),
            'Add Activity Template',
            $this->nav->entityRoutePath('app_activitytemplate_add', $entities['environment'], $entities['component']),
            'clipboard');

        $template = null;
        $errors = [];
        if ((($class = $request->query->get('class')) || ($class = $request->request->get('class')))
            && ($templateClass = $this->templateMgr->getTemplateClass($class))) {
            $template = new ActivityTemplate($entities['environment'], $entities['component'], $templateClass);
        } elseif (($import = $request->files->get('import')) && ($json = \json_decode($import->getContent(), true))) {
            $importTemplateRequest = $this->bodyConverter->getUtil()->parse($json, ActivityTemplateRequest::class);
            if ($importTemplateRequest && ($templateClass = $this->templateMgr->getTemplateClass($importTemplateRequest->class ?? ''))) {
                $template = new ActivityTemplate($entities['environment'], $entities['component'], $templateClass);
                $importTemplateRequest->populate($template);
            } else {
                $errors['import'] = 'Previous file upload was not a valid exported activity template JSON file';
            }
        }
        return $this->add_or_update($entities['environment'], $entities['component'], $atr, $template, $errors);
    }

    #[Route('/{ecosystemId}/:{componentId}/@{environmentId}/{templateId}/!', name: 'app_activitytemplate_update', priority: 20)]
    public function update(?ActivityTemplateRequest $atr): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('act-'.$entities['ecosystem']->getId().'-'.$entities['environment']->getId(),
            'Update Activity Template',
            $this->nav->entityRoutePath('app_activitytemplate_update', $entities['template']),
            'pencil');

        return $this->add_or_update($entities['environment'], $entities['component'], $atr, $entities['template']);
    }

    protected function add_or_update(Environment $environment,
                                     Component $component,
                                     ?ActivityTemplateRequest $atr,
                                     ?ActivityTemplate $template = null,
                                     array $errors = []): Response
    {
        $config = [
            'classes' => array_filter($this->templateMgr->allTemplateClasses(), fn($cls) =>
                (call_user_func([$cls, 'allowMultipleTemplates']) || 0 == $this->ds->count(ActivityTemplate::class, [
                    'class' => \get_class($cls),
                    'component' => $component,
                    'environment' => $environment,
                ]))),
            'template' => $template,
        ];

        if ($atr) {
            if ($atr->formAction == 'delete' && $template->canDelete()) {
                $this->addFlash('info', "Activity template '{$template->getName()}' has been deleted");
                $redirect = $this->redirectToRoute('app_component_view', $this->nav->entityToParameters($component));
                $this->ds->delete($template);
                return $redirect;
            }
            $atr->populate($template, $this->ds);
            $templateClass = $this->templateMgr->getTemplateClass($template->getClass());
            $errors += $templateClass->validateTemplate($template);
            if (!($errors += $this->validator->validate($template))) {
                $this->ds->commit($template);
                return $this->redirectToRoute('app_activitytemplate_view', $this->nav->entityToParameters($template));
            }
        }
        $config['errors'] = $errors;

        if ($template) {
            $config['configurationsForm'] = $this->templateMgr->getTemplateClass($template->getClass())->templateFormTwigFilepath();
        }

        return $this->render('activity_template/form.html.twig', $config);
    }

    #[Route('/{ecosystemId}/:{componentId}/@{environmentId}/{templateId}/export', name: 'app_activitytemplate_export', priority: 20)]
    public function export(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        /** @var ActivityTemplate $template */
        $template = $entities['template'];
        $filename = preg_replace('/[^a-z0-9]+/', '-', strtolower($template->getName()));
        return new JsonResponse($template, 200, [
            'Content-Disposition' => "attachment; filename=\"{$filename}.json\"",
        ]);
    }
}