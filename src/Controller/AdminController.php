<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\UserUpdateRequest;
use App\Entity\Login;
use App\Entity\User;
use App\Kernel;
use PhpExtended\Tail\Tail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_ADMIN')]
class AdminController extends Controller
{

    #[Route('/@users', name: 'app_admin_users', priority: 10)]
    public function users(): Response
    {
        $this->addHeader('admin-users',
            'Manage Users',
            $this->nav->generateUrl('app_admin_users'),
            'users');
        return $this->render('admin/users/index.html.twig', [
                'users' => $this->ds->queryMany(User::class),
        ]);
    }

    #[Route('/@users/{userId}', name: 'app_admin_users_update', priority: 10)]
    public function users_update(Request $request,
                                 ?UserUpdateRequest $userUpdateRequest, $userId): Response
    {
        /** @var User $user */
        $user = $this->ds->queryOne(User::class, $userId);
        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        if ($userUpdateRequest) {
            switch ($userUpdateRequest->formAction) {
                case 'delete':
                    $this->ds->delete($user);
                    $this->addFlash("info", "User deleted.");
                    return $this->redirectToRoute('app_admin_users');
                case 'block':
                    $user->setBlocked(true);
                    $this->ds->commit();
                    $this->addFlash("info", "User blocked.");
                    return $this->reload($request);
                case 'unblock':
                    $user->setBlocked(false);
                    $this->ds->commit();
                    $this->addFlash("info", "User unblocked.");
                    return $this->reload($request);
                default:
                    $userUpdateRequest->populate($user, $this->ds);
                    if (!($errors = $this->validator->validate($user))) {
                        $this->ds->commit();
                        $this->addFlash("info", "User updated.");
                        return $this->reload($request);
                    }
                    $this->addFlash("error", "Cannot update user.");
            }
        }

        $this->addHeader('admin-users',
            'Manage Users',
            $this->nav->generateUrl('app_admin_users'),
            'users');
        $this->addHeader('userupd',
            $user->getFullname(),
            $this->nav->generateUrl('app_admin_users_update', ['userId' => $userId]),
            'user');

        return $this->render('admin/users/update.html.twig', [
                'user' => $user,
                'squad_memberships' => $this->ds->createQuery('SELECT s AS squad, m.role FROM App\\Entity\\Squad s JOIN s.ecosystem e LEFT JOIN App\\Entity\\Membership m WITH m.squad = s AND m.member = :user ORDER BY m.rolesort DESC, e.name ASC, s.name ASC')
                    ->setParameter('user', $user)
                    ->getResult(),
                'has_contrib' => $this->checkUserHasContributions($user),
                'errors' => $errors ?? [],
        ]);
    }

    #[Route('/@logins', name: 'app_admin_logins', priority: 10)]
    public function logins(): Response
    {
        $this->addHeader('admin-logins',
            'Manage Logins',
            $this->nav->generateUrl('app_admin_logins'),
            'key');
        return $this->render('admin/logins/index.html.twig', [
                'logins' => $this->ds->queryMany(Login::class),
        ]);
    }

    #[Route('/@phpinfo', name: 'app_admin_phpinfo', priority: 10)]
    public function phpinfo()
    {
        $this->addHeader('admin-viewlogs',
            'PHP Info',
            $this->nav->generateUrl('app_admin_phpinfo'),
            'php');
        return $this->render("base.html.twig", [
                'content' => '<iframe id="topmargin" style="position:fixed; top:0px; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden;" src="'.$this->generateUrl('app_admin_phpinfo_content').'" />'
        ]);
    }

    #[Route('/@phpinfo@', name: 'app_admin_phpinfo_content', priority: 10)]
    public function phpinfo_content()
    {
        ob_start();
        phpinfo();
        $html = ob_get_contents();
        ob_end_clean();
        return new Response($html);
    }

    #[Route('/@logs', name: 'app_admin_viewlogs', priority: 10)]
    public function viewlog(Kernel $kernel)
    {
        $this->addHeader('admin-viewlogs',
            'View Logs',
            $this->nav->generateUrl('app_admin_viewlogs'),
            'bug');
        $logfile = $_ENV['APP_ENV'].'.log';
        $logfilepath = $kernel->getLogDir()."/".$logfile;
        if (file_exists($logfilepath)) {
            $tail = new Tail($logfilepath);
            return $this->render("base.html.twig", [
                    'content' => '<h3>Showing Last 100 Lines of <em>'.$logfile.'</em></h3><pre style="overflow:auto">'.htmlentities(implode("\n", $tail->smart(100))).'</pre>'
            ]);
        } else {
            return $this->render("base.html.twig", [
                    'content' => '<h3>Log file <em>'.$logfile.'</em> does not exist, meaning there is nothing logged yet. And that is good :)</h3>'
            ]);
        }
    }

    protected function checkUserHasContributions(User $user)
    {
        $has_contrib = false;
        if ($user->getCreated()) {
            $entities = [
                'Activity',
                'ActivityFile',
                'Checklist',
                'ChecklistUpdate',
                'Component',
                'Deployment',
                'DeploymentRequest',
                'Ecosystem',
                'Environment',
                'Item',
                'ItemActivity',
                'ItemComment',
                'ItemStatusLog',
                'Squad',
            ];
            foreach ($entities as $entity) {
                $has_contrib = $has_contrib || 0 < count($this->ds->queryUsingOr("\App\Entity\\$entity",
                            ['createdBy' => $user, 'updatedBy' => $user]));
            }
        }
        return $has_contrib;
    }
}