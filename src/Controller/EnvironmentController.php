<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\EnvironmentRequest;
use App\Entity\ActivityTemplate;
use App\Entity\Deployment;
use App\Entity\Ecosystem;
use App\Entity\Environment;
use DateTime;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class EnvironmentController extends Controller
{

    #[Route('/{ecosystemId}/@+', name: 'app_environment_add', priority: 20)]
    public function add(?EnvironmentRequest $environmentRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('envnew',
            'Add Environment',
            $this->nav->entityRoutePath('app_environment_add', $entities['ecosystem']),
            'globe');
        return $this->add_or_update($environmentRequest, $entities['ecosystem']);
    }

    #[Route('/{ecosystemId}/@{environmentId}', name: 'app_environment_view', priority: 10)]
    public function view(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $templates = $this->ds
                ->queryBuilder(ActivityTemplate::class, 't')
                ->where('t.environment = :environment')
                ->setParameter('environment', $entities['environment'])
                ->getQuery()->getResult();
        $componentTemplates = [];
        foreach ($templates as $template) {
            /** @var ActivityTemplate $template */
            list($layer, $component) = [$template->getComponent()->getLayer(),
                $template->getComponent()];
            $componentTemplates["$layer"]["$component"][] = $template;
        }
        return $this->render('environment/view.html.twig', $entities + [
                'componentTemplates' => $componentTemplates,
        ]);
    }

    #[Route('/{ecosystemId}/@{environmentId}/!', name: 'app_environment_update', priority: 20)]
    public function update(?EnvironmentRequest $environmentRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('envupd',
            'Update Environment',
            $this->nav->entityRoutePath('app_environment_update', $entities['environment']),
            'pencil');
        return $this->add_or_update($environmentRequest, $entities['environment']);
    }

    protected function add_or_update(?EnvironmentRequest $environmentRequest,
                                     Ecosystem|Environment $environment_or_ecosystem): Response
    {
        $environment = ($environment_or_ecosystem instanceof Environment ? $environment_or_ecosystem
                : new Environment($environment_or_ecosystem, $environmentRequest
                    ? $environmentRequest->identifier ?? null : null));
        if ($environmentRequest) {
            if ($environmentRequest->formAction == 'delete' && $environment->canDelete()) {
                $this->addFlash('info', "Environment '{$environment->getName()}' has been deleted");
                $redirect = $this->redirectToRoute('app_ecosystem_view', $this->nav->entityToParameters($environment->getEcosystem()));
                $this->ds->delete($environment);
                return $redirect;
            }
            $environmentRequest->populate($environment);
            if (!($errors = $this->validator->validate($environment))) {
                $this->ds->commit($environment);
                return $this->redirectToRoute('app_ecosystem_view', $this->nav->entityToParameters($environment->getEcosystem()));
            }
        }
        return $this->render('environment/form.html.twig', [
                'environment' => $environment,
                'errors' => $errors ?? [],
        ]);
    }

    #[Route('/{ecosystemId}/@{environmentId}/_/{activityFileId}', name: 'app_file_download', priority: 20)]
    public function download($activityFileId): Response
    {
        $this->resolveEntitiesAndCheckPermission();
        $activityFile = $this->ds->getActivityFile($activityFileId);
        if (!$activityFile) {
            throw new NotFoundHttpException();
        }
        return $activityFile->getFile()->getDownloadResponse();
    }

    #[Route('/{ecosystemId}/@{environmentId}/past', name: 'app_environment_past', priority: 20)]
    public function pastDeployments(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('past',
            'Past Deployments',
            $this->nav->entityRoutePath('app_environment_past', $entities['environment']),
            'history');
        $qb = $this->ds->queryBuilder(Deployment::class, 'd')
            ->join('d.environment', 'e')
            ->andWhere('e = :environment')
            ->andWhere('d.expectedCompletionDate <= :now')
            ->orderBy('d.executeDate', 'DESC')
            ->setParameter('now', new DateTime())
            ->setParameter('environment', $entities['environment']);
        return $this->render('environment/past.html.twig', $entities + [
                'deployments' => $qb->getQuery()->getResult(),
        ]);
    }
}