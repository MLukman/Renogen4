<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\ComponentRequest;
use App\Entity\Component;
use App\Entity\Ecosystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ComponentController extends Controller
{

    #[Route('/{ecosystemId}/:+', name: 'app_component_add', priority: 20)]
    public function add(?ComponentRequest $componentRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader(
            'add-component',
            'Add Component',
            $this->nav->entityRoutePath('app_component_add', $entities['ecosystem']),
            'cubes');
        return $this->add_or_update($componentRequest, $entities['ecosystem']);
    }

    #[Route('/{ecosystemId}/:{componentId}/', name: 'app_component_view', priority: 10)]
    public function view(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        return $this->render('component/view.html.twig', $entities);
    }

    #[Route('/{ecosystemId}/:{componentId}/!', name: 'app_component_update', priority: 10)]
    public function update(?ComponentRequest $componentRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('compupd',
            'Update Component',
            $this->nav->entityRoutePath('app_component_update', $entities['component']),
            'pencil');
        return $this->add_or_update($componentRequest, $entities['component']);
    }

    protected function add_or_update(?ComponentRequest $componentRequest,
                                     Ecosystem|Component $component_or_ecosystem): Response
    {
        $component = ($component_or_ecosystem instanceof Component ? $component_or_ecosystem
                : new Component($component_or_ecosystem, $componentRequest ? $componentRequest->identifier
                    ?? null : null));
        if ($componentRequest) {
            if ($componentRequest->formAction == 'delete' && $component->canDelete()) {
                $this->addFlash('info', "Component '{$component->getName()}' has been deleted");
                $redirect = $this->redirectToRoute('app_ecosystem_view', $this->nav->entityToParameters($component->getEcosystem()));
                $this->ds->delete($component);
                return $redirect;
            }
            $componentRequest->populate($component);
            if (!($errors = $this->validator->validate($component))) {
                $this->ds->commit($component);
                return $this->redirectToRoute('app_component_view', $this->nav->entityToParameters($component));
            }
        }
        return $this->render('component/form.html.twig', [
                'component' => $component,
                'errors' => $errors ?? [],
        ]);
    }
}