<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\DeploymentRequest;
use App\DTO\RunBookUpdateRequest;
use App\Entity\Deployment;
use App\Entity\Environment;
use App\Entity\Item;
use App\Entity\RunItem;
use App\Service\TemplateManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Service\Attribute\Required;

class DeploymentController extends Controller
{
    protected TemplateManager $templateMgr;

    #[Required]
    public function requireTemplateManager(TemplateManager $templateMgr)
    {
        $this->templateMgr = $templateMgr;
    }

    #[Route('/{ecosystemId}/@{environmentId}/+', name: 'app_deployment_add', priority: 20)]
    public function add(?DeploymentRequest $deploymentRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('depnew',
            'Add Deployment',
            $this->nav->entityRoutePath('app_deployment_add', $entities['environment']),
            'calendar plus o');
        return $this->add_or_update($deploymentRequest, $entities['environment']);
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}', name: 'app_deployment_view', priority: 10)]
    public function view(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        return $this->render('deployment/view.html.twig', $entities);
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/!', name: 'app_deployment_update', priority: 10)]
    public function update(?DeploymentRequest $deploymentRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('depupd',
            'Update Deployment',
            $this->nav->entityRoutePath('app_deployment_update', $entities['deployment']),
            'pencil');
        return $this->add_or_update($deploymentRequest, $entities['deployment']);
    }

    protected function add_or_update(?DeploymentRequest $deploymentRequest,
                                     Environment|Deployment $deployment_or_environment): Response
    {
        $deployment = ($deployment_or_environment instanceof Deployment ? $deployment_or_environment
                : new Deployment($deployment_or_environment));
        if ($deploymentRequest) {
            if ($deploymentRequest->formAction == 'delete' && $deployment->canDelete()) {
                $this->addFlash('info', "Deployment '{$deployment->displayTitle()}' has been deleted");
                $redirect = $this->redirectToRoute('app_environment_view', $this->nav->entityToParameters($deployment->getEnvironment()));
                $this->ds->delete($deployment);
                return $redirect;
            }
            $deploymentRequest->populate($deployment);
            if (!($errors = $this->validator->validate($deployment))) {
                $this->ds->commit($deployment);
                return $this->redirectToRoute('app_deployment_view', $this->nav->entityToParameters($deployment));
            }
        }

        return $this->render('deployment/form.html.twig', [
                'deployment' => $deployment,
                'errors' => $errors ?? [],
        ]);
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/@', name: 'app_deployment_releasenote', priority: 20)]
    public function releasenote(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('releasenote',
            'Release Note',
            $this->nav->entityRoutePath('app_deployment_releasenote', $entities['deployment']),
            'checkmark box');
        return $this->render('deployment/releasenote.html.twig', $entities);
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/@@', name: 'app_deployment_runbook', priority: 20)]
    public function runbook(Request $request,
                            ?RunBookUpdateRequest $runbookUpdateReq): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        if ($runbookUpdateReq) {
            if (($runitem = $this->ds->queryOne(RunItem::class,
                ['id' => $runbookUpdateReq->runItemId, 'deployment' => $entities['deployment']]))) {
                /** @var RunItem $runitem */
                $runitem->setStatus($runbookUpdateReq->newStatus);

                switch ($runitem->getStatus()) {
                    case 'Failed':
                        foreach ($runitem->getItemActivities() as $itemActivity) {
                            $item = $itemActivity->getItem();
                            if ($item->getStatus() != Item::STATUS_READY) {
                                continue;
                            }
                            /** @var Item $item */
                            $this->ds->changeItemStatus($item, Item::STATUS_FAILED, $runbookUpdateReq->remark
                                        ?? null);
                        }
                        break;

                    case 'Completed':
                        foreach ($runitem->getItemActivities() as $itemActivity) {
                            $item = $itemActivity->getItem();
                            if ($item->getStatus() != Item::STATUS_READY) {
                                continue;
                            }
                            foreach ($item->getItemActivities() as $itemItemActivity) {
                                if (!($itemRunItem = $itemItemActivity->getRunItem())
                                    ||
                                    $itemRunItem->getStatus() != 'Completed') {
                                    continue 2;
                                }
                            }
                            $this->ds->changeItemStatus($item, Item::STATUS_COMPLETED, $runbookUpdateReq->remark
                                        ?? null);
                        }
                        break;
                }
                $this->ds->commit();
            }
            return $this->reload($request);
        }

        $this->addHeader('runbook',
            'Run Book',
            $this->nav->entityRoutePath('app_deployment_runbook', $entities['deployment']),
            'play circle');
        return $this->render('deployment/runbook.html.twig', $entities + [
                'templateMgr' => $this->templateMgr,
                'activities' => $this->ds
                    ->createQuery('SELECT a FROM App\\Entity\\Activity a JOIN a.template t WHERE a.deployment = :deployment AND EXISTS (SELECT r FROM App\\Entity\\RunItem r WHERE r.activity = a) ORDER BY t.priority DESC, a.created ASC')
                    ->setParameter('deployment', $entities['deployment'])
                    ->getResult(),
        ]);
    }
}