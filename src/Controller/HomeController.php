<?php

namespace App\Controller;

use App\Base\Controller;
use App\Entity\Ecosystem;
use App\Entity\Item;
use App\Entity\Membership;
use App\Entity\Squad;
use DateTime;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{

    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        $ecosystems = [];

        // first, fill up ecosystems that the user is an ecosystemAdmin of
        foreach ($this->ds->getUser()->getEcosystemAdmins() as $ecosystemAdmin) {
            $ecoId = $ecosystemAdmin->getEcosystem()->getId();
            if (!isset($ecosystems[$ecoId])) {
                $ecosystems[$ecoId] = [
                    'entity' => $ecosystemAdmin->getEcosystem(),
                    'squads' => [],
                ];
            }
        }

        // next, fill up ecosystems+squads for which current user has memberships
        foreach ($this->ds->queryMany(Membership::class, ['member' => $this->ds->getUser()],
            ['favourite' => 'DESC', 'role' => 'ASC', 'created' => 'ASC']) as $membership) {
            $squad = $membership->getSquad();
            if ($squad->getDecommissioned()) {
                continue;
            }
            $ecoId = $squad->getEcosystem()->getId();
            if (!isset($ecosystems[$ecoId])) {
                $ecosystems[$ecoId] = [
                    'entity' => $squad->getEcosystem(),
                    'squads' => [],
                ];
            }
            $ecosystems[$ecoId]['squads'][$squad->getId()] = [
                'entity' => $squad,
                'membership' => $membership,
                'role' => $membership->getRole(),
            ];
        }
        // next, fill up squads the current user is not a member of
        foreach ($this->ds->queryMany(Squad::class, ['decommissioned' => null]) as $squad) {
            if ($squad->getEcosystem()->isPrivate() && !$this->isGranted('ROLE_ADMIN')) {
                continue;
            }
            $ecoId = $squad->getEcosystem()->getId();
            if (!isset($ecosystems[$ecoId])) {
                $ecosystems[$ecoId] = [
                    'entity' => $squad->getEcosystem(),
                    'squads' => [],
                ];
            }
            if (!isset($ecosystems[$ecoId]['squads'][$squad->getId()])) {
                $ecosystems[$ecoId]['squads'][$squad->getId()] = [
                    'entity' => $squad,
                    'membership' => null,
                    'role' => null,
                ];
            }
        }
        // finally, fill up ecosystems that do not have squads yet
        foreach ($this->ds->queryMany(Ecosystem::class) as $ecosystem) {
            if ($ecosystem->isPrivate() && !$this->isGranted('ROLE_ADMIN')) {
                continue;
            }
            $ecoId = $ecosystem->getId();
            if (!isset($ecosystems[$ecoId])) {
                $ecosystems[$ecoId] = [
                    'entity' => $ecosystem,
                    'squads' => [],
                ];
            }
        }

        // special case for no ecosystem and user is admin
        if (empty($ecosystems) && $this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('app_ecosystem_add');
        }

        return $this->render('home/index.html.twig', [
                'ecosystems' => $ecosystems,
                'actions_needed' => $this->queryActionsNeeded(),
        ]);
    }

    protected function queryActionsNeeded(): array
    {
        $actions_needed = ['count' => 0];
        $actions_needed['items'] = $this->ds
            ->createQuery('SELECT i FROM App\\Entity\\Item i JOIN App\\Entity\\Membership m JOIN i.deployment d '
                ."WHERE m.squad = i.squad AND m.member = :user "
                ."AND d.expectedCompletionDate > :now "
                ."AND ("
                ."(m.role IN ('approval','entry') AND i.status IN (:status_init)) "
                ."OR (m.role IN ('entry') AND i.status IN (:status_rejected)) "
                ."OR (m.role IN ('approval','review') AND i.status IN (:status_review)) "
                ."OR (m.role IN ('approval') AND i.status IN (:status_approval)) "
                ."OR (m.role IN ('execute') AND i.status IN (:status_ready)) "
                .")")
            ->setParameter('now', new DateTime())
            ->setParameter('user', $this->ds->getUser())
            ->setParameter('status_init', [Item::STATUS_INIT, Item::STATUS_FAILED])
            ->setParameter('status_rejected', [Item::STATUS_REJECTED])
            ->setParameter('status_review', [Item::STATUS_REVIEW])
            ->setParameter('status_approval', [Item::STATUS_APPROVAL])
            ->setParameter('status_ready', [Item::STATUS_READY])
            ->getResult();
        $actions_needed['requests'] = $this->ds
            ->createQuery('SELECT dr FROM App\\Entity\\DeploymentRequest dr JOIN dr.environment e '
                ."WHERE dr.executeDate > :now AND dr.deployment IS NULL "
                ."AND ("
                ."EXISTS(SELECT m FROM App\\Entity\\Membership m JOIN m.squad s WHERE s.ecosystem = e.ecosystem AND m.member = :user)"
                ."OR EXISTS(SELECT a FROM App\\Entity\\EcosystemAdmin a WHERE a.ecosystem = e.ecosystem AND a.user = :user)"
                .") ORDER BY dr.executeDate ASC ")
            ->setParameter('now', new DateTime())
            ->setParameter('user', $this->ds->getUser())
            ->getResult();
        $actions_needed['tasks'] = $this->ds
            ->createQuery("SELECT c FROM App\\Entity\\Checklist c JOIN c.pics p WITH p = :user WHERE c.status NOT IN ('Completed','Cancelled') "
                ."ORDER BY c.start ASC ")
            ->setParameter('user', $this->ds->getUser())
            ->getResult();

        $actions_needed['count'] = count($actions_needed['items']) + count($actions_needed['requests'])
            + count($actions_needed['tasks']);
        return $actions_needed;
    }
}