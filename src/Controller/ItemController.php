<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\ItemChangeStatusRequest;
use App\DTO\ItemCommentActionRequest;
use App\DTO\ItemCommentRequest;
use App\DTO\ItemRequest;
use App\Entity\Activity;
use App\Entity\Deployment;
use App\Entity\Item;
use App\Entity\ItemActivity;
use App\Entity\ItemComment;
use App\Service\TemplateManager;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Service\Attribute\Required;

class ItemController extends Controller
{
    protected TemplateManager $templateMgr;

    #[Required]
    public function requireTemplateManager(TemplateManager $templateMgr)
    {
        $this->templateMgr = $templateMgr;
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/@{squadId}/+', name: 'app_item_add', priority: 20)]
    public function add(?ItemRequest $itemRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('itemnew',
            'Add Deployment Item',
            $this->nav->entityRoutePath('app_item_add', $entities['deployment'], $entities['squad']),
            'flag');
        return $this->add_or_update($itemRequest, new Item($entities['deployment'], $entities['squad']));
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/@{squadId}/{itemId}', name: 'app_item_view', priority: 10)]
    public function view(Request $request,
                         ?ItemChangeStatusRequest $changeStatusReq,
                         ?ItemCommentRequest $itemCommentReq,
                         ?ItemCommentActionRequest $itemCommentActionReq): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $item = $entities['item'];

        if ($changeStatusReq) {
            $new_status = $changeStatusReq->newStatus;
            $remark = trim($changeStatusReq->statusRemark);
            $transitions = [];
            foreach ($this->ds->getItemAllowedTransitions($item) as $transits) {
                $transitions = $transitions + $transits;
            }
            if (isset($transitions[$new_status]) &&
                (!empty($remark) || !$transitions[$new_status]['remark'])) {
                $this->ds->changeItemStatus($item, $new_status, $remark);
                $this->ds->commit();
            }
            return $this->reload($request);
        }

        if ($itemCommentReq) {
            $itemComment = new ItemComment($item);
            $itemComment->setText($itemCommentReq->itemComment);
            $this->ds->commit($itemComment);
            return $this->reload($request);
        }

        if ($itemCommentActionReq &&
            ($itemComment = $this->ds->queryOne(ItemComment::class, $itemCommentActionReq->commentId))
            && $itemComment->getCreatedBy() === $this->ds->getUser()) {
            switch ($itemCommentActionReq->commentAction) {
                case 'delete':
                    $itemComment->setDeletedDate(new DateTime());
                    break;
                case 'undelete':
                    $itemComment->setDeletedDate(null);
                    break;
            }
            $this->ds->commit($itemComment);
            return $this->reload($request);
        }

        return $this->render('item/view.html.twig', $entities + [
                'itemActivities' => $this->ds
                    ->createQuery('SELECT ia FROM App\\Entity\\ItemActivity ia JOIN ia.activity a JOIN a.template t WHERE ia.item = :item ORDER BY t.priority DESC, a.created ASC')
                    ->setParameter('item', $entities['item'])
                    ->getResult(),
                'templateMgr' => $this->templateMgr,
                'editable' => $item->compareCurrentStatusTo(Item::STATUS_READY) > 0
                && empty($entities['squad']->getDecommissioned()),
        ]);
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/@{squadId}/{itemId}/!', name: 'app_item_update', priority: 20)]
    public function update(?ItemRequest $itemRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('itemupd',
            'Update Deployment Item',
            $this->nav->entityRoutePath('app_item_update', $entities['item']),
            'pencil');
        return $this->add_or_update($itemRequest, $entities['item']);
    }

    protected function add_or_update(?ItemRequest $itemRequest, Item $item): Response
    {
        if ($itemRequest) {
            if ($itemRequest->formAction == 'delete' && $item->canDelete()) {
                $this->addFlash('info', "Deployment item '{$item->getName()}' has been deleted");
                $redirect = $this->redirectToRoute(
                    'app_squad_deployment',
                    $this->nav->entityToParameters($item->getSquad()) + $this->nav->entityToParameters($item->getDeployment()));
                $this->ds->delete($item);
                return $redirect;
            }
            if ($itemRequest->formAction == 'move' &&
                ($moveto = $this->ds->queryOne(Deployment::class, [
                'id' => $itemRequest->moveto,
                'environment' => $item->getDeployment()->getEnvironment(),
                ]))) {
                $item->setDeployment($moveto);
                // now handle activities
                $toRemove = [];
                foreach ($item->getItemActivities() as $itemActivity) {
                    /** @var ItemActivity $itemActivity */
                    // first find activity in new deployment that uses the same template
                    $currentActivity = $itemActivity->getActivity();
                    $similarActivities = $this->ds->queryMany(Activity::class, [
                        'deployment' => $moveto,
                        'template' => $currentActivity->getTemplate(),
                    ]);
                    if (count($similarActivities) == 1) {
                        // found exactly one activity in destination deployment using the same template so just share it
                        $item->addItemActivity(new ItemActivity($item, $similarActivities[0]));
                        $toRemove[] = $itemActivity;
                    } elseif ($currentActivity->getItemActivities()->count() == 1) {
                        // the current activity is not shared with other items so just move it to the new deployment
                        $currentActivity->setDeployment($moveto);
                    } else {
                        // need to clone the activity and then detach from it
                        $clone = new Activity($currentActivity->getTemplate(), $moveto);
                        $clone->setParameters($currentActivity->getParameters());
                        $this->ds->manage($clone);
                        foreach ($currentActivity->getFiles() as $activityFile) {
                            $newActivityFile = $activityFile->clone();
                            $newActivityFile->setActivity($clone);
                            $this->ds->manage($newActivityFile);
                            $clone->setParameters(
                                \json_decode(
                                    str_replace(
                                        $activityFile->getId(),
                                        $newActivityFile->getId(),
                                        \json_encode($clone->getParameters())
                                    )
                                    , true
                                )
                            );
                        }
                        $item->addItemActivity(new ItemActivity($item, $clone));
                        $this->ds->commit();
                        $toRemove[] = $itemActivity;
                    }
                }
                foreach ($toRemove as $ia) {
                    $ia->getItem()->removeItemActivity($ia);
                    $activity = $ia->getActivity();
                    $activity->removeItemActivity($ia);
                    $this->ds->delete($ia);
                    $this->ds->reloadEntity($activity);
                    if ($activity->getItemActivities()->count() == 0) {
                        $this->ds->delete($activity);
                    }
                }
            }
            $itemRequest->populate($item);
            if (!($errors = $this->validator->validate($item))) {
                $this->ds->commit($item);
                return $this->redirectToRoute('app_item_view', $this->nav->entityToParameters($item));
            }
        }

        return $this->render('item/form.html.twig', [
                'item' => $item,
                'errors' => $errors ?? [],
        ]);
    }
}