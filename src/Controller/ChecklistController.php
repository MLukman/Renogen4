<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\ChecklistRequest;
use App\Entity\Checklist;
use App\Entity\ChecklistUpdate;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChecklistController extends Controller
{

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/checklist/+', name: 'app_checklist_add', priority: 30)]
    public function add(?ChecklistRequest $checklistRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('checklistnew',
            'Add Checklist Task',
            $this->nav->entityRoutePath('app_checklist_add', $entities['deployment']),
            'tasks');
        return $this->add_or_update($checklistRequest, new Checklist($entities['deployment']));
    }

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/checklist/{checklistId}', name: 'app_checklist_update', priority: 20)]
    public function update(?ChecklistRequest $checklistRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        return $this->add_or_update($checklistRequest, $entities['checklist']);
    }

    protected function add_or_update(?ChecklistRequest $checklistRequest,
                                     Checklist $checklist): Response
    {
        if ($checklistRequest) {
            $redirect = $this->nav->entityRoutePath('app_deployment_view', $checklist->getDeployment()).'#/checklist';
            if ($checklistRequest->formAction == 'delete' && $this->isGranted('delete', $checklist)) {
                $this->addFlash('info', "Checklist task '{$checklist->getTitle()}' has been deleted");
                $this->ds->delete($checklist);
                return $this->redirect($redirect);
            }
            $checklistRequest->populate($checklist, $this->ds);
            $errors = $this->validator->validate($checklist);
            if ($checklist->getCreated()) {
                if (empty($comment = trim($checklistRequest->update))) {
                    $this->validator->addValidationError($errors, 'update', 'Required');
                } elseif (empty($errors)) {
                    $update = new ChecklistUpdate($checklist);
                    $update->setStatus($checklist->getStatus());
                    $update->setComment($comment);
                    $checklist->addUpdate($update);
                }
            }
            if (empty($errors)) {
                $this->ds->commit($checklist);
                return $this->redirect($redirect);
            }
        }

        return $this->render('checklist/form.html.twig', [
                'checklist' => $checklist,
                'errors' => $errors ?? [],
        ]);
    }
}