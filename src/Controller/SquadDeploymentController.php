<?php

namespace App\Controller;

use App\Base\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SquadDeploymentController extends Controller
{

    #[Route('/{ecosystemId}/@{environmentId}/{deploymentId}/@{squadId}', name: 'app_squad_deployment')]
    public function index(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        return $this->render('squad_deployment/index.html.twig', $entities + [
                'squad_items' => $entities['deployment']->getItemsForSquad($entities['squad']),
        ]);
    }
}