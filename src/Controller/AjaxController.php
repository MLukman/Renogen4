<?php

namespace App\Controller;

use App\Base\Controller;
use App\Service\Markdown;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends Controller
{

    #[Route('/$/markdown', name: 'app_ajax_markdown', priority: 10)]
    public function markdown(Request $request)
    {
        $parser = new Markdown();
        return new Response($parser->parse($request->request->get("code")));
    }
}