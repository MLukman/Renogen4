<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ErrorController extends AbstractController
{

    protected string $title;

    public function show(Request $request, $exception)
    {
        $this->title = 'Error Found';
        switch ($exception->getStatusCode() ?: $exception->getCode()) {
            case 403;
                $this->title = 'Access Denied';
                break;
            case 404;
                $this->title = 'Not Found';
                break;
        }
        return $this->errorPage($this->title, $exception->getMessage());
    }

    public function errorPage($title, $message)
    {
        return $this->render('error/index.html.twig', [
                'error' => [
                    'title' => $title,
                    'message' => $message,
                ]
        ]);
    }
}