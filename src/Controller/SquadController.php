<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\SquadFavouriteRequest;
use App\DTO\SquadMembersRequest;
use App\DTO\SquadRequest;
use App\Entity\Ecosystem;
use App\Entity\Membership;
use App\Entity\Squad;
use App\Entity\User;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SquadController extends Controller
{

    #[Route('/{ecosystemId}/+', name: 'app_squad_add', priority: 20)]
    public function add(?SquadRequest $squadRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('projnew',
            'Add Squad',
            $this->nav->entityRoutePath('app_squad_add', $entities['ecosystem']),
            'users');
        return $this->add_or_update($squadRequest, $entities['ecosystem']);
    }

    #[Route('/{ecosystemId}/{squadId}', name: 'app_squad_view')]
    public function view(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $upcomingDeployments = $this->ds->createQuery('SELECT d FROM App\\Entity\\Deployment d JOIN d.environment env JOIN env.ecosystem eco JOIN eco.squads s WITH s = :squad WHERE d.expectedCompletionDate > :now ORDER BY d.executeDate ASC')
            ->setParameter('now', new DateTime())
            ->setParameter('squad', $entities['squad'])
            ->getResult();
        $pastDeployments = $this->ds->createQuery('SELECT d FROM App\\Entity\\Deployment d JOIN d.environment env JOIN env.ecosystem eco JOIN eco.squads s WITH s = :squad WHERE d.expectedCompletionDate < :now AND EXISTS (SELECT i FROM App\\Entity\\Item i WHERE i.deployment = d AND i.squad = s) ORDER BY d.executeDate DESC')
            ->setParameter('now', new DateTime())
            ->setParameter('squad', $entities['squad'])
            ->setMaxResults(10)
            ->getResult();
        return $this->render('squad/view.html.twig', $entities + [
                'upcomingDeployments' => $upcomingDeployments,
                'pastDeployments' => $pastDeployments,
                'membership' => $this->ds->getSquadMembership($entities['squad'], $this->ds->getUser()),
        ]);
    }

    #[Route('/{ecosystemId}/{squadId}/!', name: 'app_squad_update')]
    public function update(?SquadRequest $squadRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        if ($squadRequest && $squadRequest->formAction == 'delete' && $entities['squad']->canDelete()) {
            $this->addFlash('info', "Squad '{$entities['squad']->getName()}' has been deleted");
            $redirect = $this->redirectToRoute('app_ecosystem_view', $this->nav->entityToParameters($entities['ecosystem']));
            $this->ds->delete($entities['squad']);
            return $redirect;
        }
        $this->addHeader('squadupd',
            'Update Squad',
            $this->nav->entityRoutePath('app_squad_update', $entities['squad']),
            'pencil');
        return $this->add_or_update($squadRequest, $entities['squad']);
    }

    protected function add_or_update(?SquadRequest $squadRequest,
                                     Ecosystem|Squad $squad_or_ecosystem): Response
    {
        $squad = ($squad_or_ecosystem instanceof Squad ? $squad_or_ecosystem : new Squad($squad_or_ecosystem, $squadRequest
                    ? $squadRequest->identifier ?? null : null));
        if ($squadRequest) {
            $squadRequest->populate($squad);
            if (!($errors = $this->validator->validate($squad))) {
                $this->ds->commit($squad);
                return $this->redirectToRoute('app_squad_view', $this->nav->entityToParameters($squad));
            }
        }
        return $this->render('squad/form.html.twig', [
                'squad' => $squad,
                'errors' => $errors ?? [],
        ]);
    }

    #[Route('/{ecosystemId}/{squadId}/users', name: 'app_squad_users')]
    public function users(Request $request,
                          ?SquadMembersRequest $squadMembersRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        if ($squadMembersRequest) {
            switch ($squadMembersRequest->formAction ?? null) {
                case 'save':
                    foreach ($squadMembersRequest->memberships as $userId => $role) {
                        if (!($membership = $entities['squad']->getMemberships()->get($userId))) {
                            continue;
                        }
                        if (in_array($role, Membership::ROLESEQUENCE)) {
                            $membership->setRole($role);
                        } else {
                            $entities['squad']->removeMembership($membership);
                        }
                    }
                    $this->ds->commit();
                    break;
                case 'add':
                    if (!in_array($squadMembersRequest->newMemberRole, Membership::ROLESEQUENCE)) {
                        break;
                    }
                    foreach ($squadMembersRequest->newMemberUsers as $userId) {
                        if (!($user = $this->ds->queryOne(User::class, $userId))) {
                            continue;
                        }
                        $membership = new Membership($user, $entities['squad'], $squadMembersRequest->newMemberRole);
                        $entities['squad']->addMembership($membership);
                    }
                    $this->ds->commit();
                    break;
            }
            return $this->reload($request);
        }

        $this->addHeader('squadusers',
            'Manage Members',
            $this->nav->entityRoutePath('app_squad_users', $entities['squad']),
            'settings');

        return $this->render('squad/users.html.twig', [
                'non_members' => $this->ds
                    ->createQuery('SELECT u FROM App\\Entity\\User u WHERE NOT EXISTS (SELECT m FROM App\\Entity\\Membership m WHERE m.member = u AND m.squad = :squad)')
                    ->setParameter('squad', $entities['squad'])
                    ->getResult(),
        ]);
    }

    #[Route('/{ecosystemId}/{squadId}/favourite', name: 'app_squad_favourite_ajax', priority: 10)]
    public function favourite(?SquadFavouriteRequest $squadFavouriteRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        if ($squadFavouriteRequest) {
            /** @var Membership $membership */
            $membership = $this->ds->queryOne(Membership::class,
                $squadFavouriteRequest->membershipId ?: [
                'squad' => $entities['squad'],
                'member' => $this->ds->getUser(),
            ]);
            if ($membership) {
                $membership->setFavourite($squadFavouriteRequest->favourite > 0);
                $this->ds->commit();
            }
        }
        return new JsonResponse($entities['squad']->getStarCount());
    }
}