<?php

namespace App\Controller;

use App\Base\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GuidesController extends Controller
{

    #[Route('/@guides', name: 'app_guides', priority: 20)]
    public function index(): Response
    {
        $this->addHeader('guides', 'Guides', $this->generateUrl('app_guides'), 'question circle outline');
        return $this->render('guides/index.html.twig');
    }
}