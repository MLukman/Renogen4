<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\EcosystemRequest;
use App\Entity\ActivityTemplate;
use App\Entity\Ecosystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EcosystemController extends Controller
{

    #[Route('/{ecosystemId}/', name: 'app_ecosystem_view')]
    public function view(): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $squads = $this->ds
            ->createQuery('SELECT s as squad, m.role, CASE WHEN m.rolesort IS NULL THEN 0 ELSE m.rolesort END rolesort, m.favourite FROM App\\Entity\\Squad s LEFT JOIN s.memberships m WITH m.member = :user WHERE s.ecosystem = :ecosystem AND s.decommissioned IS NULL ORDER BY m.favourite DESC, rolesort DESC, m.created')
            ->setParameter('user', $this->ds->getUser())
            ->setParameter('ecosystem', $entities['ecosystem'])
            ->getResult();
        $decommissioned = $this->ds
            ->createQuery('SELECT s FROM App\\Entity\\Squad s WHERE s.ecosystem = :ecosystem AND s.decommissioned IS NOT NULL ORDER BY s.decommissioned DESC')
            ->setParameter('ecosystem', $entities['ecosystem'])
            ->getResult();
        $templates = $this->ds
                ->queryBuilder(ActivityTemplate::class, 't')
                ->join('t.component', 'c')
                ->join('t.environment', 'e')
                ->where('e.ecosystem = :ecosystem')
                ->setParameter('ecosystem', $entities['ecosystem'])
                ->getQuery()->getResult();
        $layerComponentEnvironmentTemplates = [];
        foreach ($templates as $template) {
            /** @var ActivityTemplate $template */
            list($layer, $component, $environment) = [$template->getComponent()->getLayer(),
                $template->getComponent(), $template->getEnvironment()];
            $layerComponentEnvironmentTemplates["$layer"]["$component"]["$environment"][]
                = $template;
        }
        return $this->render('ecosystem/view.html.twig', $entities + [
                'squads' => $squads,
                'decommissioned' => $decommissioned,
                'componentTemplates' => $layerComponentEnvironmentTemplates,
        ]);
    }

    #[Route('/{ecosystemId}/!', name: 'app_ecosystem_update')]
    public function update(?EcosystemRequest $ecosystemRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('ecoupd',
            'Update Ecosystem',
            $this->nav->entityRoutePath('app_ecosystem_update', $entities['ecosystem']),
            'pencil');
        return $this->add_or_update($ecosystemRequest, $entities['ecosystem']);
    }

    #[Route('/+', name: 'app_ecosystem_add', priority: 10)]
    public function add(?EcosystemRequest $ecosystemRequest): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $this->addHeader('ecoadd',
            'Add Ecosystem',
            $this->nav->entityRoutePath('app_ecosystem_add'),
            'sitemap');
        return $this->add_or_update($ecosystemRequest);
    }

    protected function add_or_update(?EcosystemRequest $ecosystemRequest,
                                     ?Ecosystem $ecosystem = null): Response
    {
        if ($ecosystemRequest) {
            if (!$ecosystem) {
                $ecosystem = new Ecosystem($ecosystemRequest->id);
            }
            if ($ecosystemRequest->formAction == 'delete' && $ecosystem->canDelete()) {
                $this->addFlash('info', "Ecosystem '{$ecosystem->getName()}' has been deleted");
                $redirect = $this->redirectToRoute('app_home');
                $this->ds->delete($ecosystem);
                return $redirect;
            }
            $ecosystemRequest->populate($ecosystem);
            if (!($errors = $this->validator->validate($ecosystem))) {
                $this->ds->commit($ecosystem);
                return $this->redirectToRoute('app_ecosystem_view', $this->nav->entityToParameters($ecosystem));
            }
        }
        return $this->render('ecosystem/form.html.twig', [
                'ecosystem' => $ecosystem,
                'errors' => $errors ?? [],
        ]);
    }
}