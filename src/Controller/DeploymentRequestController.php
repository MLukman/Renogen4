<?php

namespace App\Controller;

use App\Base\Controller;
use App\DTO\DeploymentRequestRequest;
use App\Entity\Deployment;
use App\Entity\DeploymentRequest;
use App\Entity\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeploymentRequestController extends Controller
{

    #[Route('/{ecosystemId}/@{environmentId}/+/+', name: 'app_deployment_request_add', priority: 30)]
    public function add(?DeploymentRequestRequest $deploymentRequestRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('depreqnew',
            'Add Deployment Request',
            $this->nav->entityRoutePath('app_deployment_add', $entities['environment']),
            'calendar plus o');
        return $this->add_or_update($deploymentRequestRequest, $entities['environment']);
    }

    #[Route('/{ecosystemId}/@{environmentId}/+/{deploymentRequestId}/', name: 'app_deployment_request_update', priority: 20)]
    public function update(?DeploymentRequestRequest $deploymentRequestRequest): Response
    {
        $entities = $this->resolveEntitiesAndCheckPermission();
        $this->addHeader('deprequpd',
            'Update Deployment Request',
            $this->nav->entityRoutePath('app_deployment_request_update', $entities['deploymentRequest']),
            'pencil');
        return $this->add_or_update($deploymentRequestRequest, $entities['deploymentRequest']);
    }

    protected function add_or_update(?DeploymentRequestRequest $deploymentRequestRequest,
                                     Environment|DeploymentRequest $deployment_or_environment): Response
    {
        /** @var DeploymentRequest $deploymentRequest */
        $deploymentRequest = ($deployment_or_environment instanceof DeploymentRequest
                ? $deployment_or_environment : new DeploymentRequest($deployment_or_environment));
        if ($deploymentRequestRequest) {
            switch ($deploymentRequestRequest->formAction) {
                case 'accept':
                    $this->denyAccessUnlessGranted('accept', $deploymentRequest);
                    if (!$deploymentRequest->getDeployment()) {
                        $deployment = $deploymentRequest->createDeployment();
                        if (!($errors = $this->validator->validate($deployment))) {
                            $this->ds->commit($deployment);
                            return $this->redirectToRoute('app_deployment_update', $this->nav->entityToParameters($deployment));
                        } else {
                            $this->addFlash('error', "Unable to create deployment most probably due to another deployment with the same date & time already exists");
                        }
                    }
                    break;

                case 'delete':
                    $this->denyAccessUnlessGranted('delete', $deploymentRequest);
                    $this->addFlash('info', "Deployment request '{$deploymentRequest->displayTitle()}' has been deleted");
                    $redirect = $this->redirectToRoute('app_environment_view', $this->nav->entityToParameters($deploymentRequest->getEnvironment()), anchor: '#/requests');
                    $this->ds->delete($deploymentRequest);
                    return $redirect;

                default:
                    $this->denyAccessUnlessGranted('update', $deploymentRequest);
                    $deploymentRequestRequest->populate($deploymentRequest);
                    if (!($errors = $this->validator->validate($deploymentRequest))) {
                        $this->ds->commit($deploymentRequest);
                        $this->addFlash('info', "Deployment request has been saved. Please wait for it to be accepted by a user with 'approval' squad role.");
                        return $this->redirectToRoute('app_environment_view', $this->nav->entityToParameters($deploymentRequest->getEnvironment()), anchor: '#/requests');
                    }
            }
        }

        if (!isset($errors)) {
            //$this->saveCurrentUrlToRedirectLater($this->requestStack->getCurrentRequest(), true);
        }
        return $this->render('deployment_request/form.html.twig', [
                'deploymentRequest' => $deploymentRequest,
                'errors' => $errors ?? [],
        ]);
    }
}