<?php

namespace App\ActivityTemplate;

use App\Base\ActivityTemplateInterface;
use App\Entity\Actionable;
use App\Entity\Activity;
use App\Entity\ActivityFile;
use App\Entity\ActivityTemplate;
use App\Service\DataStore;
use App\Service\Markdown;
use App\Service\Navigator;
use MLukman\DoctrineHelperBundle\Service\ObjectValidator;
use MLukman\DoctrineHelperBundle\Type\FileWrapper;
use Symfony\Component\HttpFoundation\RequestStack;

class UpdateConfiguration implements ActivityTemplateInterface
{

    public function __construct(protected ObjectValidator $validator,
                                protected RequestStack $requestStack,
                                protected DataStore $ds,
                                protected Navigator $nav)
    {
        
    }

    static function formatBytes($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision).' '.$units[$pow];
    }

    public function describeActionableAsArray(Actionable $actionable,
                                              bool $isForRunBook = false): array
    {
        $configurations = $actionable->getTemplate()->getConfigurations();
        $parameters = $actionable->getParameters();
        $instructions = str_replace("{@ID}", $actionable->getId(), $actionable->getTemplate()->getConfigurations()['instructions']);
        $array = [];

        foreach ($configurations['parameters'] as $paramCfg) {
            $d = $isForRunBook ? $paramCfg['id'] : $paramCfg['title'];

            if (!$isForRunBook &&
                ($paramCfg['type'] == 'password' || ($paramCfg['sensitive'] ?? false))) {
                $array[$d] = '<em>-- Redacted Due to Sensitive Info --</em>';
                continue;
            }

            switch ($paramCfg['type']) {
                case 'file':
                    if (empty($parameters[$paramCfg['id']] ?? null)) {
                        $array[$d] = '<em>-- Not Provided --</em>';
                        break;
                    }
                    $activityFile = $this->ds->getActivityFile($parameters[$paramCfg['id']]['activityFileId']);
                    if ($activityFile) {
                        $url = $this->nav->generateUrl(
                            'app_file_download',
                            $this->nav->entityToParameters($actionable->getTemplate()->getEnvironment())
                            + ['activityFileId' => $activityFile->getId()]);
                        $array[$d] = '<a href="'.$url.'">'.$activityFile->getFile()->getName().'</a> ('.static::formatBytes($activityFile->getFile()->getSize()).')';
                    } else {
                        $array[$d] = null;
                    }
                    break;

                case 'url':
                    if (strpos($instructions, "{{$paramCfg['id']}}") !== false &&
                        !($paramCfg['sensitive'] ?? false)) {
                        $instructions = str_replace("{{$paramCfg['id']}}", $parameters[$paramCfg['id']], $instructions);
                    } else {
                        $array[$d] = [
                            'templateString' => '{{ base.textLink(text, link) }}',
                            'templateContext' => [
                                'text' => $parameters[$paramCfg['id']],
                                'link' => $parameters[$paramCfg['id']]
                            ],
                        ];
                    }
                    break;

                case 'script':
                case 'multiline':
                    $array[$d] = empty($parameters[$paramCfg['id']]) ?
                        '<em>-- Empty / Not Specified --</em>' : [
                        'templateString' => '{{ base.copyableSourceField(label, value) }}',
                        'templateContext' => [
                            'label' => $d,
                            'value' => $parameters[$paramCfg['id']]
                        ],
                    ];
                    break;

                case 'checkbox':
                    $array[$d] = [
                        'templateString' => '{{ base.readonlyCheckbox(checked) }}',
                        'templateContext' => [
                            'checked' => !empty($parameters[$paramCfg['id']])
                        ],
                    ];
                    break;

                case 'multiselect':
                    $array[$d] = $parameters[$paramCfg['id']];
                    break;

                default:
                    if (strpos($instructions, "{{$paramCfg['id']}}") !== false &&
                        !($paramCfg['sensitive'] ?? false)) {
                        $instructions = str_replace("{{$paramCfg['id']}}", $parameters[$paramCfg['id']], $instructions);
                    } else {
                        $array[$d] = empty($parameters[$paramCfg['id']]) ?
                            null : [
                            'templateString' => '{{ base.copyableTextField(label, value) }}',
                            'templateContext' => [
                                'label' => $d,
                                'value' => $parameters[$paramCfg['id']]
                            ],
                        ];
                    }
            }
        }

        return ['Instructions' => (new Markdown())->parse($instructions),] +
            (!empty($array) ? ['Parameters' => $array] : []);
    }

    public function prepareTemplateName(ActivityTemplate $template): string
    {
        return "Update Configuration ".$template->getComponent()->displayTitle();
    }

    public function validateActivity(Activity $activity): array
    {
        $errors = [];

        $parameters = $activity->getParameters();
        $configurations = $activity->getTemplate()->getConfigurations();
        $parameterFiles = $this->requestStack->getMainRequest()->files->all('parameters');
        foreach ($configurations['parameters'] as $paramCfg) {
            if ($paramCfg['type'] == 'file') {
                $activityFile = $this->ds->getActivityFile($parameters[$paramCfg['id']]['activityFileId']);
                if ($activityFile && isset($parameters[$paramCfg['id']]['delete'])) {
                    $this->ds->em()->remove($activityFile);
                    $parameters[$paramCfg['id']] = null;
                } elseif (isset($parameterFiles[$paramCfg['id']])) {
                    if (!$activityFile) {
                        $activityFile = new ActivityFile();
                        $activityFile->setIdentifier($paramCfg['id']);
                        $activityFile->setActivity($activity);
                        $this->ds->manage($activityFile);
                    }
                    $activityFile->setFile(FileWrapper::fromUploadedFile($parameterFiles[$paramCfg['id']]));
                    $parameters[$paramCfg['id']] = ['activityFileId' => $activityFile->getId()];
                } elseif (!$activityFile) {
                    $parameters[$paramCfg['id']] = null;
                }
            } else {
                if (\is_string($parameters[$paramCfg['id']])) {
                    $parameters[$paramCfg['id']] = trim($parameters[$paramCfg['id']]);
                }
                if (isset($paramCfg['required']) && empty($parameters[$paramCfg['id']])) {
                    $this->validator->addValidationError($errors, 'parameters.'.$paramCfg['id'], 'Required');
                } elseif ($paramCfg['type'] == 'url' && !filter_var($parameters[$paramCfg['id']], FILTER_VALIDATE_URL)) {
                    $this->validator->addValidationError($errors, 'parameters.'.$paramCfg['id'], 'Must be a valid URL');
                } elseif ($paramCfg['type'] == 'formatted' && !preg_match("/^{$p['details']}$/", $parameters[$paramCfg['id']])) {
                    $this->validator->addValidationError($errors, 'parameters.'.$paramCfg['id'], 'Invalid format');
                }
            }
        }
        $activity->setParameters($parameters);

        return $errors;
    }

    public function validateTemplate(ActivityTemplate $template): array
    {
        $errors = [];
        $configurations = &$template->getConfigurations();
        $configurations['instructions'] = trim($configurations['instructions'] ?? null);
        if (empty($configurations['instructions'])) {
            $this->validator->addValidationError($errors, 'configurations.instructions', 'Required');
        }
        $configurations['parameters'] = array_filter(
            ($configurations['parameters'] ?? []) ?: [],
            fn($parameter) => !empty($parameter)
        );
        $keys = [];
        foreach ($configurations['parameters'] as $k => $parameter) {
            foreach (['id', 'title', 'type'] as $field) {
                if (!isset($parameter[$field])) {
                    $this->validator->addValidationError($errors, "configurations.parameters.{$k}.{$field}", 'Required');
                }
            }
            if (isset($parameter['id'])) {
                if (isset($keys[$parameter['id']])) {
                    $this->validator->addValidationError($errors, "configurations.parameters.{$k}.id", 'Must be unique');
                } else {
                    $keys[$parameter['id']] = 1;
                }
            }
        }
        return $errors;
    }

    public static function templateFormTwigFilepath(): string
    {
        return 'activity_template/updateConfiguration.template.html.twig';
    }

    public static function activityFormTwigFilepath(): string
    {
        return 'activity_template/updateConfiguration.activity.html.twig';
    }

    public static function allowMultipleTemplates(): bool
    {
        return true;
    }
}