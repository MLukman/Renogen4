<?php

namespace App\ActivityTemplate;

use App\Base\ActivityTemplateInterface;
use App\Entity\Actionable;
use App\Entity\Activity;
use App\Entity\ActivityTemplate;
use App\Service\Markdown;
use MLukman\DoctrineHelperBundle\Service\ObjectValidator;

class UpdateVersion implements ActivityTemplateInterface
{

    public function __construct(protected ObjectValidator $validator)
    {
        
    }

    public function describeActionableAsArray(Actionable $actionable,
                                              bool $isForRunBook = false): array
    {
        $configurations = $actionable->getTemplate()->getConfigurations();
        $version = $actionable->getParameters()['version'] ?? null;
        $label = $isForRunBook ? ($configurations['versionRunbookLabel'] ?? 'VERSION')
                : ($configurations['versionActivityLabel'] ?? 'Version');
        return [
            'Instructions' => (new Markdown())->parse(str_replace("{VERSION}", $version, $configurations['instructions'])),
            ($label) => [
                'templateString' => '{{ base.copyableTextField(label, value) }}',
                'templateContext' => [
                    'label' => $label,
                    'value' => $version,
                ],
            ]
        ];
    }

    public function validateActivity(Activity $activity): array
    {
        $errors = [];
        $parameters = &$activity->getParameters();
        $parameters['version'] = trim($parameters['version']);
        if (empty($parameters['version'])) {
            $this->validator->addValidationError($errors, 'parameters.version', 'Required');
        }
        return $errors;
    }

    public function validateTemplate(ActivityTemplate $template): array
    {
        $errors = [];
        $configurations = &$template->getConfigurations();
        $configurations['instructions'] = trim(
            $configurations['instructions'] ?? '');
        if (empty($configurations['instructions'])) {
            $this->validator->addValidationError($errors, 'configurations.instructions', 'Required');
        }
        $configurations['versionActivityLabel'] = trim(
            $configurations['versionActivityLabel'] ?? '');
        if (empty($configurations['versionActivityLabel'])) {
            $this->validator->addValidationError($errors, 'configurations.versionActivityLabel', 'Required');
        }
        $configurations['versionRunbookLabel'] = trim(
            $configurations['versionRunbookLabel'] ?? '');
        if (empty($configurations['versionRunbookLabel'])) {
            $this->validator->addValidationError($errors, 'configurations.versionRunbookLabel', 'Required');
        }
        return $errors;
    }

    public static function templateFormTwigFilepath(): string
    {
        return 'activity_template/updateVersion.template.html.twig';
    }

    public static function activityFormTwigFilepath(): string
    {
        return 'activity_template/updateVersion.activity.html.twig';
    }

    public static function allowMultipleTemplates(): bool
    {
        return false;
    }

    public function prepareTemplateName(ActivityTemplate $template): string
    {
        return "Update Version ".$template->getComponent()->displayTitle();
    }
}