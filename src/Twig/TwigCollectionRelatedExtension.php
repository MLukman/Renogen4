<?php

namespace App\Twig;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigCollectionRelatedExtension extends AbstractExtension
{

    public function getFilters(): array
    {
        return [
            new TwigFilter('collfilter', [$this, 'filter']),
        ];
    }

    public function filter($collection, array $comparisons)
    {
        if (!($collection instanceof Collection)) {
            return $collection;
        }
        $criteria = new Criteria();
        foreach ($comparisons as $key => $value) {
            $criteria->andWhere(new Comparison($key, '=', $value));
        }
        return $collection->matching($criteria);
    }
}