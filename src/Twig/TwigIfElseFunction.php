<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigIfElseFunction extends AbstractExtension
{

    public function getFunctions(): array
    {
        return [
            new TwigFunction('ifelse', [$this, 'ifElse']),
            new TwigFunction('switch', [$this, 'switch']),
        ];
    }

    public function ifElse(mixed $condition, mixed $ifTrue, mixed $ifFalse = ''): mixed
    {
        return !empty($condition) ? $ifTrue : $ifFalse;
    }

    public function switch(mixed $condition, array $cases, mixed $ifNoMatch = ''): mixed
    {
        if (isset($cases[$condition])) {
            return $cases[$condition];
        }
        return $ifNoMatch;
    }
}