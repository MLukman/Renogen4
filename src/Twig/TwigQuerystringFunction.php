<?php

namespace App\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigQuerystringFunction extends AbstractExtension
{

    public function __construct(protected RequestStack $requestStack)
    {

    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('querystring', [$this, 'querystring']),
        ];
    }

    /**
     * Return the value of the requested environment variable.
     * 
     * @param array $queries
     * @return string
     */
    public function querystring(array $queries): string
    {
        $currentQueries = $this->requestStack->getCurrentRequest()->query->all();
        foreach ($queries as $k => $v) {
            if (is_null($v) && isset($currentQueries[$k])) {
                unset($currentQueries[$k]);
                continue;
            }
            $currentQueries[$k] = $v;
        }
        return '?'.\http_build_query($currentQueries);
    }
}