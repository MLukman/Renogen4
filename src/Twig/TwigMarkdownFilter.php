<?php

namespace App\Twig;

use App\Service\Markdown;
use Twig\Extension\AbstractExtension;
use Twig\Markup;
use Twig\TwigFilter;

class TwigMarkdownFilter extends AbstractExtension
{

    public function getFilters(): array
    {
        return [
            new TwigFilter('markdown', [$this, 'markdown'], ['is_safe' => ['html']]),
        ];
    }

    public function markdown($var, array $props = [])
    {
        $parser = new Markdown($props);
        return new Markup($parser->parse(trim($var)), 'UTF-8');
    }
}