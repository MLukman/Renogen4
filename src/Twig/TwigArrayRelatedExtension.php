<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigArrayRelatedExtension extends AbstractExtension
{

    public function getFilters(): array
    {
        return [
            new TwigFilter('unique', [$this, 'unique']),
        ];
    }

    public function unique($array)
    {
        if (is_array($array)) {
            $array = array_unique($array);
        }
        return $array;
    }
}