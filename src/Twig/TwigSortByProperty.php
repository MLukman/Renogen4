<?php

namespace App\Twig;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigSortByProperty extends AbstractExtension
{
    protected PropertyAccessor $propertyAccessor;

    public function __construct()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('psort', [$this, 'sortByProperty']),
        ];
    }

    /**
     * Sort $var array by its items properties. Use dot to navigate down nested properties, e.g. parent.child.grandchild.property.
     * This sorter support multiple properties to break the tie if the previous properties result in same values.
     *
     * Example use: {% for item in items|psort('parent.child.level','parent.level') %}
     *
     * @param type $var The array to sort
     * @param string $props The property/properties to sort the array by
     * @return mixed The sorted input array, or the original $var if it's not an array
     */
    public function sortByProperty($var, string ...$props): mixed
    {
        if (!is_array($var) || count($props) == 0) {
            return $var;
        }
        uasort($var, $this->makeSortFunction($props));
        return $var;
    }

    public function makeSortFunction(array $props)
    {
        return function ($a, $b) use ($props) {
            $va = $this->extractProperty($a, $props[0]);
            $vb = $this->extractProperty($b, $props[0]);
            if ($va == $vb) {
                return count($props) > 1 ?
                    $this->makeSortFunction(array_slice($props, 1))($a, $b) : 0;
            }
            return ($va < $vb) ? -1 : 1;
        };
    }

    public function extractProperty($var, $prop)
    {
        $tokens = explode(".", $prop);
        $p = array_shift($tokens);
        $v = (is_array($var) ?
            (isset($var[$p]) ? $var[$p] : null) :
            (is_object($var) ? $this->propertyAccessor->getValue($var, $p) : null));
        if (empty($tokens)) {
            return $v;
        } else {
            return $this->extractProperty($v, implode(".", $tokens));
        }
    }
}