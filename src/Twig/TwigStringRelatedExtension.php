<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigStringRelatedExtension extends AbstractExtension
{

    public function getFilters(): array
    {
        return [
            new TwigFilter('basename', [$this, 'basename']),
            new TwigFilter('uncamelize', [$this, 'uncamelize']),
            new TwigFilter('json_decode', [$this, 'json_decode']),
        ];
    }

    public function basename($value)
    {
        return \trim(\preg_replace('/^(.*[\\\\\/])?([^\\\\\/]*)$/', '$2', $value));
    }

    public function uncamelize($value)
    {
        return \trim(\preg_replace('/\s+/', ' ', \preg_replace('/([A-Z]*)([A-Z][^A-Z]+)/', '$1 $2 ', $value)));
    }

    public function json_decode($value)
    {
        return \json_decode($value, true) ?: [];
    }
}