<?php

namespace App\Twig;

use ReflectionClass;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigTest;

class TwigClassRelatedExtension extends AbstractExtension
{

    public function getTests(): array
    {
        return [
            new TwigTest('instanceof', [$this, 'isInstanceOf']),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('class', [$this, 'getClass']),
        ];
    }

    public function isInstanceof($var, $instance): bool
    {
        if (!is_object($var)) {
            return false;
        }
        $reflexionClass = new ReflectionClass($instance);
        return $reflexionClass->isInstance($var);
    }

    public function getClass($value)
    {
        return \get_class($value);
    }
}