<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;

class SquadMembersRequest extends RequestBody
{
    public ?array $memberships = [];
    public ?array $newMemberUsers = [];
    public ?string $newMemberRole = null;
    public ?string $formAction = 'save';

}