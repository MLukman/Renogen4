<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;
use Symfony\Component\Validator\Constraints as Assert;

class ItemCommentRequest extends RequestBody
{
    #[Assert\NotBlank(normalizer: 'trim')]
    public ?string $itemComment;

}