<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;

class EcosystemRequest extends RequestBody
{
    public ?string $id = null;
    public ?string $name;
    public ?string $description = '';
    public ?string $layers = '';
    public ?bool $private;
    public ?string $formAction = 'save';

}