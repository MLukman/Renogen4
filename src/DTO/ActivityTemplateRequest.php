<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;

class ActivityTemplateRequest extends RequestBody
{
    public ?string $class;
    public ?string $name;
    public ?array $configurations;
    public ?int $priority;
    public ?bool $singleActivityPerDeployment;
    public ?bool $disabled;
    public ?string $formAction = 'save';

}