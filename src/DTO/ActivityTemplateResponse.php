<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\ResponseBody;

class ActivityTemplateResponse extends ResponseBody
{
    public ?string $class;
    public ?string $name;
    public ?array $configurations;
    public ?int $priority;
    public ?bool $singleActivityPerDeployment;
    public ?bool $disabled;

}