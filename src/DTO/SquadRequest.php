<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;
use MLukman\DoctrineHelperBundle\Type\ImageWrapper;

class SquadRequest extends RequestBody
{
    public ?string $identifier;
    public ?string $name;
    public ?string $description = '';
    public ?ImageWrapper $icon;
    public ?bool $decommissioned;
    public ?string $formAction = 'save';

}