<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;

class EnvironmentRequest extends RequestBody
{
    public ?string $identifier;
    public ?string $name;
    public ?int $level;
    public ?string $formAction = 'save';

}