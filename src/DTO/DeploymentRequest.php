<?php

namespace App\DTO;

use DateTimeInterface;
use MLukman\DoctrineHelperBundle\DTO\RequestBody;

class DeploymentRequest extends RequestBody
{
    public ?string $name = '';
    public ?string $description = '';
    public ?int $duration;
    public ?DateTimeInterface $executeDate;
    public ?string $formAction = 'save';

}