<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;
use Symfony\Component\Validator\Constraints as Assert;

class RunBookUpdateRequest extends RequestBody
{
    #[Assert\NotBlank]
    public ?string $runItemId = null;

    #[Assert\NotBlank]
    public ?string $newStatus = null;
    public ?bool $remarkRequired = false;
    public ?string $remark;

}