<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;

class ItemRequest extends RequestBody
{
    public ?string $name;
    public ?string $refnum = '';
    public ?string $category;
    public ?string $description = '';
    public ?string $externalUrl = '';
    public ?string $externalUrlLabel = '';
    public ?string $moveto;
    public ?string $formAction = 'save';

}