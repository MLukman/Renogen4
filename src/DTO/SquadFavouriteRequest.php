<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;

class SquadFavouriteRequest extends RequestBody
{
    public ?string $membershipId = null;
    public ?int $favourite = 0;

}