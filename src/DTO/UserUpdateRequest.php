<?php

namespace App\DTO;

use App\Entity\Ecosystem;
use App\Entity\EcosystemAdmin;
use App\Entity\Membership;
use App\Entity\Squad;
use App\Entity\User;
use App\Service\DataStore;
use Doctrine\Common\Collections\Collection;
use LogicException;
use MLukman\DoctrineHelperBundle\DTO\RequestBody;
use MLukman\DoctrineHelperBundle\DTO\RequestBodyTargetInterface;

class UserUpdateRequest extends RequestBody
{
    public ?string $fullname;
    public ?string $email;
    public ?int $admin = 0;
    public ?array $memberships = [];
    public ?array $ecosystemAdmins = [];
    public ?string $formAction = 'save';

    protected function prepareTargetPropertyValue(RequestBodyTargetInterface $target,
                                                  string $property_name,
                                                  mixed $request_property_value,
                                                  mixed $target_property_value,
                                                  array $target_property_types,
                                                  mixed $context = null): mixed
    {
        if ($context instanceof DataStore && $target instanceof User) {
            if ($property_name == 'memberships' &&
                $target_property_value instanceof Collection) {
                foreach ($request_property_value as $squadId => $role) {
                    $squad = $context->queryOne(Squad::class, $squadId);
                    $membership = $context->queryOne(Membership::class, [
                        'squad' => $squad,
                        'member' => $target,
                    ]);
                    $role = in_array($role, Membership::ROLESEQUENCE) ? $role : null;
                    if ($membership) {
                        if (!empty($role)) {
                            $membership->setRole($role);
                        } else {
                            $target_property_value->removeElement($membership);
                        }
                    } elseif (!empty($role)) {
                        $membership = new Membership($target, $squad, $role);
                        $target_property_value->add($membership);
                    }
                }
                return $target_property_value;
            } elseif ($property_name == 'ecosystemAdmins' &&
                $target_property_value instanceof Collection) {
                foreach ($target_property_value as $ea) {
                    $key = $ea->getEcosystem()->getId();
                    if (!in_array($key, $request_property_value)) {
                        $target_property_value->remove($key);
                    }
                }
                foreach ($request_property_value as $ecoId) {
                    if (!$target_property_value->containsKey($ecoId) &&
                        ($ecosystem = $context->queryOne(Ecosystem::class, $ecoId))) {
                        $target_property_value->set($ecoId, new EcosystemAdmin($ecosystem, $target));
                    }
                }
                return $target_property_value;
            }
            return parent::prepareTargetPropertyValue($target, $property_name, $request_property_value, $target_property_value, $target_property_types, $context);
        } else {
            throw new LogicException(\sprintf("Class %s requires App\\Entity\\User and App\\Service\\DataStore as first and second parameters of populate() method", \get_class($this)));
        }
    }
}