<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;
use Symfony\Component\Validator\Constraints as Assert;

class ItemChangeStatusRequest extends RequestBody
{
    #[Assert\NotBlank]
    public ?string $newStatus = null;
    public ?string $statusRemark = null;
    public ?bool $remarkRequired = false;

}