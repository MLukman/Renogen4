<?php

namespace App\DTO;

use App\Entity\Checklist;
use App\Entity\User;
use App\Service\DataStore;
use DateTime;
use Doctrine\Common\Collections\Collection;
use LogicException;
use MLukman\DoctrineHelperBundle\DTO\RequestBody;
use MLukman\DoctrineHelperBundle\DTO\RequestBodyTargetInterface;

class ChecklistRequest extends RequestBody
{
    public ?string $title = '';
    public ?DateTime $start = null;
    public ?DateTime $end = null;
    public ?array $pics = [];
    public ?string $status = 'Not Started';
    public ?string $update = '';
    public ?string $formAction = 'save';

    protected function prepareTargetPropertyValue(RequestBodyTargetInterface $target,
                                                  string $property_name,
                                                  mixed $request_property_value,
                                                  mixed $target_property_value,
                                                  array $target_property_types,
                                                  mixed $context = null): mixed
    {
        if (!($context instanceof DataStore && $target instanceof Checklist)) {
            throw new LogicException(\sprintf("Class %s requires App\\Entity\\Checklist and App\\Service\\DataStore as first and second parameters of populate() method", \get_class($this)));
        }
        if ($property_name == 'pics' &&
            $target_property_value instanceof Collection) {
            $target_property_value->clear();
            foreach ($request_property_value as $userId) {
                $user = $context->queryOne(User::class, $userId);
                if ($user) {
                    $target_property_value->add($user);
                }
            }
            return $target_property_value;
        }
        return parent::prepareTargetPropertyValue($target, $property_name, $request_property_value, $target_property_value, $target_property_types, $context);
    }
}