<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;
use Symfony\Component\Validator\Constraints as Assert;

class ItemCommentActionRequest extends RequestBody
{
    #[Assert\NotBlank]
    public ?string $commentId;
    public ?string $commentAction = null;

}