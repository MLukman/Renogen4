<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;

class ProfileRequest extends RequestBody
{
    public ?string $fullname = '';
    public ?string $email = '';

}