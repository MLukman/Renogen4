<?php

namespace App\DTO;

use MLukman\DoctrineHelperBundle\DTO\RequestBody;

class ComponentRequest extends RequestBody
{
    public ?string $identifier;
    public ?string $name = '';
    public ?string $layer = null;
    public ?string $formAction = 'save';

}