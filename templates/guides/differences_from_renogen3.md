## Metamodel: From Siloed to Sharing

Renogen 3 has a siloed metamodel in which projects are not connected to each other. 
In order to cater multiple environments, separate projects needed to be created. Combined with multiple squads, it has resulted in `(number of projects) = (number of squads) x (number of environments)` had to be created and maintained all the time.
This has resulted in duplicate deployments and activities had to be created even though they deployed same component into the same environment at the same deployment windows.

```mermaid
flowchart LR
    P(Project)-->D(Deployment)
    D-->I(Item)
    I-->A(Activity)
```

Renogen 4 separates Renogen3's projects into squads and environments, both consolidated into ecosystems. Deployments are created under an environment, which can be shared among multiple squads. Each squad creates and owns deployment items, while activities created for an item are actually tied to the deployment and can be shared with other items from other squads.

```mermaid
flowchart LR
    E(ecosystem)-->S(Squad)
    E-->V(Environment)
    V-->D(Deployment)
    S-->I(Item)
    D-->I
    D-->A(Activity)
    I-->IA(Item Activity)
    A-->IA(Item Activity)
```

## Component Deployment Activity History

In Renogen3, there is no way to easily get the history of deployment of a particular component since deployment activities are spread over multiple projects. The easiest is to go through each project's activity templates to get the list of activities that used those particular templates. For activity templates that can be used to deploy a choice of components, one would need to eye-ball which of the activities to filter those which are related to the particular component. In short, it's not that easy.

In Renogen4, components are first class data model citizens under ecosystems. Activity templates are created for specific components and specific environments. Whenever a particular activity template is used to create a deployment activity, it is already for a specific component and therefore, it would later be very easy to check the evolution of a component in any particular environment.

```mermaid
flowchart LR
    E(ecosystem)-->C(Component)
    E-->V(Environment)
    V-->AT(Activity Template)
    C-->AT
    V-->D(Deployment)
    D-->A(Activity)
    AT-->A
```