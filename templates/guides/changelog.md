
## 4.1.1

- Fixed `$_caches` error
- Upgraded to Symfony 7.1

## 4.1.0

- Added support to allow login via LDAP
- Added "remember me" feature to login

## 4.0.5

- Fixed activities ordering inside item view page to be based on template priorities instead of based on datetime the activities are added to the item.
- Fixed runbook ordering of activities created using the same activity template to be based on creation date.
- Added support for activities and templates with only instructions but no parameters.
- Enhanced ReCaptcha implementation to fail silently if there is connection timeout to Google ReCaptcha server.

## 4.0.4

- A deployment item can now be moved to another upcoming deployment

## 4.0.3

- Activity templates can now be exported by ecosystem admins into JSON files and the files can later be used to import as new activity templates for other components and/or environments, even across ecosystems.
- Improved performance of markdown parsing as well as supports for mermaid diagrams in activity templates' instructions.
- Ecosystem can now be set private to hide from non-members and non-admins.
- Squad can now be decommissioned to make it read-only.

## 4.0.2

- Page to view a particular environment's all past deployments was added and linked from the "Past Deployments" tab.
- Squad page now shows an icon for members who has logged in within last 30 minutes.
- "Components" tab in ecosystem view page has been changed to tabular form and now lists down all activity templates in all environments instead of just showing the counts.
- "Components" tab in environment view page has been renamed as "Activity Templates" tab and has been changed into tabular form.

## 4.0.1

- Release note page was implemented and accessible from deployment view page.
- Navigation links to the previous and next deployments was added to deployment view page.
- Activity templates can now be disabled by administrators to prevent creation of new activities using those templates.

## 4.0.0

Introducing revamped Renogen with re-designed data modeling. 
