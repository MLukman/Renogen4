These are roles & permissions inside Renogen4

## Administrator (global)

- Create/update/remove ecosystems
- Create/update/remove components
- Create/update/remove activity templates
- Create/update/remove environments
- Create/update/remove squads
- Assign/unassign users as ecosystem administrators
- Assign/unassign users to/from squads

## Ecosystem Administrator (per-ecosystem)

- Update ecosystems
- Create/update/remove components
- Create/update/remove activity templates
- Create/update/remove environments
- Create/update/remove deployments
- Create/update/remove squads
- Assign/unassign users to/from squads

## Other Roles (per-squad)

### Approval

- Update squads
- Assign/unassign users to/from squads
- Create/update/remove deployments
- Create/update/remove items
- Create/update/remove activities
- Approve deployment requests
- Change items status from "Go No Go" to "Ready For Release"
- Change runbook item status to either "Completed" or "Failed"

### Review

- Change items status from "Review" to "Go No Go"

### Entry

- Create/update deployment requests
- Create/update items
- Remove own items
- Create/update/remove activities
- Change items status from "Documentation" to "Review"

### Execute

- Change runbook item status to either "Completed" or "Failed"
