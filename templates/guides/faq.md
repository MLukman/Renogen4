## How do I join a squad?

You need to request the squad owner, the parent ecosystem administrator or a global administrator to add you to the squad.

## There is a missing/incomplete activity template for a specific component that I need to deploy.

You can report to an ecosystem administrator to add or modifiy the activity template.

## I can't delete a deployment item because the Delete button does not appear.

You can delete a deployment item only if two conditions below are fulfilled:

- You are the item creator or have 'approval' role in the squad
- The deployment item does not have any activities

## I reverted a deployment item status from 'Completed' to 'Ready For Release' but the status does not change.

If a deployment item changes status to 'Ready For Release' and either all of its activities are completed or it does not have any activities, the status will be automatically advanced to 'Completed'. It's by design.

If you want to revert a completed deployment item, please revert to any status before 'Ready For Release'.