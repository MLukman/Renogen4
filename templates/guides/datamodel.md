The following high level and entity relationship diagrams are for reference purpose only for those who are interested about the inner working of Renogen.

## High Level

```mermaid
flowchart TD
    E(Ecosystem) --> S(Squad)
    E --> V(Environment)
    I-->CM(Item Comment)
    I-->SL(Item Status Log)
    V --> D(Deployment)
    S --> I(Deployment Item)        
    I <--> A(Deployment Activity)
    D --> I
    D --> A
    E --> C(Component)
    C --> T(Activity Template)
    V --> T
    T --> A
    U(User)-->L(Login)
    U-->M(Membership)
    M-->S
    A-->AF(Activity File)
    A-->RI(Run Item)
    T-->RI
```

## ERD

```mermaid
erDiagram
    USER {
        string id PK
        string shortname
        string fullname
        string email
    }
    LOGIN {
        string username PK
        string method
        string credential
        string fullname
        string email
        datetime lastLogin
    }
    MEMBERSHIP {
        string role
    }
    ECOSYSTEM {
        string id PK
        string name
        array layers
    }
    SQUAD {
        guid id PK
        string identifier
        string name
        image icon
    }
    ENVIRONMENT {
        guid id PK
        string identifier
        string name
        int level
    }
    COMPONENT {
        guid id PK
        string identifier
        string name
        string layer
    }
    ACTIVITY_TEMPLATE {
        guid id PK
        string name
        string class
        json configurations
        int priority
        bool singleActivityPerDeployment
        bool disabled
    }
    DEPLOYMENT {
        guid id PK
        string identifier
        string name
        datetime executeDate
        string dateString
    }
    ITEM {
        guid id PK
        string refnum
        string name
        string status
        string category
        text description
        string externalUrl
        string externalUrlLabel
    }
    ITEM_STATUS_LOG {
        guid id PK
        string status
    }
    ACTIVITY {
        guid id PK
        json parameters
        string status
    }
    RUN_ITEM {
        guid id PK
        json parameters
        string status
    }
    ITEM_COMMENT {
        guid id PK
        text text
    }
    ACTIVITY_FILE {
        guid id PK
        string identifier
        blob file
    }
    USER ||--o{ LOGIN: "authenticate via"
    USER ||--o{ MEMBERSHIP: "member of"
    MEMBERSHIP }o--|| SQUAD: "for"
    ECOSYSTEM ||--o{ SQUAD: "contains"
    ECOSYSTEM ||--o{ ENVIRONMENT: "contains"
    ECOSYSTEM ||--o{ COMPONENT: "contains"
    SQUAD ||--o{ ITEM: "manages"
    ENVIRONMENT ||--o{ DEPLOYMENT: "has"
    DEPLOYMENT ||--o{ ITEM: "consists of"
    DEPLOYMENT ||--o{ ACTIVITY: "consists of"
    DEPLOYMENT ||--o{ RUN_ITEM: "consists of"
    ITEM ||--o{ ITEM_COMMENT: "has"
    ITEM ||--o{ ITEM_STATUS_LOG: "has"
    ITEM }o--o{ ACTIVITY: "registers"
    COMPONENT ||--o{ ACTIVITY_TEMPLATE: "provides"
    ENVIRONMENT ||--o{ ACTIVITY_TEMPLATE: "manages"
    ACTIVITY_TEMPLATE ||--o{ ACTIVITY: "produces"
    ACTIVITY_TEMPLATE ||--o{ RUN_ITEM: "produces"
    ACTIVITY ||--o{ ACTIVITY_FILE: "has"
    ACTIVITY ||--o{ RUN_ITEM: "produces"
    

```

