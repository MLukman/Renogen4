# About Renogen

Renogen is a release management approval & tracking platform developed by [Muhammad Lukman Nasaruddin](mailto:lukman.nasaruddin@tm.com.my).

It allows developers to request approval from the system environment owners for the releases of developed enhancements as well as for system environment administrators to execute the steps needed to release the enhancements.

## Common flow

```mermaid
graph TB
req([Developer requests deployment window])-->crt
crt([Owner creates deployment window])-->item
item([Developer registers deployment items & specifies deployment activities derived from activity templates])-->rev
rev([Reviewer reviews deployment items])-->appr
appr([Owner approves deployment items])-->exec
exec([System Admin executes deployment activities])-->complete([Complete])
```

## Technologies

Renogen was proudly built using the following technologies: 

- PHP 8
- Symfony 6
- Doctrine ORM
- MySQL
- Twig
- Fomantic-UI
- jQuery

The source code of Renogen is available at https://gitlab.com/MLukman/Renogen4/
