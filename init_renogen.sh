#!/bin/bash

if [ -n "$BASE_PATH" ] && [ ! -d "public/$BASE_PATH" ]; then 
  ln -s . "public/$BASE_PATH"
fi

if [ -n "$TZ" ]; then
  ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
  echo $TZ > /etc/timezone
  echo "date.timezone=$TZ" > /usr/local/etc/php/conf.d/timezone.ini
fi

if [ ! -n "$DATABASE_URL" ]; then
  export DATABASE_URL="mysql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}"
fi

APP_ENV=dev php bin/console make:migration
php bin/console doctrine:migrations:migrate --no-interaction || echo No migrations needed!

# Call special migration code
bin/console app:migrate --no-interaction

chown -R www-data:www-data var
